#!/bin/bash
declare -a listPlayersOutput
listPlayersOutput=($(./gradlew listPlayers | grep 'PLAYER:' | sed -e 's/^PLAYER:\ //' | grep -vi '^test'))
echo "Players:"
for i in `seq 0 $((${#listPlayersOutput[@]}-1))`
do
    echo ${listPlayersOutput[i]}
done

listMapsOutput=$(./gradlew listMaps | grep 'MAP:' | sed -e 's/^MAP:\ //' | xargs | sed -e 's/ /,/g')
echo "Maps:"
echo $listMapsOutput

for i in `seq 0 $((${#listPlayersOutput[@]}-1))`
do
    for j in `seq $i $((${#listPlayersOutput[@]}-1))`
    do
        ./gradlew run -PteamA=${listPlayersOutput[i]} -PteamB=${listPlayersOutput[j]} -Pmaps=$listMapsOutput
    done
done
