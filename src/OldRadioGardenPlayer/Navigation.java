package OldRadioGardenPlayer;

import battlecode.common.*;

public strictfp class Navigation {

    public static final float PROBE_MIN_DISTANCE = 0.45f;

    private enum Mode {
    TO_END_LOCATION,
        BOUNDRY_BROWSE
    }

    private static Mode currentMode;
    public static MapLocation startLocation,endLocation,currentLocation;
    private static float neededProximityToTarget=0.0f;
    private static RobotController rc;

    private static final float globalbeamsize = 6.0f;
    private static final float beamangletowall1 = 45.0f;
    private static final float beamangletowall2 = 90.0f;
    private static final float beamangletowall3 = 135.0f;


    // initialized to the last known direction that the navigation took or may also have a potential better direction
    // when ih Boundry_Browse mode
    private static Direction currentDirection;
    private static Direction wallDirection1;
    private static Direction wallDirection2;
    private static Direction wallDirection3;
    // Distance from previous location to endlocation
    private static float previousDistance;
    //Distance to end location when booundry_browse mode is activated
    private static float boundaryStartDistance;
    private static MapLocation boundaryStartLocation;

    //The location on the boundary that seems to be the most closest to the target
    private static float closestBoundaryToTargetDistance;
    static MapLocation closestBoundaryToTargetLocation;
    private static int boundaryStepCount = 0;
    private static boolean isPossibleLoopInBoundary = false;
    private static boolean hasReachecdClosestBoundaryLocation = false;

    private static int minBoundaryStepCountBeforeCheckingForLoop = 13;


    static void initializeToLocation(RobotController myRC, MapLocation targetLocation,float NeededProximityToTarget){
    initializeToLocation(myRC,targetLocation,NeededProximityToTarget,13);
    }
    /**
     * Call this function to initialize navigation from current location to target
     * @param myRC
     * @param targetLocation
     */
    static void initializeToLocation(RobotController myRC, MapLocation targetLocation,float NeededProximityToTarget , int pminBoundaryStepCountBeforeCheckingForLoop){
        minBoundaryStepCountBeforeCheckingForLoop = pminBoundaryStepCountBeforeCheckingForLoop;
        rc = myRC;
        startLocation = rc.getLocation();
        endLocation = targetLocation;
        previousDistance = startLocation.distanceTo(endLocation);
        currentMode = Mode.TO_END_LOCATION;
        currentLocation = startLocation;
        currentDirection = startLocation.directionTo(endLocation);

        reachedTarget = false;
        //System.out.println(NeededProximityToTarget);
        neededProximityToTarget = NeededProximityToTarget + 0.1f;
        resetBoundaryParameters();


    }

    private static void resetBoundaryParameters() {
        //boundary values
        wallDirection1 = null;
        wallDirection2 = null;
        wallDirection3 = null;
        boundaryStartDistance = 0.0f;
        isPossibleLoopInBoundary = false;
        hasReachecdClosestBoundaryLocation = false;
        boundaryStartLocation = null;
        boundaryStepCount = 0;
        closestBoundaryToTargetLocation = null;
        closestBoundaryToTargetDistance = 0;

    }

    static boolean isInitialized() {
        if (endLocation != null) {
            return true;
        }
        return false;

    }

    private static boolean reachedTarget = false;
    static boolean hasReachedTarget(){
        if(reachedTarget  || endLocation ==null)
            return true;
        currentLocation = rc.getLocation();
        float currrentdistance = currentLocation.distanceTo(endLocation);
        debug_calledhasreachedtarget(currrentdistance);
        if(currrentdistance<=neededProximityToTarget){
            return true;
        }
        return false;
    }

    private static void debug_calledhasreachedtarget(float currentdistance) {
        //System.out.println("Called hasReachedTarget:currentdistance:" + currentdistance + " neededdistance" + neededProximityToTarget);
    }

    static boolean hasProbableLoop(){
        if(isPossibleLoopInBoundary && currentMode == Mode.BOUNDRY_BROWSE){
            debug_probablyloop();
            return true;
        }
        return false;
    }

    private static void debug_probablyloop() {
        System.out.println("Probable Loop");
    }
    static boolean hasReachedClosestBoundaryLocation(){
        return hasReachecdClosestBoundaryLocation;
    }

    static void terminate() {
        debug_calledterminate();
        endLocation = null;
    }

    private static void debug_calledterminate() {
        System.out.println("Called Terminate");
    }

    /**
     *  Call this function in every round to perform navigation to end
     returns
     * @return true if it moved
     * @throws GameActionException
     */
    static boolean Navigate() throws GameActionException {
        return Navigate(false);
    }

    public static boolean TryStepCloserToBoundary() throws GameActionException {

        currentLocation = rc.getLocation();
        Direction dirtoend = currentLocation.directionTo(endLocation);
        debug_printranTryStepCloserToBoundary();
        rc.setIndicatorLine(currentLocation,currentLocation.add(dirtoend,rc.getType().strideRadius + rc.getType().bodyRadius),255,255,255);
        if(!rc.hasMoved() && rc.canMove(dirtoend)){
            rc.move(dirtoend);
            //tryMove(dirtoend);
            if(!checkForNonFriendlyTreeOnBoundary(dirtoend,globalbeamsize)) {
                debug_printranTryStepCloserToBoundaryinsidefor();
                currentMode = Mode.TO_END_LOCATION;
                resetBoundaryParameters();
            }
            return true;
        }
        return false;

    }

    private static void debug_printranTryStepCloserToBoundary() {
        System.out.println("called :trystepclosertoboundary");

    }

    private static void debug_printranTryStepCloserToBoundaryinsidefor() {
        System.out.println("called :trystepclosertoboundary inside for");

    }

    static void debug_print(String s){
        System.out.println(s);
    }
    /**
     *  Call this function in every round to perform navigation to boundaryLocation
     returns
     * @return true if it moved
     * @throws GameActionException
     */
    static boolean Navigate(boolean toClosestBoundaryLocation) throws GameActionException {
        currentLocation = rc.getLocation();
        if (endLocation == null) {
            return false;
        }

        //debug_print("Navigate before reached target: " + currentLocation.distanceTo(endLocation)  + " : " + neededProximityToTarget);
        if(currentLocation.distanceTo(endLocation) <= neededProximityToTarget){
            debug_print("Navigate reached target TRUE!!");
            reachedTarget = true;
            terminate();
            return false;
        }

        rc.setIndicatorLine(currentLocation,endLocation,0,153,0);
        boolean moved = false;
        if(currentMode == Mode.TO_END_LOCATION){
            wallDirection1 = null;
            wallDirection2 = null;
            wallDirection3 = null;
            moved = tryMove(currentLocation.directionTo(endLocation));

        }else if(currentMode == Mode.BOUNDRY_BROWSE){// currentmode is Mode.BoundryBrowse


            if(toClosestBoundaryLocation == true && !hasReachecdClosestBoundaryLocation){
                moved = tryMove(currentLocation.directionTo(closestBoundaryToTargetLocation));
            }else{
                moved = tryMove(currentDirection);
            }


            // use a beam to check for wall and move
            float currectdistance = currentLocation.distanceTo(endLocation);
            //Heuristic used to check when to leave boundary

            float giveupsteps = 0.0f;
            if(rc.getType() == RobotType.GARDENER){
                giveupsteps = GameConstants.MAP_MAX_HEIGHT;
            }else{
                giveupsteps = GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT;
            }
            if((!blockedByObjectOrMapBoundary(wallDirection1,globalbeamsize) &&
                    !blockedByObjectOrMapBoundary(wallDirection2,globalbeamsize) &&
                    !blockedByObjectOrMapBoundary(wallDirection3,globalbeamsize))
                    || boundaryStartDistance - currectdistance >= 1 || boundaryStepCount >= giveupsteps){
                currentMode = Mode.TO_END_LOCATION;
                resetBoundaryParameters();
            }

            if(moved){
                boundaryStepCount++;
            }
        }


        rc.setIndicatorLine(currentLocation,currentLocation.add(currentDirection,2),0,0,0);
        debug_PrintDistandMode();
        return moved;
    }

    private static void debug_PrintDistandMode() {
        System.out.println(previousDistance + ":" + currentMode.toString());
    }

    /**
     * returns whether a beam from start to end is blocked at any location
     * @param startlocation
     * @param endLocation
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean blockedByObjectOrMapBoundary(MapLocation startlocation, MapLocation endLocation, float beamLength) throws GameActionException {
        Direction directiontoCheck = startlocation.directionTo(endLocation);

        MapLocation currentLocation;

        for(float i = rc.getType().bodyRadius+0.5f;i< rc.getType().bodyRadius + beamLength; i = i+1.0f){
            currentLocation = startlocation.add(directiontoCheck,i);
            rc.setIndicatorLine(startlocation,currentLocation,255,255,(int)(153+i));
            if(!rc.onTheMap(currentLocation) || rc.isLocationOccupied(currentLocation)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether a beam in direction directiontocheck is blocked by any object
     * @param directiontoCheck
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean blockedByObjectOrMapBoundary(Direction directiontoCheck, float beamLength) throws GameActionException {

        //float totaldistancetocheck = rc.getType().bodyRadius + beamLength;
        float totaldistancetocheck = beamLength;

        rc.setIndicatorLine(currentLocation,currentLocation.add(directiontoCheck,totaldistancetocheck),255,255,153);
        MapLocation newcurrentLocation;

        for(float i = rc.getType().bodyRadius+0.5f;i< totaldistancetocheck; i = i+1.0f){
            newcurrentLocation = currentLocation.add(directiontoCheck,i);
            if(!rc.canSenseLocation(newcurrentLocation))
                return false;
            rc.setIndicatorLine(currentLocation,newcurrentLocation,255,255,(int)(153+i));
            if(!rc.onTheMap(newcurrentLocation) || rc.isLocationOccupiedByTree(newcurrentLocation)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether a beam in direction directiontocheck is blocked
     * @param directiontoCheck
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean checkForNonFriendlyTreeOnBoundary(Direction directiontoCheck, float beamLength) throws GameActionException {

        MapLocation newcurrentLocation;
        TreeInfo currentTree;
        for(float i = rc.getType().bodyRadius+0.5f;i< rc.getType().bodyRadius + beamLength; i = i+1.0f){
            newcurrentLocation = currentLocation.add(directiontoCheck,i);
            rc.setIndicatorDot(newcurrentLocation,0,0,0);
            if(!rc.canSenseLocation(newcurrentLocation))
                return false;
            rc.setIndicatorLine(currentLocation,newcurrentLocation,255,255,(int)(153+i));
            if(rc.isLocationOccupiedByTree(newcurrentLocation)){
                //Check if the tree is our own tree and if so return false;
                currentTree = rc.senseTreeAtLocation(newcurrentLocation);
                if(currentTree.getTeam()==rc.getTeam()){
                    return false;
                }else{
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    private static boolean tryMove(Direction dir) throws GameActionException {

        return tryMove(dir,10,18);
    }


    private static DirectionVector getValidMoveDirectionVector(Direction direction, float mindistance, float maxdistance){

        if(maxdistance-mindistance<=0.35){

            if(rc.canMove(direction,maxdistance))
                return new DirectionVector(direction,maxdistance);
            else
            if(rc.canMove(direction,mindistance))
                return new DirectionVector(direction,mindistance);
            else
                return null;
        }
        float middistance = (maxdistance + mindistance) /2;
        if(rc.canMove(direction,maxdistance)){
            return new DirectionVector(direction,maxdistance);
        }
        else if(rc.canMove(direction,mindistance)){
            if(rc.canMove(direction,middistance)){
                return getValidMoveDirectionVector(direction,middistance,maxdistance);
            }else{
                return getValidMoveDirectionVector(direction,mindistance, middistance);
            }
        }

        return null;
    }
    private static Direction chosenDirection = null;
    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    private static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

        chosenDirection = dir;
        // First, try intended direction
        float mindistance = 0.1f;
        float maxdistance = rc.getType().strideRadius;
        DirectionVector canmovedirection = getValidMoveDirectionVector(dir,mindistance,maxdistance);
        if (!rc.hasMoved() && canmovedirection!=null) {
            rc.move(canmovedirection.direction,canmovedirection.distance);
            recalculateParameters(dir);

            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        //find a good direction to start
        Direction directiontoend = currentLocation.directionTo(endLocation);
        float degsbetween = dir.degreesBetween(directiontoend);
        boolean startLeft = (degsbetween>0);
        while(currentCheck<=checksPerSide) {
            if(startLeft) {
                // Try the offset of the left side
                if (tryMoveLeft(dir, degreeOffset, currentCheck)) return true;
                // Try the offset on the right side
                if (tryMoveRight(dir, degreeOffset, currentCheck)) return true;
            }else{
                // Try the offset on the right side
                if (tryMoveRight(dir, degreeOffset, currentCheck)) return true;
                // Try the offset of the left side
                if (tryMoveLeft(dir, degreeOffset, currentCheck)) return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }
        recalculateParameters(chosenDirection);
        // A move never happened, so return false.
        return false;
    }

    private static boolean tryMoveRight(Direction dir, float degreeOffset, int currentCheck) throws GameActionException {
        if(rc.hasMoved())
            return false;
        Direction rightRotatedDirection = dir.rotateRightDegrees(degreeOffset * currentCheck);

        float mindistance = 0.1f;
        float maxdistance = rc.getType().strideRadius;
        DirectionVector canmovedirection = getValidMoveDirectionVector(rightRotatedDirection,mindistance,maxdistance);

        if(canmovedirection!=null && rc.canMove(canmovedirection.direction, canmovedirection.distance)) {
            rc.move(canmovedirection.direction,canmovedirection.distance);
            recalculateParameters(rightRotatedDirection);
            chosenDirection = rightRotatedDirection;
            return true;
        }
        return false;
    }

    private static boolean tryMoveLeft(Direction dir, float degreeOffset, int currentCheck) throws GameActionException {
        if(rc.hasMoved())
            return false;
        Direction leftRotatedDirection = dir.rotateLeftDegrees(degreeOffset * currentCheck);
        float mindistance = 0.1f;
        float maxdistance = rc.getType().strideRadius;
        DirectionVector canmovedirection = getValidMoveDirectionVector(leftRotatedDirection,mindistance,maxdistance);

        if(canmovedirection!=null && rc.canMove(canmovedirection.direction, canmovedirection.distance)) {
            rc.move(canmovedirection.direction,canmovedirection.distance);
            recalculateParameters(leftRotatedDirection);
            chosenDirection = leftRotatedDirection;
            return true;
        }
        return false;
    }

    /**
     * THis function calculates certain parameters that are bookeeping to follow boundary
     * in boundary browse mode
     * @param dir
     * @throws GameActionException
     */
    private static void recalculateParameters(Direction dir) throws GameActionException {
        float newdistance = currentLocation.distanceTo(endLocation);

        TreeInfo[] nearbyTrees = rc.senseNearbyTrees(rc.getType().bodyRadius+1);
        //recheck if it can move in the target direction
        if(!rc.canMove(dir) && newdistance >= previousDistance && nearbyTrees.length>0 && currentMode == Mode.TO_END_LOCATION){
            currentMode = Mode.BOUNDRY_BROWSE;
            boundaryStartDistance = newdistance;
            boundaryStartLocation = currentLocation;
            boundaryStepCount = 0;
            isPossibleLoopInBoundary = false;
            hasReachecdClosestBoundaryLocation = false;
            closestBoundaryToTargetDistance = newdistance;
            closestBoundaryToTargetLocation = currentLocation;

            // pwall direction will be null the first time mode changes to boundary_browse and
            // there is a free movement in the direction the robot is going

            Direction directiontoend = currentLocation.directionTo(endLocation);
            float degsbetween = dir.degreesBetween(directiontoend);
            if(degsbetween < 0){
                wallDirection1 = dir.rotateRightDegrees(beamangletowall1);
                wallDirection2 = dir.rotateRightDegrees(beamangletowall2);
                wallDirection3 = dir.rotateRightDegrees(beamangletowall3);
            }else{
                wallDirection1 = dir.rotateLeftDegrees(beamangletowall1);
                wallDirection2 = dir.rotateLeftDegrees(beamangletowall2);
                wallDirection3 = dir.rotateLeftDegrees(beamangletowall3);
            }


            //just in case no wall is found
            if(!blockedByObjectOrMapBoundary(wallDirection1,globalbeamsize)
                    && !blockedByObjectOrMapBoundary(wallDirection2,globalbeamsize)
                    && !blockedByObjectOrMapBoundary(wallDirection3,globalbeamsize)){
//                wallDirection1 = wallDirection1.opposite();
//                wallDirection2 = wallDirection2.opposite();
//                wallDirection3 = wallDirection3.opposite();
                currentMode = Mode.TO_END_LOCATION;
            }
            rc.setIndicatorLine(currentLocation,currentLocation.add(wallDirection1,3),102,204,255);
            rc.setIndicatorLine(currentLocation,currentLocation.add(wallDirection2,3),102,204,255);
            rc.setIndicatorLine(currentLocation,currentLocation.add(wallDirection3,3),102,204,255);
            //}
        }
        else
        if(wallDirection1 !=null && currentMode == Mode.BOUNDRY_BROWSE){
            //it is going in intended direction so recalculate direction and walldirection
            //probe towards wall
            Direction newDirection=null;

            float mindistance = 0.1f;
            float maxdistance = rc.getType().strideRadius;
            DirectionVector canmovedirection = getValidMoveDirectionVector(dir,mindistance,maxdistance);

            if(canmovedirection!=null) {
                newDirection = probeTowardsWall(dir, wallDirection1);

            }else{
                newDirection = probeAwayFromWall(dir, wallDirection1);

                if(newDirection == null){
                    //newDirection = probeTowardsWallInward(dir,wallDirection1);
                }
            }

            if(newDirection == null){
                newDirection = dir;
            }

            if(newDirection!=null)
                currentDirection = newDirection;
            float anglesbetween = dir.degreesBetween(wallDirection1);
            if(anglesbetween < 0){
                wallDirection1 = currentDirection.rotateRightDegrees(beamangletowall1);
                wallDirection2 = currentDirection.rotateRightDegrees(beamangletowall2);
                wallDirection3 = currentDirection.rotateRightDegrees(beamangletowall3);
            }else{
                wallDirection1 = currentDirection.rotateLeftDegrees(beamangletowall1);
                wallDirection2 = currentDirection.rotateLeftDegrees(beamangletowall2);
                wallDirection3 = currentDirection.rotateLeftDegrees(beamangletowall3);
            }

            //if its a potential loop then set the flag
            if(boundaryStepCount >= minBoundaryStepCountBeforeCheckingForLoop
                    && currentLocation.distanceTo(boundaryStartLocation)<=1){
                isPossibleLoopInBoundary = true;
            }

            // if it is looking for the closest boundary and its reached
            if(closestBoundaryToTargetLocation!=null
                    && currentLocation.distanceTo(closestBoundaryToTargetLocation)<=1.0f){
                hasReachecdClosestBoundaryLocation = true;
            }

        }else{ // not it boundary_browse mode
            currentDirection = dir;
        }


        previousDistance = newdistance;
    }

    /**
     * Tries to probe towards the wall direction to see if a move is possible
     * @param currentmovedirection
     * @param wallDirection
     * @return
     * returns the direction that is moveable that is closest to the wall
     */
    private static Direction probeTowardsWall(Direction currentmovedirection, Direction wallDirection) {
        Direction currentvaliddirection = null;
        float anglesbetween = currentmovedirection.degreesBetween(wallDirection);

        int currentCheck = 1;

        Direction currentCheckDirection = null;
        while(currentCheck<=36) {
            if(anglesbetween < 0){
                currentCheckDirection = currentmovedirection.rotateRightDegrees(currentCheck * 2.5f);
            }else{
                currentCheckDirection = currentmovedirection.rotateLeftDegrees(currentCheck * 2.5f);
            }
            rc.setIndicatorLine(currentLocation,currentLocation.add(currentCheckDirection,5.0f),128,128,0);
            float mindistance = PROBE_MIN_DISTANCE;
            float maxdistance = rc.getType().strideRadius;
            DirectionVector canmovedirection = getValidMoveDirectionVector(currentCheckDirection,mindistance,maxdistance);
            if(canmovedirection!=null) {
                currentvaliddirection = currentCheckDirection;
            }
            else{
                rc.setIndicatorLine(currentLocation,currentLocation.add(currentvaliddirection,5.0f),128,128,0);
                return currentvaliddirection;
            }
            currentCheck++;
        }
        rc.setIndicatorLine(currentLocation,currentLocation.add(currentvaliddirection,5.0f),255,255,0);
        return currentvaliddirection;

    }


    /**
     * Tries to probe towards the wall direction to see if a move is possible
     * @param currentmovedirection
     * @param wallDirection
     * @return
     * returns the direction that is moveable that is closest to the wall
     */
    private static Direction probeTowardsWallInward(Direction currentmovedirection, Direction wallDirection) {
        Direction currentvaliddirection = null;
        float anglesbetween = currentmovedirection.degreesBetween(wallDirection);

        int currentCheck = 1;

        Direction currentCheckDirection = null;
        while(currentCheck<=45) {
            if(anglesbetween < 0){
                currentCheckDirection = currentmovedirection.rotateRightDegrees(currentCheck * 2);
            }else{
                currentCheckDirection = currentmovedirection.rotateLeftDegrees(currentCheck * 2);
            }
            rc.setIndicatorLine(currentLocation,currentLocation.add(currentCheckDirection,5.0f),128,128,0);
            float mindistance = PROBE_MIN_DISTANCE;
            float maxdistance = rc.getType().strideRadius;
            DirectionVector canmovedirection = getValidMoveDirectionVector(currentCheckDirection,mindistance,maxdistance);
            if(canmovedirection!=null) {
                currentvaliddirection = currentCheckDirection;
                return currentvaliddirection;
            }
            currentCheck++;
        }
        rc.setIndicatorLine(currentLocation,currentLocation.add(currentvaliddirection,5.0f),255,255,0);
        return currentvaliddirection;

    }

    /**
     * Tries to probe towards the wall direction to see if a move is possible
     * @param currentmovedirection
     * @param wallDirection
     * @return
     * returns the direction that is moveable that is closest to the wall
     */
    private static Direction probeAwayFromWall(Direction currentmovedirection, Direction wallDirection) {
        Direction currentvaliddirection = null;
        float anglesbetween = currentmovedirection.degreesBetween(wallDirection);

        int currentCheck = 1;

        Direction currentCheckDirection = null;
        while(currentCheck<=45) {
            if(anglesbetween > 0){
                currentCheckDirection = currentmovedirection.rotateRightDegrees(currentCheck * 2);
            }else{
                currentCheckDirection = currentmovedirection.rotateLeftDegrees(currentCheck * 2);
            }
            rc.setIndicatorLine(currentLocation,currentLocation.add(currentCheckDirection,5.0f),128,128,0);

            float mindistance = PROBE_MIN_DISTANCE;
            float maxdistance = rc.getType().strideRadius;
            DirectionVector canmovedirection = getValidMoveDirectionVector(currentCheckDirection,mindistance,maxdistance);

            if(canmovedirection!=null) {
                currentvaliddirection = currentCheckDirection;
                rc.setIndicatorLine(currentLocation,currentLocation.add(currentvaliddirection,5.0f),128,128,0);
                return currentvaliddirection;
            }

            currentCheck++;
        }
        rc.setIndicatorLine(currentLocation,currentLocation.add(currentvaliddirection,5.0f),255,255,0);
        return currentvaliddirection;

    }



}

