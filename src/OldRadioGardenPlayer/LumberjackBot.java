package OldRadioGardenPlayer;

import battlecode.common.*;

import java.util.Arrays;
import java.util.Comparator;

public strictfp class LumberjackBot extends BaseBot {

    //Communication variables
    private static int lastRequest = 0;
    private static int requestTimeout = 150;
    private static int treesClearedSinceRequest = 0;
    private static int chopsSinceRequest = 0;
    private static int lastRandomMoveTime = -100000;
    private static Direction lastRandomMoveDirection = null;
    private static boolean hasChopped = false;
    private static MapLocation currentLocation;

    public static void initialize() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_LUMBERJACK_HEALTH, (int) rc.getHealth());
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    public static void run() throws GameActionException {
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        currentLocation = rc.getLocation();
        boolean moved = false;
        hasChopped = false;
        // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
        RobotInfo[] enemiesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);
        RobotInfo[] friendliesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, rc.getTeam());

        if(enemiesWithinStrike.length >0 && enemiesWithinStrike.length >= friendliesWithinStrike.length && !rc.hasAttacked()) {
            // Use strike() to hit all nearby robots!
            rc.strike();
        } else {
            // No close robots, so search for robots within sight radius
            RobotInfo[] enemyWithinSight = rc.senseNearbyRobots(-1, enemy);
            Arrays.sort(enemyWithinSight, new Comparator<RobotInfo>() {
                @Override
                public int compare(RobotInfo o1, RobotInfo o2) {
                    if (o1.type == RobotType.SOLDIER && o2.type != RobotType.SOLDIER)
                        return -1;
                    if (o1.type == RobotType.TANK && o2.type != RobotType.SOLDIER && o2.type != RobotType.TANK)
                        return -1;
                    float f = o1.location.distanceTo(currentLocation) - o2.location.distanceTo(currentLocation);
                    if (f < 0) return -1;
                    else if (f > 0) return 1;
                    return 0;
                }
            });

            // If there is a enemy soldier or tank, move toward from it
            if (enemyWithinSight.length > 0 && Math.random() < 0.99) {

                MapLocation enemyLocation = enemyWithinSight[0].getLocation();
                rc.setIndicatorLine(currentLocation, enemyLocation, 100, 0, 100);
                Direction toEnemy = currentLocation.directionTo(enemyLocation);
                /*if (enemyWithinSight[0].type == RobotType.SOLDIER || enemyWithinSight[0].type == RobotType.TANK) {
                    if (moved = tryMove(toEnemy.opposite())) {
                        lastRandomMoveDirection = toEnemy.opposite();
                        lastRandomMoveTime = rc.getRoundNum() + 10;
                    }

                }*/
                tryMove(toEnemy);
            }

            //check for help requests
            if ((treesClearedSinceRequest > 50) ||
                    (treesClearedSinceRequest > 0 && lastRequest > 0.8*requestTimeout) ||
                    (lastRequest > requestTimeout) ||
                    (Navigation.isInitialized() && Navigation.hasReachedTarget() && rc.senseNearbyTrees(-1, Team.NEUTRAL).length == 0)
                    ) {
                RequestData rd = Radio.RadioRequest.getRequest(RobotType.LUMBERJACK);
                if (rd != null) {
                    debug_lumberjackreqeustresponse();
                    //if called by gardener assume needed proximity as distance from one garden
                    float proximitytolocation = RobotType.LUMBERJACK.bodyRadius + RobotType.GARDENER.bodyRadius + GameConstants.BULLET_TREE_RADIUS + 0.5f;
                    Navigation.initializeToLocation(rc, rd.location, proximitytolocation);
                    lastRequest = 0;
                    treesClearedSinceRequest = 0;
                    chopsSinceRequest = 0;
                }
            }
            ++lastRequest;

            if (!moved && !rc.hasAttacked()) {
                runLumberJackNavigation();
            }

            if ((!rc.hasAttacked() || Math.random() < 0.3) && !rc.hasMoved()) {
                rc.setIndicatorDot(currentLocation, 200, 200, 0);
                Direction dir = null;
                if (lastRandomMoveDirection == null || rc.getRoundNum() - lastRandomMoveTime > 5 ||
                        (rc.hasAttacked() && Math.random() < 0.8)) {
                    dir = randomDirection();
                } else {
                    dir = lastRandomMoveDirection;
                }

                if (!tryClearTreesInRange() && !moved) {
                    tryMove(dir);
                }

                //if (Math.random() < 0.2 && !rc.hasMoved()) tryMove(dir);

                if (!rc.hasMoved()) lastRandomMoveDirection = null;
                else { lastRandomMoveTime = rc.getRoundNum(); lastRandomMoveDirection = dir; }
            }
        }
        if(!hasChopped){
            tryClearTreeInInteractionRange();
        }
        requestHelp();
    }


    /**
     * THis function currently keeps the soldier in perpetual navigation between archon locations
     * @throws GameActionException
     */
    private static void runLumberJackNavigation() throws GameActionException {


        currentLocation = rc.getLocation();

        if(Navigation.isInitialized() && !Navigation.hasReachedTarget()) {
            // What should the bot do if there is a loop
            if (Navigation.hasProbableLoop()) {
                //It could move to the boundary location closest to the target and clear trees

                //if it hasent reached its boundary
                if (!Navigation.hasReachedClosestBoundaryLocation()) {
                    Navigation.Navigate(true);
                    tryClearTreesInRange();
                } else {//reached closestboundarylocation

                    rc.setIndicatorDot(currentLocation, 200, 0, 0);

                    if (!TryClearTreesInDirection(currentLocation.directionTo(Navigation.endLocation))) {
                        //if no tree was found try move towards endlocation
                        Navigation.TryStepCloserToBoundary();
                    }
                }

            } else {
                Navigation.Navigate();
            }
        }


    }

    private static void debug_lumberjackreqeustresponse() {
        System.out.println("[debug] Lumberjack coming!");
    }

    private static boolean TryClearTreesInDirection(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (checkAndClearTreeInDirection(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (checkAndClearTreeInDirection(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (checkAndClearTreeInDirection(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
        return false;
    }

    private static boolean checkAndClearTreeInDirection(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    tryClearTreeOrMoveToward(treeinfo);
                }else{
                    //this is my own team tree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }
    private static void navigateToDest(MapLocation loc) {
        if (!Navigation.isInitialized()) {
            Navigation.initializeToLocation(rc, loc, RobotType.LUMBERJACK.bodyRadius + 3.0f);

        }


    }
    private static boolean tryClearTreesInRange() throws GameActionException {

        TreeInfo[] treeswithinrange = rc.senseNearbyTrees(-1);

        for(int i=0; i<treeswithinrange.length;i++){
            if(treeswithinrange[i].getTeam() != rc.getTeam()){
                return tryClearTreeOrMoveToward(treeswithinrange[i]);
            }
        }
        return false;
    }

    private static boolean tryClearTreeOrMoveToward(TreeInfo treeInfo) throws GameActionException {
        if(rc.canInteractWithTree(treeInfo.ID)){
            if(!rc.hasAttacked() && rc.canChop(treeInfo.getID()) && hasChopped==false){
                if (treeInfo.getHealth() <= GameConstants.LUMBERJACK_CHOP_DAMAGE) treesClearedSinceRequest++;
                chopsSinceRequest++;
                rc.chop(treeInfo.getID());
                hasChopped = true;
                debug_showChopped();
                return true;
            }
        }else{
            tryMoveDirectionOrRandom(rc.getLocation().directionTo(treeInfo.getLocation()));
        }
        return false;
    }

    private static void debug_showChopped() throws GameActionException {
        rc.setIndicatorDot(rc.getLocation(),255, 153, 255);
    }

    private static boolean tryClearTreeInInteractionRange() throws GameActionException {

        TreeInfo[] surroundingTrees = rc.senseNearbyTrees(rc.getType().bodyRadius + 1.0f);

        if (surroundingTrees.length > 0) {
            for(int i =0;i<surroundingTrees.length;i++) {
                if (rc.canInteractWithTree(surroundingTrees[i].ID)) {

                    if (surroundingTrees[i].getTeam() != rc.getTeam() && !rc.hasAttacked() && rc.canChop(surroundingTrees[i].getID())) {
                        if (surroundingTrees[i].getHealth() <= GameConstants.LUMBERJACK_CHOP_DAMAGE)
                            treesClearedSinceRequest++;
                        chopsSinceRequest++;
                        rc.chop(surroundingTrees[i].getID());
                        hasChopped = true;
                        debug_showChopped();
                        return true;
                    }

                } else {
                    //cant interact
                    return false;
                }
            }
            return false;
        }
        return false;
    }

    private static final int requestDelay = 40;
    private static int lastRequestSoldier = -100;
    private static void requestHelp() throws GameActionException {
        RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(-1, enemy);
        if (nearbyEnemies.length > 0
                && rc.getRoundNum() - lastRequestSoldier > requestDelay) {
            Radio.RadioRequest.request(RobotType.SOLDIER, nearbyEnemies[0].getLocation());
            lastRequestSoldier = rc.getRoundNum();
            System.out.println("[debug] Requested soldier:" + Radio.RadioRequest.countRequests(RobotType.SOLDIER));
        }
    }

    private static float lastRoundHealth = RobotType.SOLDIER.maxHealth;
    private static void updateHealth() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_LUMBERJACK_HEALTH,
                ((int) Math.ceil(rc.getHealth() - lastRoundHealth)));
        lastRoundHealth = rc.getHealth();
    }
}
