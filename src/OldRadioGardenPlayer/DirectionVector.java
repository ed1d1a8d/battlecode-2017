package OldRadioGardenPlayer;

import battlecode.common.Direction;

/**
 * Created by jacob on 1/30/2017.
 */
public strictfp  class DirectionVector{
    public Direction direction = null;
    public  float distance = 0.0f;

    public DirectionVector(Direction pdirection, float pdistance){
        direction = pdirection;
        distance = pdistance;
    }
}
