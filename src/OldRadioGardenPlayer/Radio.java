package OldRadioGardenPlayer;

import battlecode.common.*;

public strictfp class Radio {
    private static RobotController rc;
    private static MapLocation globalCenter = null; //protected so testing class can access

    //Will be null if not initialized
    public static MapLocation getGlobalCenter() {
        return globalCenter;
    }

    //Does nothing if called more than once globally
    public static void GlobalInitialize(RobotController myrc, MapLocation center) throws GameActionException {
        rc = myrc;
        if (RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_SET) == 0) {
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_X, Float.floatToIntBits(center.x));
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_Y, Float.floatToIntBits(center.y));
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_SET, 1);
        }
    }

    //Throws exception if called before GlobalInitialize()
    public static void LocalInitialize(RobotController myrc) throws GameActionException {
        rc = myrc;
        if (RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_SET) == 1) {
            globalCenter = new MapLocation(
                                Float.intBitsToFloat(RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_X)),
                                Float.intBitsToFloat(RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_Y)));
        } else {
            throw new GameActionException(GameActionExceptionType.CANT_DO_THAT,
                    "Called Radio.LocalInitialize() before Radio.GlobalInitialize()");
        }
    }

    // Some flags. Sometimes returned by appropriate functions
    public static final int ERROR = -1;
    public static final int OUT_OF_BOUNDS = -2; //This is the return value for error
    public static final int EMPTY = -3;
    public static final int NOT_ENOUGH_BYTECODES = -4;

    /**
     *  Data is ordered [RadioSettings, RadioCustomChannels, RadioRequest, RadioStack]
     *  The size of everything but RadioStack is defined inside the corresponding class.
     *  RadioSetting and RadioCustomChannels sizes are defined by the size of their enums
     *  RadioHeatMap has a fixed size.
     *  PLEASE TRY TO USE METHODS PROVIDED BY THIS CLASS
     */

    /**
     * The RadioSetting is for use by this class only, and stores some internal values.
     * Avoid reading and writing to this block from outside this class -- the data there is meant for
     * internal use.
     */
    public strictfp static class RadioSettings {
        //Internal Setting enum
        //This is used like C-style enum, all values are treated as integers
        public enum Setting {
            GLOBAL_CENTER_SET,
            GLOBAL_CENTER_X,
            GLOBAL_CENTER_Y,
            STACK_BOTTOM,
            STACK_TOP
        }
        public static final int SIZE = Setting.values().length;
        public static final int START = 0;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        public static int read(Setting setting) throws GameActionException {
            return read(setting.ordinal());
        }

        public static void write(Setting setting, int val) throws GameActionException {
            write(setting.ordinal(), val);
        }
    }

    /**
     * A collection of random global variables that you want to have global access to
     * Add these variables to the enum channels.
     * Access the variable by calling read(Channel channel) and write(Channel channel, int val);
     */
    public strictfp static class RadioCustomChannels {
        public enum Channel {
            NUM_GARDENERS_TO_BUILD,
            NUM_LUMBERJACKS_TO_BUILD,
            NUM_SOLDIERS_TO_BUILD,
            NUM_SCOUTS_TO_BUILD,
            TOTAL_SOLDIER_HEALTH,
            TOTAL_GARDENER_HEALTH,
            TOTAL_LUMBERJACK_HEALTH
        }
        public static final int SIZE = Channel.values().length;
        public static final int START = RadioSettings.SIZE;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        public static int read(Channel channel) throws GameActionException {
            return read(channel.ordinal());
        }

        public static void write(Channel channel, int val) throws GameActionException {
            write(channel.ordinal(), val);
        }

        public static void add(Channel channel, int d) throws GameActionException {
            int cur = read(channel);
            write(channel, cur + d);
        }
    }

    /**
     * A structure that allows bots to request for backup.
     * Bots can request for a specific robot to back them up, by calling:
     *      request(MapLocation location, RobotType type)
     * This will request a robot of type type to the specified location
     * If a robot wishes to accept a request for help, they can call MapLocation getRequest(RobotType),
     * which will provide them with the location from which help was requested.
     */

    public strictfp static class RadioRequest {
        //Values initialized in static block
        public static final int[] ROBOT_STACK_SIZE;
        public static final int[] ROBOT_START;
        public static final int PREFIX; //used to store bottom and top of stacks
        public static final int SIZE; //total memory taken up
        public static final int START;
        static {
            ROBOT_STACK_SIZE = new int[RobotType.values().length];
            ROBOT_STACK_SIZE[RobotType.ARCHON.ordinal()] = 10;
            ROBOT_STACK_SIZE[RobotType.GARDENER.ordinal()] = 500;
            ROBOT_STACK_SIZE[RobotType.LUMBERJACK.ordinal()] = 500;
            ROBOT_STACK_SIZE[RobotType.SCOUT.ordinal()] = 500;
            ROBOT_STACK_SIZE[RobotType.SOLDIER.ordinal()] = 1000;
            ROBOT_STACK_SIZE[RobotType.TANK.ordinal()] = 500;

            ROBOT_START = new int[RobotType.values().length];
            for (int i = 1; i < ROBOT_START.length; i++) {
                ROBOT_START[i] = ROBOT_START[i - 1] + ROBOT_STACK_SIZE[i - 1];
            }

            PREFIX = 2 * RobotType.values().length;

            int sum = 0;
            for (RobotType robot : RobotType.values()) {
                sum += ROBOT_STACK_SIZE[robot.ordinal()];
            }
            SIZE = PREFIX + sum;

            START = RadioSettings.SIZE + RadioCustomChannels.SIZE;
        }

        private static int getBottom(RobotType robot) throws GameActionException {
            return rc.readBroadcast(START + 2 * robot.ordinal());
        }

        private static void setBottom(RobotType robot, int val) throws GameActionException {
            rc.broadcast(START + 2 * robot.ordinal(), val);
        }

        private static int getTop(RobotType robot) throws GameActionException  {
            return rc.readBroadcast(START + 2 * robot.ordinal() + 1);
        }

        private static void setTop(RobotType robot, int val) throws GameActionException  {
            rc.broadcast(START + 2 * robot.ordinal() + 1, val);
        }

        private static int read(RobotType robot, int idx) throws GameActionException {
            if (idx < 0 || idx > ROBOT_STACK_SIZE[robot.ordinal()]) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + PREFIX + ROBOT_START[robot.ordinal()] + idx);
        }

        //Does nothing if out of bounds
        private static void write(RobotType robot, int idx, int val) throws GameActionException {
            if (idx < 0 || idx > ROBOT_STACK_SIZE[robot.ordinal()]) {
                return;
            }
            rc.broadcast(START + PREFIX + ROBOT_START[robot.ordinal()] + idx, val);
        }

        private static void push(RobotType robot, int val) throws GameActionException {
            int bottom = getBottom(robot);
            int top = getTop(robot);

            write(robot, top % ROBOT_STACK_SIZE[robot.ordinal()], val);

            if (bottom + ROBOT_STACK_SIZE[robot.ordinal()] == top) {
                setBottom(robot, bottom + 1);
            }
            setTop(robot, top + 1);
        }

        private static int pop(RobotType robot) throws GameActionException {
            int bottom = getBottom(robot);
            int top = getTop(robot);
            if (bottom == top) {
                return EMPTY;
            }
            setTop(robot, top - 1);

            int ret = read(robot, (top - 1) % ROBOT_STACK_SIZE[robot.ordinal()]);
            return ret;
        }

        /*
         *  OUTSIDE API
         */

        public static int countRequests(RobotType robot) throws GameActionException {
            return getTop(robot) - getBottom(robot);
        }

        // Returns true or false depending on whether enough bytecodes to submit request
        public static Boolean request(RobotType robot, MapLocation location) throws GameActionException {
            if (Clock.getBytecodesLeft() < 180) { //actual amount is 166 actual
                System.out.println("[debug] Not enough bytcodes to RadioRequest.request()");
                return false;
            }

            push(robot, (new RequestData(location, rc.getRoundNum())).toIntBits());
            return true;
        }

        //Atomic, returns NOT_ENOUGH_BYTECODES if not enough bytecodes to complete operation
        //return null if empty or not enough bytecodes
        public static RequestData getRequest(RobotType robot) throws GameActionException {
            if (Clock.getBytecodesLeft() < 145) { // 130 actual
                System.out.println("[debug] Not enough bytcodes to RadioRequest.request()");
                return null;
            }

            int val = pop(robot);
            if (val == EMPTY) {
                return null; //returns self location if empty
            }
            return new RequestData(val);
        }
    }

    /**
     * The RadioStack is a stack. It has size RadioStack.SIZE.
     * You can push data on and pop data off.
     * New data is guaranteed to be pushed on.
     * If size is exceeded, the oldest data (bottom of stack) is thrown away.
     * It has the following methods:
     *     void push(int val);
     *     int pop();
     *
     * Note: There is no isEmpty() function is implemented due to ByteCode costs.
     *       Instead call pop until EMPTY is returned.
     */
    public strictfp static class RadioStack {
        public static final int START = RadioSettings.SIZE + RadioCustomChannels.SIZE + RadioRequest.SIZE;
        public static final int SIZE = GameConstants.BROADCAST_MAX_CHANNELS - START;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        //Atomic, does nothing if not enough bytecodes
        public static void push(int val) throws GameActionException {
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 78) { //TODO: Recheck bytecodes
                return;
            }
            //Takes 74 bytecodes
            int bottom = RadioSettings.read(RadioSettings.Setting.STACK_BOTTOM);
            int top = RadioSettings.read(RadioSettings.Setting.STACK_TOP);
            write(top % SIZE, val);
            if (bottom + SIZE == top) {
                RadioSettings.write(RadioSettings.Setting.STACK_BOTTOM, bottom + 1);
            }
            RadioSettings.write(RadioSettings.Setting.STACK_TOP, top + 1);
        }

        //Atomic, returns NOT_ENOUGH_BYTECODES if not enough bytecodes to complete operation
        //return EMPTY if empty
        public static int pop() throws GameActionException {
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 65) { //TODO: Recheck bytecodes
                return NOT_ENOUGH_BYTECODES;
            }
            //Takes 61 bytecodes from here
            int bottom = RadioSettings.read(RadioSettings.Setting.STACK_BOTTOM);
            int top = RadioSettings.read(RadioSettings.Setting.STACK_TOP);
            if (bottom == top) {
                return EMPTY;
            }
            RadioSettings.write(RadioSettings.Setting.STACK_TOP, top - 1);
            return read((top - 1) % SIZE);
        }
    }
}
