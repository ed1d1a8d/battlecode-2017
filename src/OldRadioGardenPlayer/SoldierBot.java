package OldRadioGardenPlayer;

import battlecode.common.*;

public class SoldierBot extends BaseBot {
    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static MapLocation currentLocation;

    private static MapLocation curNeighborhood = null;
    private static final float neighborhoodRadius = 2 * RobotType.SOLDIER.strideRadius;
    private static int lastNeighborhoodChange = 100000;

    private static int previousShot = 0;
    private static int previousShotDelay = 15;



    //Incomming Communication variables
    private static int lastRequest = 0;
    private static int requestTimeout = 20;

    //Outgoing Communication variables
    private static int lastRequestedLumberjack = -100;
    private static int requestDelay = 30;

    private static MapLocation chosenArchon;
    private static final int outpostDefendDistance = 13;

    public static void initialize() throws GameActionException {
        MapLocation close = enemyArchonLocations[0];
        for (MapLocation loc : enemyArchonLocations) {
            if (rc.getLocation().distanceTo(loc) < rc.getLocation().distanceTo(close)) {
                close = loc;
            }
        }
        Direction dir = rc.getLocation().directionTo(close);
        chosenArchon = rc.getLocation().add(dir, outpostDefendDistance);
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH, (int) rc.getHealth());
    }

    private static boolean hasShot = false;
    public static void run() throws GameActionException {
        checkAttackMode();
        currentLocation = rc.getLocation();
        if (curNeighborhood == null || curNeighborhood.distanceTo(currentLocation) > neighborhoodRadius) {
            curNeighborhood = currentLocation;
            lastNeighborhoodChange = rc.getRoundNum();
        }
        boolean hasmoved = false;
        hasShot = false;
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(7);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            MapLocation safeLoc = Dodge.dodgeBulletsRandom(rc.getLocation());
            if (rc.canMove(safeLoc))
                rc.move(safeLoc);
        }

        // See if there are any nearby enemy enemyRobots
        RobotInfo[] enemyRobots = rc.senseNearbyRobots(-1, enemy);

        //also use the nearby friendlies
        RobotInfo[] nearbyFriendlies = rc.senseNearbyRobots(-1, rc.getTeam());

        // If there are some...
        RobotType chosenRobotType = null;
        RobotInfo chosenEnemy = null;
        if (enemyRobots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...

            chosenEnemy = getClosestHostileShootableRobot(enemyRobots);

            RobotInfo oldEnemy = null;
            if (chosenEnemy == null && previousShotAtEnemyID != 0) {
                oldEnemy = getRobotWithId(enemyRobots, previousShotAtEnemyID);
                if (oldEnemy != null) {
                    //check if oldenemy is loosing health else change angle
                    if (previousShotAtEnemyHealth == oldEnemy.getHealth() && !hasmoved) {
                        if (generator.nextFloat() >= 0.5) {
                            hasmoved = tryMove(rc.getLocation().directionTo(oldEnemy.getLocation()).rotateRightDegrees(45));
                        } else {
                            hasmoved = tryMove(rc.getLocation().directionTo(oldEnemy.getLocation()).rotateLeftDegrees(45));
                        }
                    }
                    chosenEnemy = oldEnemy;
                }
            }

            if (chosenEnemy == null)
                chosenEnemy = enemyRobots[0];

            chosenRobotType = chosenEnemy.getType();
            if (!hasmoved) {
                if (chosenRobotType == RobotType.TANK || chosenRobotType == RobotType.SOLDIER) {
                    hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()).opposite());
                } else {
                    hasmoved = binarySearchMove(currentLocation.directionTo(chosenEnemy.getLocation()), 10);
                    if (!hasmoved) {
                        hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()));
                    }
                }
                hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()));

            }

            if (canShoot(chosenEnemy)) {
                shootAtRobot(chosenEnemy, enemyRobots, nearbyFriendlies);
            }

        }
        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
            previousShot = 0;
        }

        //check for help requests
        if (lastRequest > requestTimeout) {
            RequestData rd = Radio.RadioRequest.getRequest(RobotType.SOLDIER);
            if (rd != null) {
                debug_soldierreqeustresponse();
                //if called by gardener assume needed proximity as distance from one garden
                float proximitytolocation = RobotType.SOLDIER.bodyRadius + RobotType.GARDENER.bodyRadius + GameConstants.BULLET_TREE_RADIUS + 0.5f;
                Navigation.initializeToLocation(rc, rd.location,proximitytolocation);
                lastRequest = 0;
            }
        }
        ++lastRequest;
        ++previousShot;

        //Move away from nearby lumberjacks and soldiers


        if(nearbyFriendlies.length > 0){
            RobotInfo chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.LUMBERJACK);
            if (chosenRobot != null && !hasmoved){
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS +0.3f){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.SOLDIER);
            if(chosenRobot != null && !hasmoved){
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*3){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.TANK);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*2 + RobotType.TANK.bodyRadius){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.SCOUT);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*3){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.ARCHON);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*4) {
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
        }

        if (chosenEnemy != null && canShoot(chosenEnemy)) {
            shootAtRobot(chosenEnemy, enemyRobots, nearbyFriendlies);
        }

        if(!hasmoved && !rc.hasAttacked()) {
            runSoldierNavigation();
        }

        requestHelp();
        updateHealth();
        ++lastRequestedLumberjack;
    }

    private static RobotInfo getRobotWithId(RobotInfo[] robots, int id) {

        for(int i=0;i<robots.length;i++){
            if(robots[i].getID() == id){
                return robots[i];
            }
        }
        return null;
    }

    private static void debug_soldierreqeustresponse() {
         System.out.println("[debug] Soldier coming!");
    }

    private static RobotInfo getClosestHostileShootableRobot(RobotInfo[] enemyRobots) throws GameActionException {
        for (int i = 0;i<enemyRobots.length;i++){
            if(enemyRobots[i].getType() == RobotType.TANK ||
                    enemyRobots[i].getType() == RobotType.SOLDIER ||
                    enemyRobots[i].getType() == RobotType.LUMBERJACK ||
                    enemyRobots[i].getType() == RobotType.SCOUT) {
                if (canShoot(enemyRobots[i])) {
                    return enemyRobots[i];
                }
            }
        }
        return null;
    }


    private static final float shootNeutralTreesWhenStaleProb = 0.8f;
    private static final float shootNeutralTreesProb = 0.08f;
    private static boolean canShoot(RobotInfo targetRobot) throws GameActionException {
        // Check to see if friendly robots in the way
        MapLocation myLocation = rc.getLocation();
        Direction dir = myLocation.directionTo(targetRobot.getLocation());

        RobotInfo currentRobot = null;
        int neutralTreesInPath = 0;
        for (int i = 1; i <= rc.getLocation().distanceTo(targetRobot.getLocation()); i++) {
            if (rc.senseNearbyRobots(rc.getLocation().add(dir, i), 0.7f, myTeam).length > 0) {
                return false;
            }

            neutralTreesInPath += rc.senseNearbyTrees(rc.getLocation().add(dir, i), 0.5f, Team.NEUTRAL).length;
        }
        if (neutralTreesInPath > 0) {
            if (rc.getRoundNum() - lastNeighborhoodChange > 80) return Math.random() < shootNeutralTreesWhenStaleProb;
            else return Math.random() < shootNeutralTreesProb;
        }
        return true;
    }

    private static RobotInfo getClosestRobotOfType(RobotInfo[] robots, RobotType robotType) {
        for (int i = 0;i < robots.length;i++){
            if (robots[i].getType() == robotType && robots[i].getID() != rc.getID()){
                return robots[i];
            }
        }
        return null;
    }


    private static int loopDetectCount = 0;
    /**
     * THis function currently keeps the soldier in perpetual navigation between archon locations
     * @throws GameActionException
     */
    private static void runSoldierNavigation() throws GameActionException {
        //if it reached archon and archon not found... move to next
        if (!Navigation.isInitialized() || Navigation.hasReachedTarget()) {
            if (rc.getLocation().distanceTo(chosenArchon) < RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius) {
                Navigation.terminate();
                for (int i = 0; i < 20; i++) {
                    if (tryMove(randomDirection())) {
                        break;
                    }
                }
                return;
            } else {
                Navigation.initializeToLocation(rc, chosenArchon, RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius);
            }
        }


        if(!Navigation.hasReachedTarget()) {
            // What should the bot do if there is a loop
            if (Navigation.hasProbableLoop()) {
                //It could move to the boundary location closest to the target and clear trees

                if (Navigation.closestBoundaryToTargetLocation != null ) {
                    Navigation.terminate();
                }
                //if it hasent reached its boundary
//                if (!Navigation.hasReachedClosestBoundaryLocation()) {
//                    Navigation.Navigate(true);
//                } else {//reached closestboundarylocation
//                    rc.setIndicatorDot(currentLocation, 200, 0, 0);
//
//                    if (!TryClearTrees(currentLocation.directionTo(Navigation.endLocation))) {
//                        //if no tree was found try move towards endlocation
//                        Navigation.TryStepCloserToBoundary();
//                    }
//                }

            } else {
                Navigation.Navigate();
            }
        }
    }


    private static final int requestSendDelay = 20;
    private static MapLocation lastRequestLocation = null;
    private static int lastRequestSoldier = -100;
    private static void requestHelp() throws GameActionException {
        if (lastRequestLocation == null || lastRequestLocation.distanceTo(rc.getLocation()) > RobotType.SOLDIER.sensorRadius
                || rc.getRoundNum() - lastRequestSoldier > requestSendDelay) {
            RobotInfo[] nearbyRobots = rc.senseNearbyRobots(-1);
            float totEnemyHealth = 0;
            float totMyHealth = rc.getHealth();
            for (RobotInfo robot : nearbyRobots) {
                if (robot.team == myTeam) {
                    if (robot.getType() == RobotType.SOLDIER || robot.getType() == RobotType.TANK) {
                        totMyHealth += robot.health;
                    }
                } else {
                    if (robot.getType() == RobotType.SOLDIER
                            || robot.getType() == RobotType.TANK
                            || robot.getType() == RobotType.SCOUT
                            || robot.getType() == RobotType.LUMBERJACK) {
                        totEnemyHealth += robot.health;
                    }
                }
            }

            int numToRequest = (int) Math.ceil(totEnemyHealth / totMyHealth - 0.5);
            if (numToRequest > 0) {
                MapLocation requestTarget = rc.getLocation();
                for (RobotInfo robot : nearbyRobots) {
                    if (robot.team == enemy) {
                        requestTarget = robot.getLocation();
                        break;
                    }
                }
                for (int i = 0; i < numToRequest; i++) {
                    Radio.RadioRequest.request(RobotType.SOLDIER, requestTarget);
                }
                lastRequestLocation = rc.getLocation();
                lastRequestSoldier = rc.getRoundNum();
            }
        }

        if (rc.getRoundNum() - lastRequestedLumberjack > requestDelay) {
            TreeInfo[] nearbyTrees = rc.senseNearbyTrees(RobotType.SOLDIER.bodyRadius + 1, Team.NEUTRAL);
            if (nearbyTrees.length > 1) {
                lastRequestedLumberjack = rc.getRoundNum();
                Radio.RadioRequest.request(RobotType.LUMBERJACK, nearbyTrees[0].getLocation());
            }
        }
    }

    //returns false if no tree was found immediately in front
    //else fires at tree

    private static boolean TryClearTrees(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (CheckAndFireAtTree(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
        return false;
    }

    private static boolean CheckAndFireAtTree(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    if (rc.canFirePentadShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.firePentadShot(directiontocheck);
                    }
                    else if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(directiontocheck);
                    }
                }else{
                    //this is my own team tgree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }


    private static final float SHOT_BUFFER = 2.0f;
    private static boolean trishotbit = false;
    private static void shootAtRobot(RobotInfo robot,RobotInfo[] enemyRobots,RobotInfo[] nearbyFriendlies) throws GameActionException {
        float distancetorobot = rc.getLocation().distanceTo(robot.getLocation());


        boolean shouldshootmultiple = distancetorobot < SHOT_BUFFER + rc.getType().bodyRadius + robot.getType().bodyRadius
                || enemyRobots.length>nearbyFriendlies.length;


        if (!trishotbit && rc.canFireTriadShot() && robot.getType() != RobotType.ARCHON && shouldshootmultiple) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireTriadShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            trishotbit = true;
        }else if (trishotbit && rc.canFirePentadShot() && robot.getType() != RobotType.ARCHON && shouldshootmultiple) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            previousShot = 0;
            hasShot = true;
            trishotbit = false;
        } else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            previousShot = 0;
            hasShot = true;
        }
    }


    private static final int minUnitsToAttack = 10;
    private static boolean attackBit = false;
    private static void checkAttackMode() throws GameActionException {
        if (!attackBit) {
            float totHealth = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH);
            if (minUnitsToAttack * RobotType.SOLDIER.maxHealth < totHealth) {
                chosenArchon = enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)];
                Navigation.initializeToLocation(rc, chosenArchon, RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius);
                attackBit = true;
            }
        }
    }


    private static float lastRoundHealth = RobotType.SOLDIER.maxHealth;
    private static void updateHealth() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH,
                                            ((int) Math.ceil(rc.getHealth() - lastRoundHealth)));
        lastRoundHealth = rc.getHealth();
    }
}
