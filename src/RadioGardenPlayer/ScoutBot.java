package RadioGardenPlayer;

import battlecode.common.*;

public strictfp class ScoutBot extends BaseBot {
    private static MapLocation target;
    private static boolean[] seenEnemy = new boolean[32005];

    public static void initialize() {
        target = enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)];
    }

    private static float lastHealth;
    // Currently just moves around randomly while dodging
    public static void run() throws GameActionException {
        if (lastHealth != rc.getHealth()) {
            Dodge.dodgeBulletsRandom(rc.getLocation());
        }

        moveTowardsBullets();

        tryMove(rc.getLocation().directionTo(target));
        if (!rc.hasMoved()) {
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    break;
                }
            }
        }

        requestHelp();

        if (rc.getLocation().distanceTo(target) < RobotType.SCOUT.sensorRadius / 2) {
            target = enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)];
        }
        lastHealth = rc.getHealth();
    }


    private static void requestHelp() throws GameActionException {
        RobotInfo[] nearbyRobots = rc.senseNearbyRobots(-1, enemy);
        for (RobotInfo robot : nearbyRobots) {
            int id = robot.getID();
            if (!seenEnemy[id]) {
                seenEnemy[id] = true;
                Radio.RadioRequest.request(RobotType.SOLDIER, robot.getLocation());
                Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD, 1);
            }
        }
    }

    private static void moveTowardsBullets() throws GameActionException {
        TreeInfo[] trees = rc.senseNearbyTrees(-1, Team.NEUTRAL);
        TreeInfo best = null;
        for (TreeInfo tree : trees) {
            if (best == null || tree.containedBullets > best.containedBullets) {
                best = tree;
            }
        }
        if (best != null && best.containedBullets > 0) {
            rc.setIndicatorDot(best.getLocation(), 100, 0, 0);
            System.out.println("Bullets!\n");
            tryMove(rc.getLocation().directionTo(best.getLocation()));
        }
    }
}
