package RadioGardenPlayer;

import battlecode.common.*;

import java.util.ArrayList;

public strictfp class GardenerBot extends BaseBot {
    // List of behaviors and current behavior
    private enum Behavior {
        WANDER, // find a place to settle
        FARM, // farm and build other robots, 1 gap open
        TURTLE, // hole yourself off
        TANK // build tanks by leaving a gap of 2
    }
    private static Behavior curBehavior = Behavior.WANDER;
    private static TreeInfo[] myTrees = new TreeInfo[6];
    private static Direction safeDirection = null;
    private static boolean haveBuiltLumberjack = false;

    public static void initialize() throws GameActionException {
        for (int i = 0; i < 6; i++) {
            myTrees[i] = null;
        }
        MapLocation close = getCloseFarmLocation(5);
        if (close != null) {
            Navigation.initializeToLocation(rc, close, RobotType.GARDENER.bodyRadius + RobotType.GARDENER.strideRadius);
        }
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_GARDENER_HEALTH, (int) rc.getHealth());
    }


    public static void run() throws GameActionException {
        lastWords();
        updateStackSizeHistory();
        requestHelp();
        setSafeDirection();
        switch (curBehavior) {
            case WANDER:
                wander();
                break;
            case FARM:
                farm();
                break;
            case TURTLE:
                turtle();
                break;
            case TANK:
                tank();
                break;
        }
        checkEmergencyLumberjack();
        buildNeededUnits();
        updateHealth();
    }


    private static int wanderTime = 0;
    private static final int downgradeTime = 5; // time it takes for gardener to accept position with score one lower
    private static void wander() throws GameActionException {
        ++wanderTime;
        int valid = numValidLocations();
        if (valid + Math.min((wanderTime / downgradeTime), 6) >= 6) {
            curBehavior = Behavior.FARM;
            setSafeDirection();
            Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD, 1);
            return;
        }

        // first stage, we travel to target location
        if (Navigation.isInitialized()) {
            if (!Navigation.hasReachedTarget()) {
                if (Navigation.hasProbableLoop()) {
                    MapLocation close = getCloseFarmLocation(3);
                    if (close != null) {
                        Navigation.initializeToLocation(rc, close, RobotType.GARDENER.bodyRadius + RobotType.GARDENER.strideRadius);
                        wanderTime = 0;
                        return;
                    }
                } else {
                    Navigation.Navigate();
                    return;
                }
            }
            Navigation.terminate();
            wanderTime = 0;
        }

        // second stage we locally wander
        if (valid == -1) { // then near wall, move away from wall
            for (int i = 0; i < 4; i++) {
                Direction dir = new Direction((float) (i * Math.PI / 2));
                MapLocation query = rc.getLocation().add(dir, RobotType.GARDENER.bodyRadius + 3 * GameConstants.BULLET_TREE_RADIUS);
                if (!rc.onTheMap(query)) {
                    tryMove(dir.opposite());
                }
            }
        } else if (valid == -2) { // near another gardener, move away
            RobotInfo[] nearbyRobots = rc.senseNearbyRobots(-1, myTeam);
            for (RobotInfo robot : nearbyRobots) {
                if (robot.getType() == RobotType.GARDENER) {
                    Direction dir = rc.getLocation().directionTo(robot.getLocation()).opposite();
                    tryMove(dir);
                }
            }
        }
        if (!rc.hasMoved()) {
            tryMove(safeDirection);
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    break;
                }
            }
        }
        if (wanderTime / downgradeTime >= 6) {
            wanderTime = 0;
            MapLocation close = getCloseFarmLocation(5);
            if (close != null) {
                Navigation.initializeToLocation(rc, close, RobotType.GARDENER.bodyRadius + RobotType.GARDENER.strideRadius);
            } else {
                Navigation.initializeToLocation(rc, enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)],
                        RobotType.GARDENER.bodyRadius + RobotType.GARDENER.strideRadius);
            }
        }
    }

    private static void farm() throws GameActionException {
        senseTrees(); // important to do this before build, since build sets ignore to null
        buildTrees(1); // Leave one space for units
        waterMyTrees();
        reportOpenings();
        if (!isSafe()) {
            curBehavior = Behavior.TURTLE;
        }
    }

    private static void turtle() throws GameActionException {
        senseTrees(); // important to do this before build, since build sets ignore to null
        buildTrees(0); // Leave no spaces
        waterMyTrees();
        if (isSafe()) {
            curBehavior = Behavior.FARM;
        }
    }

    private static void tank() throws GameActionException {
        senseTrees(); // important to do this before build, since build sets ignore to null
        buildTrees(2); // Leave two spaces for tank
        waterMyTrees();
    }


    private static final int requestDelay = 40;
    private static int lastRequestSoldier = -100;
    private static int lastRequestLumberjack = -100;
    private static void requestHelp() throws GameActionException {
        //Enemies nearby
        RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(-1, enemy);
        if (nearbyEnemies.length > 0
                && rc.getRoundNum() - lastRequestSoldier > requestDelay) {
            Radio.RadioRequest.request(RobotType.SOLDIER, nearbyEnemies[0].getLocation());
            lastRequestSoldier = rc.getRoundNum();
            System.out.println("[debug] Requested soldier:" + Radio.RadioRequest.countRequests(RobotType.SOLDIER));
        }


        if (curBehavior == Behavior.FARM) {
            TreeInfo[] nearbyTrees =
                    rc.senseNearbyTrees(RobotType.GARDENER.bodyRadius + 2 * GameConstants.BULLET_TREE_RADIUS, Team.NEUTRAL);
            if (nearbyTrees.length > 0
                    && rc.getRoundNum() - lastRequestLumberjack > requestDelay) {
                Radio.RadioRequest.request(RobotType.LUMBERJACK, nearbyTrees[0].getLocation());
                lastRequestLumberjack = rc.getRoundNum();
                System.out.println("[debug] Requested lumberjack:" + Radio.RadioRequest.countRequests(RobotType.LUMBERJACK));
            }
        }
    }


    // Where can new trees be built, pushed onto Gardener Stack
    private static final int numOpeningChecks = 1;
    private static void reportOpenings() throws GameActionException {
        for (int i = 0; i < numOpeningChecks; i++) {
            Direction dir = randomDirection();
            MapLocation queryCenter = rc.getLocation().add(dir, 5);
            int cur = numValidLocations(queryCenter);
            if (cur * generator.nextFloat() > 4) { // 4/6 chance of reporting if valid position
                rc.setIndicatorDot(queryCenter, 100, 0, 0);
                Radio.RadioRequest.request(RobotType.GARDENER, queryCenter);
            }
        }
    }


    // Historical length of each stack size
    private static int stackSizeHistoryLength = 20; // How long of a history to keep
    private static int[][] stackSizeHistory = new int[RobotType.values().length][stackSizeHistoryLength];
    private static int[] historyBegin = new int[RobotType.values().length];
    private static int[] historyEnd = new int[RobotType.values().length];
    private static void updateStackSizeHistory() throws GameActionException {
        for (RobotType robot : RobotType.values()) {
            int idx = robot.ordinal();
            int beg = historyBegin[idx];
            int end = historyEnd[idx];
            stackSizeHistory[idx][end % stackSizeHistoryLength] = Radio.RadioRequest.countRequests(robot);

            if ((end - beg) % stackSizeHistoryLength == 0) {
                ++beg;
            }
            ++end;
            historyBegin[idx] = beg;
            historyEnd[idx] = end;
        }
    }


    private static final int buildDelay = 0; // two more turns than robot spawn time
    private static int lastBuild = -100;
    private static void buildNeededUnits() throws GameActionException {
        // Check to see if we should add more units to build queue (e.g. RobotCustomValues.NUM_LUMBERJACKS_TOBUILD)
        int[] changeSize = new int[RobotType.values().length];
        for (RobotType robot : RobotType.values()) {
            int idx = robot.ordinal();
            int beg = historyBegin[idx] % stackSizeHistoryLength;
            int end = (historyEnd[idx] + stackSizeHistoryLength - 1) % stackSizeHistoryLength;
            changeSize[idx] = stackSizeHistory[idx][end] - stackSizeHistory[idx][beg];
        }

        // Update build counters accordingly
        if (changeSize[RobotType.LUMBERJACK.ordinal()]
                > Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_LUMBERJACKS_TO_BUILD)) {
            Radio.RadioCustomChannels.write(Radio.RadioCustomChannels.Channel.NUM_LUMBERJACKS_TO_BUILD,
                                            changeSize[RobotType.LUMBERJACK.ordinal()]);
        }
        if (changeSize[RobotType.SOLDIER.ordinal()]
                > Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD)) {
            Radio.RadioCustomChannels.write(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD,
                    changeSize[RobotType.SOLDIER.ordinal()]);
        }
        if (changeSize[RobotType.SCOUT.ordinal()]
                > Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_SCOUTS_TO_BUILD)) {
            Radio.RadioCustomChannels.write(Radio.RadioCustomChannels.Channel.NUM_SCOUTS_TO_BUILD,
                    changeSize[RobotType.SCOUT.ordinal()]);
        }

        RobotType curBuild = null;
        // Choose a robot to build, currently picks one randomly
        if (rc.getRoundNum() - lastBuild > buildDelay) {
            int cntLumberjack = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_LUMBERJACKS_TO_BUILD);
            int cntSoldier = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD);
            int cntScout = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_SCOUTS_TO_BUILD);
            int tot = cntLumberjack + cntSoldier + cntScout;
            if (tot > 0) {
                int choose = generator.nextInt(tot);
                RobotType chosenRT = RobotType.LUMBERJACK;
                if (choose >= cntLumberjack) {
                    chosenRT = RobotType.SOLDIER;
                }
                if (choose >= cntLumberjack + cntSoldier) {
                    chosenRT = RobotType.SCOUT;
                }
                if (cntLumberjack > 0
                        && Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.TOTAL_LUMBERJACK_HEALTH)
                         < RobotType.LUMBERJACK.maxHealth) {
                    chosenRT = RobotType.LUMBERJACK;
                }
                curBuild = chosenRT;
            }
        }

        // Attempts to build chosen robot
        if (curBuild != null) {
            if (tryBuildUnit(curBuild)) {
                switch (curBuild) {
                    case LUMBERJACK:
                        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_LUMBERJACKS_TO_BUILD, -1);
                        break;
                    case SOLDIER:
                        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD, -1);
                        break;
                    case SCOUT:
                        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_SCOUTS_TO_BUILD, -1);
                        break;
                }
                lastBuild = rc.getRoundNum();
            }
        }
    }


    private static final int emergencyDuration = 30; // How long emergency state lasts.
    private static final int emergencyTrigger = 20; // How long you go without movement to trigger emergency
    private static MapLocation stuckLocation = null;
    private static int lastStuckRound = 5 - emergencyTrigger;
    private static boolean isEmergency = false;
    private static void checkEmergencyLumberjack() throws GameActionException {
        if (stuckLocation == null || rc.getLocation().distanceTo(stuckLocation) > 2 * RobotType.GARDENER.strideRadius) {
            stuckLocation = rc.getLocation();
            lastStuckRound = rc.getRoundNum();
        }
        if (!isEmergency && curBehavior == Behavior.WANDER && rc.getRoundNum() - lastStuckRound > emergencyTrigger) {
            TreeInfo[] nearbyTrees =
                    rc.senseNearbyTrees(RobotType.GARDENER.bodyRadius + 2 * GameConstants.BULLET_TREE_RADIUS, Team.NEUTRAL);
            if (nearbyTrees.length > 0) {
                isEmergency = true;
                tryBuildUnit(RobotType.LUMBERJACK);
            }
        }
        if (isEmergency && rc.getRoundNum() - lastStuckRound > emergencyDuration) {
            isEmergency = false;
        }
    }


    // request another gardener if below half health
    private static boolean requestedReplacement = false;
    private static void lastWords() throws GameActionException {
        if (!requestedReplacement && rc.getHealth() < RobotType.GARDENER.maxHealth / 2) {
            requestedReplacement = true;
            Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD, 1);
        }
    }


    private static float lastRoundHealth = RobotType.GARDENER.maxHealth;
    private static void updateHealth() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_GARDENER_HEALTH,
                ((int) Math.ceil(rc.getHealth() - lastRoundHealth)));
        lastRoundHealth = rc.getHealth();
    }

    // ======== HELPER FUNCTIONS BELOW ========

    // -2 = unacceptable, -1 = wall, other = normal obstacles
    private static int numValidLocations() throws GameActionException {
        RobotInfo[] nearbyRobots = rc.senseNearbyRobots(RobotType.GARDENER.sensorRadius, myTeam);
        for (RobotInfo robot : nearbyRobots) {
            if (robot.getType() == RobotType.GARDENER && robot.getID() != rc.getID()) {
                return -2; // too close to another gardener -- NOT OK
            }
            if (robot.getType() == RobotType.ARCHON && rc.getLocation().distanceTo(robot.getLocation()) < 6) {
                return -2; // too close to archon
            }
        }

        int ret = 0;
        for (int i = 0; i < 6; i++) {
            Direction dir = new Direction((float) (i * Math.PI / 3));
            MapLocation center = rc.getLocation().add(dir, RobotType.GARDENER.bodyRadius + GameConstants.BULLET_TREE_RADIUS);
            if (rc.onTheMap(rc.getLocation(), RobotType.GARDENER.bodyRadius + 3.5f * GameConstants.BULLET_TREE_RADIUS)) {
                if (!rc.isCircleOccupiedExceptByThisRobot(center, GameConstants.BULLET_TREE_RADIUS)) {
                    ++ret;
                }
            } else {
                return -1; // near wall is bad
            }
        }
        return ret;
    }

    private static int numValidLocations(MapLocation location) throws GameActionException {
        int ret = 0;
        for (int i = 0; i < 6; i++) {
            Direction dir = new Direction((float) (i * Math.PI / 3));
            MapLocation center = location.add(dir, RobotType.GARDENER.bodyRadius + GameConstants.BULLET_TREE_RADIUS - 0.01f);
            if (rc.onTheMap(center)) {
                if (!rc.isLocationOccupied(center)) {
                    ++ret;
                }
            } else {
                return -1; // near wall is bad
            }
        }
        return ret;
    }

    //set direction in which we leave gap
    private static void setSafeDirection() {
        MapLocation close = enemyArchonLocations[0];
        for (MapLocation loc : enemyArchonLocations) {
            if (rc.getLocation().distanceTo(loc) < rc.getLocation().distanceTo(close)) {
                close = loc;
            }
        }
        safeDirection = rc.getLocation().directionTo(close).opposite();
    }

    // ignore determines which positions to ignore: [0 ... ignore] going clockwise from safeDirection
    private static void buildTrees(int ignore) throws GameActionException{
        for (int i = 0; i < ignore; i++) {
            myTrees[i] = null;
        }
        for (int i = ignore; i < 6; i++) {
            Direction dir = safeDirection.rotateRightRads((float) (i * Math.PI / 3));
            if (rc.canPlantTree(dir)) {
                rc.plantTree(dir);
            }
        }
    }

    //Gets id's of trees around you
    private static void senseTrees() throws GameActionException {
        for (int i = 0; i < 6; i++) {
            Direction dir = safeDirection.rotateRightRads((float) (i * Math.PI / 3));
            myTrees[i] = rc.senseTreeAtLocation(
                    rc.getLocation().add(dir, RobotType.GARDENER.bodyRadius +  GameConstants.BULLET_TREE_RADIUS));
            if (myTrees[i] != null && myTrees[i].getTeam() == Team.NEUTRAL) {
                myTrees[i] = null;
            }
        }
    }

    // ignores null trees
    private static void waterMyTrees() throws GameActionException {
        float low = GameConstants.BULLET_TREE_MAX_HEALTH;
        int idx = -1;
        for (int i = 0; i < 6; i++) {
            if (myTrees[i] != null) {
                if (myTrees[i].getHealth() < low) {
                    low = myTrees[i].health;
                    idx = i;
                }
            }
        }

        if (idx != -1) {
            rc.water(myTrees[idx].getID());
        }
    }

    private static void waterFriendsTrees() throws GameActionException {
        TreeInfo[] trees = rc.senseNearbyTrees(RobotType.GARDENER.bodyRadius + 1, myTeam);
        TreeInfo low = null;
        for (TreeInfo tree : trees) {
            if (low == null || tree.health < low.health) {
                if (rc.canWater(tree.getID())) {
                    low = tree;
                }
            }
        }
        if (low != null) {
            rc.water(low.getID());
        }
    }

    // returns null if no candidates
    private static MapLocation getCloseFarmLocation(int numCandidates) throws GameActionException {
        ArrayList<MapLocation> putBack = new ArrayList<>();
        MapLocation close = null;
        for (int i = 0; i < numCandidates; i++) {
            RequestData data = Radio.RadioRequest.getRequest(RobotType.GARDENER);
            if (data != null) {
                if (close == null) {
                    close = data.location;
                } else if (rc.getLocation().distanceTo(data.location) < rc.getLocation().distanceSquaredTo(close)) {
                    putBack.add(close);
                    close = data.location;
                } else {
                    putBack.add(close);
                }
            }
        }
        for (MapLocation loc : putBack) {
            Radio.RadioRequest.request(RobotType.GARDENER, loc);
        }
        return close;
    }

    // returns true if successful, false if not
    private static boolean tryBuildUnit(RobotType robot) throws GameActionException {
        if (safeDirection != null) {
            if (rc.canBuildRobot(robot, safeDirection)) {
                rc.buildRobot(robot, safeDirection);
                return true;
            } else {
                for (int i = 0; i < 20; i++) {
                    Direction dir = randomDirection();
                    if (rc.canBuildRobot(robot, dir)) {
                        rc.buildRobot(robot, dir);
                        return true;
                    }
                }
            }
        }

        if (isEmergency) {
            TreeInfo[] trees = rc.senseNearbyTrees(RobotType.GARDENER.bodyRadius + 2 * robot.bodyRadius, Team.NEUTRAL);
            MapLocation myLoc = rc.getLocation();
            for (int i = 0; i < trees.length; i++) {
                Direction cur = myLoc.directionTo(trees[i].getLocation());
                Direction nxt = myLoc.directionTo(trees[(i + 1) % trees.length].getLocation());
                Direction avg = new Direction((cur.radians + nxt.radians) / 2);
                if (rc.canBuildRobot(robot, avg)) {
                    rc.buildRobot(robot, avg);
                    return true;
                }
            }
        }

        return false;
    }

    private static final float unsafeAngle = (float) Math.PI / 4; // on either side of safeAngle
    private static boolean isSafe() throws GameActionException {
        RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(-1, enemy);
        for (RobotInfo robot : nearbyEnemies) {
            Direction dir = rc.getLocation().directionTo(robot.getLocation());
            float anglediff = safeDirection.radiansBetween(dir);
            if (Math.abs(anglediff) < unsafeAngle) {
                return false;
            }
        }
        return true;
    }

}
