package RadioGardenPlayer;

import battlecode.common.*;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;

public class SoldierBot extends BaseBot {
    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static MapLocation currentLocation;

    private static MapLocation curNeighborhood = null;
    private static final float neighborhoodRadius = 2 * RobotType.SOLDIER.strideRadius;
    private static int lastNeighborhoodChange = 100000;

    private static int previousShot = 0;
    private static int previousShotDelay = 15;



    //Incomming Communication variables
    private static int lastRequest = 0;
    private static int requestTimeout = 20;

    //Outgoing Communication variables
    private static int lastRequestedLumberjack = -100;
    private static int requestDelay = 30;

    private static MapLocation chosenArchon;
    private static final int outpostDefendDistance = 13;

    public static void initialize() throws GameActionException {
        MapLocation close = enemyArchonLocations[0];
        for (MapLocation loc : enemyArchonLocations) {
            if (rc.getLocation().distanceTo(loc) < rc.getLocation().distanceTo(close)) {
                close = loc;
            }
        }
        Direction dir = rc.getLocation().directionTo(close);
        chosenArchon = rc.getLocation().add(dir, outpostDefendDistance);
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH, (int) rc.getHealth());
    }

    private static boolean hasShot = false;
    public static void run() throws GameActionException {
        checkAttackMode();
        currentLocation = rc.getLocation();
        if (curNeighborhood == null || curNeighborhood.distanceTo(currentLocation) > neighborhoodRadius) {
            curNeighborhood = currentLocation;
            lastNeighborhoodChange = rc.getRoundNum();
        }
        boolean hasmoved = false;
        hasShot = false;
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(7);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            MapLocation safeLoc = Dodge.dodgeBulletsRandom(rc.getLocation());
            rc.setIndicatorDot(safeLoc, 100, 0, 0);
            if (rc.canMove(safeLoc))
                rc.move(safeLoc);
        }

        // See if there are any nearby enemy enemyRobots
        RobotInfo[] enemyRobots = rc.senseNearbyRobots(-1, enemy);

        //also use the nearby friendlies
        RobotInfo[] nearbyFriendlies = rc.senseNearbyRobots(-1, myTeam);
        TreeInfo[] friendTrees = rc.senseNearbyTrees(-1, myTeam);
        TreeInfo[] neutralTrees = rc.senseNearbyTrees(-1, Team.NEUTRAL);

        // If there are some...
        RobotType chosenRobotType = null;
        RobotInfo chosenEnemy = null;
        Target target = null;
        if (enemyRobots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...

            target = getClosestHostileShootableRobot(enemyRobots, nearbyFriendlies, friendTrees, neutralTrees);
            if (target != null) chosenEnemy = target.enemy;

            RobotInfo oldEnemy = null;
            if (chosenEnemy == null && previousShotAtEnemyID != 0) {
                oldEnemy = getRobotWithId(enemyRobots, previousShotAtEnemyID);
                if (oldEnemy != null) {
                    //check if oldenemy is loosing health else change angle
                    if (previousShotAtEnemyHealth == oldEnemy.getHealth() && !hasmoved) {
                        if (generator.nextFloat() >= 0.5) {
                            hasmoved = tryMove(rc.getLocation().directionTo(oldEnemy.getLocation()).rotateRightDegrees(45));
                        } else {
                            hasmoved = tryMove(rc.getLocation().directionTo(oldEnemy.getLocation()).rotateLeftDegrees(45));
                        }
                    }
                    chosenEnemy = oldEnemy;
                }
            }

            if (chosenEnemy == null)
                chosenEnemy = enemyRobots[0];

            chosenRobotType = chosenEnemy.getType();
            if (!hasmoved) {
                if (chosenRobotType == RobotType.TANK || chosenRobotType == RobotType.SOLDIER) {
                    hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()).opposite());
                } else {
                    hasmoved = binarySearchMove(currentLocation.directionTo(chosenEnemy.getLocation()), 10);
                    if (!hasmoved) {
                        hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()));
                    }
                }
                hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()));

            }

            /*if (canShoot(chosenEnemy)) {
                shootAtRobot(chosenEnemy, enemyRobots, nearbyFriendlies);
            }*/

        }
        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
            previousShot = 0;
        }

        //check for help requests
        if (lastRequest > requestTimeout) {
            RequestData rd = Radio.RadioRequest.getRequest(RobotType.SOLDIER);
            if (rd != null) {
                debug_soldierreqeustresponse();
                //if called by gardener assume needed proximity as distance from one garden
                float proximitytolocation = RobotType.SOLDIER.bodyRadius + RobotType.GARDENER.bodyRadius + GameConstants.BULLET_TREE_RADIUS + 0.5f;
                Navigation.initializeToLocation(rc, rd.location,proximitytolocation);
                lastRequest = 0;
            }
        }
        ++lastRequest;
        ++previousShot;

        //Move away from nearby lumberjacks and soldiers


        if(nearbyFriendlies.length > 0){
            RobotInfo chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.LUMBERJACK);
            if (chosenRobot != null && !hasmoved){
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS +0.3f){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.SOLDIER);
            if(chosenRobot != null && !hasmoved){
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*3){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.TANK);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*2 + RobotType.TANK.bodyRadius){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.SCOUT);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*3){
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
            chosenRobot = getClosestRobotOfType(nearbyFriendlies, RobotType.ARCHON);
            if(chosenRobot != null && !hasmoved) {
                if(currentLocation.distanceTo(chosenRobot.getLocation())<=RobotType.SOLDIER.bodyRadius*4) {
                    hasmoved = tryMoveDirectionOrRandom(currentLocation.directionTo(chosenRobot.getLocation()).opposite());
                }
            }
        }
        if (target == null && chosenEnemy != null) {
            target = getClosestHostileShootableRobot(new RobotInfo[]{chosenEnemy},
                    nearbyFriendlies, friendTrees, neutralTrees);
        }
        if (target != null) {
            shootAtRobot(target.enemy, target.shotType, enemyRobots, nearbyFriendlies);
        }

        if(!hasmoved && !rc.hasAttacked()) {
            runSoldierNavigation();
        }

        requestHelp();
        updateHealth();
        ++lastRequestedLumberjack;
    }

    private static RobotInfo getRobotWithId(RobotInfo[] robots, int id) {

        for(int i=0;i<robots.length;i++){
            if(robots[i].getID() == id){
                return robots[i];
            }
        }
        return null;
    }

    private static void debug_soldierreqeustresponse() {
         System.out.println("[debug] Soldier coming!");
    }

    private static int typeToNum(RobotType t) {
        if (t == RobotType.SOLDIER) return 0;
        if (t == RobotType.TANK) return 1;
        if (t == RobotType.LUMBERJACK) return 2;
        if (t == RobotType.GARDENER) return 3;
        if (t == RobotType.SCOUT) return 4;
        if (t == RobotType.ARCHON) return 5;
        return 6;
    }
    enum ShootTypes {SINGLE, TRIAD, PENTAD }
    static class Target {
        public Target(RobotInfo e, ShootTypes s) {
            enemy = e;
            shotType = s;
        }
        public RobotInfo enemy;
        public ShootTypes shotType;
    }

    private static Target getClosestHostileShootableRobot(RobotInfo[] enemyRobots,
                                                             RobotInfo[] friendlies,
                                                             TreeInfo[] friendTrees,
                                                             TreeInfo[] neutralTrees) throws GameActionException {
        Arrays.sort(enemyRobots, new Comparator<RobotInfo>() {
            @Override
            public int compare(RobotInfo o1, RobotInfo o2) {
                int td = typeToNum(o1.type)-typeToNum(o2.type);
                if (td != 0) return td;
                float f = o1.location.distanceSquaredTo(currentLocation)
                        - o2.location.distanceSquaredTo(currentLocation);
                if (f < 0) return -1;
                if (f > 0) return 1;
                return 0;
            }
        });
        for (int i = 0;i<enemyRobots.length;i++ ) {
            if (canShoot(enemyRobots[i], 35, friendlies, friendTrees, neutralTrees))
                return new Target(enemyRobots[i], ShootTypes.PENTAD);

            if (canShoot(enemyRobots[i], 23, friendlies, friendTrees, neutralTrees))
                return new Target(enemyRobots[i], ShootTypes.TRIAD);

            if (canShoot(enemyRobots[i], 3, friendlies, friendTrees, neutralTrees))
                return new Target(enemyRobots[i], ShootTypes.SINGLE);
        }
        return null;
    }

    private static boolean canShoot(RobotInfo targetRobot, float angle, RobotInfo[] friendlies,
                                    TreeInfo[] friendTrees, TreeInfo[] neutralTrees) throws GameActionException {
        //0 < angle < 90
        // Check to see if friendly robots in the way
        currentLocation = rc.getLocation();
        float dist = currentLocation.distanceTo(targetRobot.location) - targetRobot.getRadius() - RobotType.SOLDIER.bodyRadius;
        if (dist <= 1.25) return true;
        Direction dir = currentLocation.directionTo(targetRobot.getLocation());
        for (RobotInfo friend : friendlies) {
            Direction d = currentLocation.directionTo(friend.location);
            MapLocation l1 = friend.location.add(d.rotateLeftDegrees(90), friend.type.bodyRadius);
            MapLocation l2 = friend.location.add(d.rotateRightDegrees(90), friend.type.bodyRadius);
            float d2f = currentLocation.distanceTo(friend.location) - friend.type.bodyRadius - RobotType.SOLDIER.bodyRadius;
            float af = Math.min(Math.abs(dir.degreesBetween(currentLocation.directionTo(friend.location))),
                                Math.min(Math.abs(dir.degreesBetween(currentLocation.directionTo(l1))),
                                        Math.abs(dir.degreesBetween(currentLocation.directionTo(l2)))));

            if (d2f > 1.4 * dist) continue;
            if (d2f <= 0.2 && af < 50) return false;
            if (d2f <= 0.6 && af < 40) return false;
            if (d2f <= 0.5 && af < 15) {
                //System.out.println("DONT SHOOT!");
                return false;
            }


            if (af < angle) {

                /*rc.setIndicatorLine(currentLocation, targetRobot.location, 250, 0, 0);
                rc.setIndicatorLine(currentLocation, friend.location, 0, 250, 0);
                System.out.println("DONT SHOOT2! " + af);*/

                return false;
            }
        }
        for (TreeInfo t : friendTrees) {
            Direction d = currentLocation.directionTo(t.location);
            float d2f = currentLocation.distanceTo(t.location) - t.getRadius() - RobotType.SOLDIER.bodyRadius;
            float af = Math.abs(d.degreesBetween(dir));

            if (d2f  > 1.1*dist) continue;

            if (d2f <= 0.2 && af < 50) return false;
            if (d2f <= 0.6 && af < 40) return false;
            if (d2f <= 3.25 && af < 15) return false;
            if (af < angle) {
                return false;
            }
        }
        for (TreeInfo t : neutralTrees) {
            Direction d = currentLocation.directionTo(t.location);
            float d2f = currentLocation.distanceTo(t.location) - t.getRadius() - RobotType.SOLDIER.bodyRadius;
            float af = Math.abs(d.degreesBetween(dir));
            if (d2f >= dist) continue;
            if (d2f <= 0.2 && af < 50) return false;
            if (d2f <= 0.6 && af < 40) return false;
            if (d2f <= 1.25 && af < 15) return false;
            if (af < 0.4*angle && Math.random() < 0.9) {
                return false;
            }
        }
        return true;
    }

    private static RobotInfo getClosestRobotOfType(RobotInfo[] robots, RobotType robotType) {
        for (int i = 0;i < robots.length;i++){
            if (robots[i].getType() == robotType && robots[i].getID() != rc.getID()){
                return robots[i];
            }
        }
        return null;
    }


    private static int loopDetectCount = 0;
    /**
     * THis function currently keeps the soldier in perpetual navigation between archon locations
     * @throws GameActionException
     */
    private static void runSoldierNavigation() throws GameActionException {
        //if it reached archon and archon not found... move to next
        if (!Navigation.isInitialized() || Navigation.hasReachedTarget()) {
            if (rc.getLocation().distanceTo(chosenArchon) < RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius) {
                Navigation.terminate();
                for (int i = 0; i < 20; i++) {
                    if (tryMove(randomDirection())) {
                        break;
                    }
                }
                return;
            } else {
                Navigation.initializeToLocation(rc, chosenArchon, RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius);
            }
        }


        if(!Navigation.hasReachedTarget()) {
            // What should the bot do if there is a loop
            if (Navigation.hasProbableLoop()) {
                //It could move to the boundary location closest to the target and clear trees

                if (Navigation.closestBoundaryToTargetLocation != null ) {
                    Navigation.terminate();
                }
                //if it hasent reached its boundary
//                if (!Navigation.hasReachedClosestBoundaryLocation()) {
//                    Navigation.Navigate(true);
//                } else {//reached closestboundarylocation
//                    rc.setIndicatorDot(currentLocation, 200, 0, 0);
//
//                    if (!TryClearTrees(currentLocation.directionTo(Navigation.endLocation))) {
//                        //if no tree was found try move towards endlocation
//                        Navigation.TryStepCloserToBoundary();
//                    }
//                }

            } else {
                Navigation.Navigate();
            }
        }
    }


    private static final int requestSendDelay = 20;
    private static MapLocation lastRequestLocation = null;
    private static int lastRequestSoldier = -100;
    private static void requestHelp() throws GameActionException {
        if (lastRequestLocation == null || lastRequestLocation.distanceTo(rc.getLocation()) > RobotType.SOLDIER.sensorRadius
                || rc.getRoundNum() - lastRequestSoldier > requestSendDelay) {
            RobotInfo[] nearbyRobots = rc.senseNearbyRobots(-1);
            float totEnemyHealth = 0;
            float totMyHealth = rc.getHealth();
            for (RobotInfo robot : nearbyRobots) {
                if (robot.team == myTeam) {
                    if (robot.getType() == RobotType.SOLDIER || robot.getType() == RobotType.TANK) {
                        totMyHealth += robot.health;
                    }
                } else {
                    if (robot.getType() == RobotType.SOLDIER
                            || robot.getType() == RobotType.TANK
                            || robot.getType() == RobotType.SCOUT
                            || robot.getType() == RobotType.LUMBERJACK) {
                        totEnemyHealth += robot.health;
                    }
                }
            }

            int numToRequest = (int) Math.ceil(totEnemyHealth / totMyHealth - 0.5);
            if (numToRequest > 0) {
                MapLocation requestTarget = rc.getLocation();
                for (RobotInfo robot : nearbyRobots) {
                    if (robot.team == enemy) {
                        requestTarget = robot.getLocation();
                        break;
                    }
                }
                for (int i = 0; i < numToRequest; i++) {
                    Radio.RadioRequest.request(RobotType.SOLDIER, requestTarget);
                }
                lastRequestLocation = rc.getLocation();
                lastRequestSoldier = rc.getRoundNum();
            }
        }

        if (rc.getRoundNum() - lastRequestedLumberjack > requestDelay) {
            TreeInfo[] nearbyTrees = rc.senseNearbyTrees(RobotType.SOLDIER.bodyRadius + 1, Team.NEUTRAL);
            if (nearbyTrees.length > 1) {
                lastRequestedLumberjack = rc.getRoundNum();
                Radio.RadioRequest.request(RobotType.LUMBERJACK, nearbyTrees[0].getLocation());
            }
        }
    }

    //returns false if no tree was found immediately in front
    //else fires at tree

    private static boolean TryClearTrees(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (CheckAndFireAtTree(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
        return false;
    }

    private static boolean CheckAndFireAtTree(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    if (rc.canFirePentadShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.firePentadShot(directiontocheck);
                    }
                    else if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(directiontocheck);
                    }
                }else{
                    //this is my own team tgree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }


    private static final float SHOT_BUFFER = 2.0f;
    private static boolean trishotbit = false;
    private static void shootAtRobot(RobotInfo robot, ShootTypes shotType, RobotInfo[] enemyRobots, RobotInfo[] nearbyFriendlies) throws GameActionException {
        float distancetorobot = rc.getLocation().distanceTo(robot.getLocation());


        /*boolean shouldshootmultiple = distancetorobot < SHOT_BUFFER + rc.getType().bodyRadius + robot.getType().bodyRadius
                || enemyRobots.length>nearbyFriendlies.length;*/
        Direction dir = currentLocation.directionTo(robot.location);
        ShootTypes[] tys = ShootTypes.values();
        int i = shotType.ordinal();
        for ( ; i >= 0; --i) {
            ShootTypes typ = tys[i];
            if (typ == ShootTypes.PENTAD && rc.canFirePentadShot()) {
                rc.firePentadShot(dir);
                break;
            } else if (typ == ShootTypes.TRIAD && rc.canFireTriadShot()) {
                rc.fireTriadShot(dir);
                break;
            } else if (typ == ShootTypes.SINGLE && rc.canFireSingleShot()) {
                rc.fireSingleShot(dir);
                break;
            }
        }

        hasShot = i >= 0;
        if (hasShot) trishotbit = i == 1;
        if (hasShot) {
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
    }


    private static final int minUnitsToAttack = 10;
    private static boolean attackBit = false;
    private static void checkAttackMode() throws GameActionException {
        if (!attackBit) {
            float totHealth = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH);
            if (minUnitsToAttack * RobotType.SOLDIER.maxHealth < totHealth) {
                chosenArchon = enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)];
                Navigation.initializeToLocation(rc, chosenArchon, RobotType.SOLDIER.bodyRadius + 2 * RobotType.SOLDIER.strideRadius);
                attackBit = true;
            }
        }
    }


    private static float lastRoundHealth = RobotType.SOLDIER.maxHealth;
    private static void updateHealth() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.TOTAL_SOLDIER_HEALTH,
                                            ((int) Math.ceil(rc.getHealth() - lastRoundHealth)));
        lastRoundHealth = rc.getHealth();
    }
}
