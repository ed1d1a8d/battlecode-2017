package RadioGardenPlayer;

import battlecode.common.Clock;
import battlecode.common.RobotController;

public strictfp class RobotPlayer {

    public static void run(RobotController rc) {
        try {
            BaseBot.InitializeRobot(rc);
            switch (rc.getType()) {
                case ARCHON:
                    ArchonBot.initialize();
                    break;
                case GARDENER:
                    GardenerBot.initialize();
                    break;
                case SOLDIER:
                    SoldierBot.initialize();
                    break;
                case SCOUT:
                    ScoutBot.initialize();
                    break;
                case LUMBERJACK:
                    LumberjackBot.initialize();
                    break;
                case TANK:
                    TankBot.initialize();
                    break;
            }

            while (true) {
                BaseBot.checkVictoryPointWin();
                BaseBot.donateVictoryPoints();
                switch (rc.getType()) {
                    case ARCHON:
                        ArchonBot.run();
                        break;
                    case GARDENER:
                        GardenerBot.run();
                        break;
                    case SOLDIER:
                        SoldierBot.run();
                        break;
                    case SCOUT:
                        ScoutBot.run();
                        break;
                    case LUMBERJACK:
                        LumberjackBot.run();
                        break;
                    case TANK:
                        TankBot.run();
                        break;
                }
                BaseBot.shakeTreeIfPossible();
                Clock.yield();
            }
        } catch (Exception e) {
            System.out.println("Archon Exception");
            e.printStackTrace();
        }
    }
}

