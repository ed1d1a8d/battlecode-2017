package RadioGardenPlayer;

import battlecode.common.*;

public strictfp class ArchonBot extends BaseBot {
    private static boolean buildingGardener = false; // we accepted request to build Gardener and are looking for a spot
    private static int lastRequestActionRound = -100; // when we accepted request. We give up on request if too long ago
    private static final int requestTimeout = 20; // time for giving up on request & timeout after failure
    private static final int scoutDelay = 200; // Build scout every scoutDelay turns
    private static final int initialScout = 50; // first turn to build scout
    private static final float surplusGardenerRatio = 1.5f;

    public static void initialize() throws GameActionException {
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD, 1);
        Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_SOLDIERS_TO_BUILD, 4);
    }

    public static void run() throws GameActionException {
        if (rc.getRoundNum() % scoutDelay == initialScout) {
            Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_SCOUTS_TO_BUILD, 1);
        }

        int totGardenerHealth = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.TOTAL_GARDENER_HEALTH);
        int numGardeners = totGardenerHealth / RobotType.GARDENER.maxHealth + Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD);
        int treeCnt = rc.getTreeCount();
        int surplus = (int) Math.ceil((surplusGardenerRatio * treeCnt - 5 * numGardeners) / 5);
        if (surplus > Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD)) {
            Radio.RadioCustomChannels.write(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD, surplus);
        }

        if (!buildingGardener && canPerformRequestAction()) {
            if (Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD) > 0) {
                buildingGardener = true;
                lastRequestActionRound = rc.getRoundNum();
            }
        }

        if (buildingGardener) {
            //Try some random hiring directions
            for (int i = 0; i < 20; i++) {
                Direction dir = randomDirection();
                if (rc.canHireGardener(dir)) {
                    rc.hireGardener(dir);
                    Radio.RadioCustomChannels.add(Radio.RadioCustomChannels.Channel.NUM_GARDENERS_TO_BUILD, -1);
                    buildingGardener = false;
                    return;
                }
            }

            if (canPerformRequestAction()) {
                lastRequestActionRound = rc.getRoundNum();
                buildingGardener = false;
            } else {
                //Otherwise we are blocked, try moving
                for (int i = 0; i < 20; i++) {
                    if (tryMove(randomDirection())) {
                        break;
                    }
                }
            }
        }

        //Don't block friendly robots

        RobotInfo[] nearbyFriendlyRobots = rc.senseNearbyRobots(RobotType.ARCHON.bodyRadius + 2 * GameConstants.BULLET_TREE_RADIUS + RobotType.GARDENER.bodyRadius, myTeam);
        RobotInfo[] nearbyEnemyRobots = rc.senseNearbyRobots(-1, enemy);
        if (nearbyEnemyRobots.length > 0) {
            if (generator.nextFloat() < 0.9) {
                tryMove(rc.getLocation().directionTo(nearbyEnemyRobots[0].getLocation()).opposite());
            }
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    break;
                }
            }
        } else if (nearbyFriendlyRobots.length > 0) {
            if (generator.nextFloat() < 0.5) {
                tryMove(rc.getLocation().directionTo(nearbyFriendlyRobots[0].getLocation()).opposite());
            }
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    break;
                }
            }
        } else {
            moveAwayFromEnemyArchon();
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    return;
                }
            }
        }

        requestHelp();
    }

    private static boolean canPerformRequestAction() {
        return rc.getRoundNum() - lastRequestActionRound > requestTimeout;
    }

    private static void moveAwayFromEnemyArchon() throws GameActionException {
        MapLocation close = enemyArchonLocations[0];
        for (MapLocation location : enemyArchonLocations) {
            if (rc.getLocation().distanceTo(location) < rc.getLocation().distanceTo(close)) {
                close = location;
            }
        }
        tryMove(rc.getLocation().directionTo(close).opposite());
    }

    private static final int requestDelay = 40;
    private static int lastRequestSoldier = -100;
    private static void requestHelp() throws GameActionException {
        //Enemies nearby
        RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(-1, enemy);
        if (nearbyEnemies.length > 0
                && rc.getRoundNum() - lastRequestSoldier > requestDelay) {
            Radio.RadioRequest.request(RobotType.SOLDIER, nearbyEnemies[0].getLocation());
            lastRequestSoldier = rc.getRoundNum();
            System.out.println("[debug] Requested soldier:" + Radio.RadioRequest.countRequests(RobotType.SOLDIER));
        }
    }
}
