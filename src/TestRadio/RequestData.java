package TestRadio;

import battlecode.common.MapLocation;

public strictfp class RequestData {
    public MapLocation location;
    public int roundSubmitted;

    RequestData(MapLocation location, int roundSubmitted) {
        this.location = location;
        this.roundSubmitted = roundSubmitted;
    }

    RequestData(int intBits) {
        location = new MapLocation(intBits & ((1 << 10) - 1), (intBits >> 10) & ((1 << 10) - 1));
        roundSubmitted = intBits >> 20;
    }

    public int toIntBits() {
        int x = (int) location.x;
        if (x < 0) {
            x = 0;
        }
        if (x >= (1 << 10)) {
            x = (1 << 10) - 1;
        }

        int y = (int) location.y;
        if (y < 0) {
            y = 0;
        }
        if (y >= (1 << 10)) {
            y = (1 << 10) - 1;
        }

        int ret = roundSubmitted;
        ret <<= 12;
        ret |= y;
        ret <<= 10;
        ret |= x;
        return ret;
    }
}
