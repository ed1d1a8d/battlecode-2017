package SoldierRadioNavigationPlayer;

import battlecode.common.*;


/**
 * Created by chillee on 1/15/17.
 */
public class Dodge {
    private static RobotController rc;

    public static void Initialize(RobotController myrc) {
        rc = myrc;
    }
    private static class Vector {
        double x;
        double y;
        public Vector(double x, double y){
            this.x = x;
            this.y = y;
        }
        public Vector(MapLocation x){
            this.x = x.x;
            this.y = x.y;
        }
        public Vector add(Vector b){
            return new Vector(this.x + b.x, this.y + b.y);
        }
        public Vector subtract(Vector b){
            return new Vector(this.x - b.x, this.y - b.y);
        }
        public Vector mult(double c){
            return new Vector(this.x*c, this.y*c);
        }
        public Vector divide(double c){
            return new Vector(this.x/c, this.y/c);
        }
        public double dot(Vector b){
            return this.x * b.x + this.y*b.y;
        }
        public double length(){
            return Math.sqrt(this.x*this.x + this.y*this.y);
        }
    }
    private static boolean willCollideAtLocation(BulletInfo b, MapLocation loc){
        Vector p1 = new Vector(b.getLocation());
        Vector p2 = new Vector(b.getLocation().add(b.getDir(), b.getSpeed()));
        Vector circPos = new Vector(loc);
        Vector segVec = p2.subtract(p1);
        Vector ptVec = circPos.subtract(p1);
        double segVecLength = segVec.length();
        Vector segVecUnit = segVec.divide(segVecLength);
        double proj = ptVec.dot(segVecUnit);
        MapLocation closest = null;
        if (proj <= 0){
            closest = new MapLocation((float)p1.x, (float)p1.y);
        } else if (proj >= segVecLength){
            closest = new MapLocation((float)p2.x, (float)p2.y);
        } else{
            Vector t = segVecUnit.mult(proj).add(p1);
            closest = new MapLocation((float)t.x, (float)t.y);
        }
        return closest.distanceTo(loc) <= rc.getType().bodyRadius;
    }
    private static boolean willCollideAtLocation2(BulletInfo b, MapLocation circPos){
        float bodyRadius = rc.getType().bodyRadius;
        float p1x = b.getLocation().x;
        float p1y = b.getLocation().y;
        MapLocation p2 = b.getLocation().add(b.getDir(), b.getSpeed());
        float segVecx = p2.x-p1x;
        float segVecy = p2.y-p1y;
        float ptVecx = circPos.x-p1x;
        float ptVecy = circPos.y-p1y;
        float segVecLength = (float)Math.sqrt(segVecx*segVecx+segVecy*segVecy);
        float segVecUnitx = segVecx/segVecLength;
        float segVecUnity = segVecy/segVecLength;
        float proj = ptVecx*segVecUnitx + ptVecy*segVecUnity;
        if (proj <= 0){
            return ptVecx*ptVecx + ptVecy*ptVecy <= bodyRadius*bodyRadius;
        } else if (proj >= segVecLength){
            return p2.distanceTo(circPos) <= bodyRadius;
        } else{
            float tx = segVecUnitx*proj + p1x - circPos.x;
            float ty = segVecUnity*proj + p1y - circPos.y;
            return tx*tx + ty*ty <= bodyRadius*bodyRadius;
        }
    }
    private static float bulletDamageAtLocation(MapLocation loc){
//        rc.setIndicatorDot(loc, 0,100,0);
        BulletInfo[] bullets = rc.senseNearbyBullets(loc, 2 + rc.getType().bodyRadius+.1f);
        float damage = 0;
        for (BulletInfo bullet : bullets){
//            if (bullet.getLocation().distanceTo(loc) > bullet.getSpeed()+rc.getType().bodyRadius+.1) {
//                continue;
//            }
            int beg = Clock.getBytecodeNum();
            if (willCollideAtLocation2(bullet, loc)){
                damage += bullet.getDamage();
            }
            System.out.println("willcollideatlocatoin: "+(Clock.getBytecodeNum()-beg));
        }
        return damage;
    }

    public static MapLocation dodgeBulletsRandom(MapLocation loc) {
        final int NUM_ATTEMPTS = 5;
        System.out.println(loc);
        int beg = Clock.getBytecodeNum();
        float minDamage = bulletDamageAtLocation(loc);
        System.out.println("bulletDamageAtLocation: "+(Clock.getBytecodeNum()-beg));
        if (minDamage == 0){
            System.out.println("bestDamage: "+minDamage);
            return loc;
        }
        MapLocation bestLocation = loc;
        for(int i=0; i<NUM_ATTEMPTS; i++){
            Direction dir = BaseBot.randomDirection();
            MapLocation cur = loc.add(dir, rc.getType().strideRadius);
            if (!rc.canMove(cur)){
                continue;
            }
            int b = Clock.getBytecodeNum();
            float curDamage = bulletDamageAtLocation(cur);
            System.out.println("bulletdamageatlocation: "+(Clock.getBytecodeNum() - b));
            if (curDamage < minDamage){
                minDamage = curDamage;
                bestLocation = cur;
            }
            if (minDamage == 0){
                System.out.println("bestDamage: "+minDamage);
                return bestLocation;
            }
        }
        rc.setIndicatorDot(bestLocation, 0,0,100);
        System.out.println("bestDamage: "+minDamage);
        return bestLocation;
    }

}
