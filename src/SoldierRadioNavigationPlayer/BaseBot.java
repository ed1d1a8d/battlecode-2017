package SoldierRadioNavigationPlayer;

import battlecode.common.*;

import java.util.Random;

public strictfp class BaseBot {
    // All attributes common to all the bots initialized here
    protected static RobotController rc;
    protected static Team myTeam;
    protected static Team enemy;
    protected static MapLocation[] enemyArchonLocations;
    protected static MapLocation center;
    protected static Random generator;

    public static void InitializeRobot(RobotController Myrc) throws GameActionException {
        rc = Myrc;

        myTeam = rc.getTeam();
        enemy = rc.getTeam().opponent();
        enemyArchonLocations = rc.getInitialArchonLocations(enemy);

        Radio.GlobalInitialize(rc, rc.getLocation());
        Radio.LocalInitialize(rc);
        center = Radio.getGlobalCenter();

        generator = new Random(rc.getID() * (rc.getRoundNum() + 1));
    }

    public static void shakeTreeIfPossible() throws GameActionException {
        if (rc.hasAttacked()) {
            return;
        }
        TreeInfo[] neutralTrees = rc.senseNearbyTrees(rc.getType().strideRadius, Team.NEUTRAL);
        for (TreeInfo tree : neutralTrees) {
            double b = rc.getTeamBullets();
            if (rc.canShake(tree.ID)) {
                rc.shake(tree.ID);
                if (rc.getTeamBullets() - b >  0){
                    System.out.println("[debug] bullets gotten: " + (rc.getTeamBullets() - b));
                }
                break;
            }
        }
        return;
    }

    public static Direction randomDirection() {
        return new Direction(generator.nextFloat() * 2 * (float)Math.PI);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    protected static boolean tryMove(Direction dir) throws GameActionException {
        if (rc.hasMoved()) {
            return true;
        }
        return tryMove(dir,20,3);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    protected static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {
        if (rc.hasMoved()) {
            return true;
        }
        // First, try intended direction
        if (rc.canMove(dir)) {
            rc.move(dir);
            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        while(currentCheck<=checksPerSide) {
            // Try the offset of the left side
            if(rc.canMove(dir.rotateLeftDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateLeftDegrees(degreeOffset*currentCheck));
                return true;
            }
            // Try the offset on the right side
            if(rc.canMove(dir.rotateRightDegrees(degreeOffset*currentCheck))) {
                rc.move(dir.rotateRightDegrees(degreeOffset*currentCheck));
                return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }

        // A move never happened, so return false.
        return false;
    }

    /**
     * A slightly more complicated example function, this returns true if the given bullet is on a collision
     * course with the current robot. Doesn't take into account objects between the bullet and this robot.
     *
     * @param bullet The bullet in question
     * @return True if the line of the bullet's path intersects with this robot's current position.
     */
    protected static boolean willCollideWithMe(BulletInfo bullet) {
        MapLocation myLocation = rc.getLocation();

        // Get relevant bullet information
        Direction propagationDirection = bullet.dir;
        MapLocation bulletLocation = bullet.location;

        // Calculate bullet relations to this robot
        Direction directionToRobot = bulletLocation.directionTo(myLocation);
        float distToRobot = bulletLocation.distanceTo(myLocation);
        float theta = propagationDirection.radiansBetween(directionToRobot);

        // If theta > 90 degrees, then the bullet is traveling away from us and we can break early
        if (Math.abs(theta) > Math.PI/2) {
            return false;
        }

        // distToRobot is our hypotenuse, theta is our angle, and we want to know this length of the opposite leg.
        // This is the distance of a line that goes from myLocation and intersects perpendicularly with propagationDirection.
        // This corresponds to the smallest radius circle centered at our location that would intersect with the
        // line that is the path of the bullet.
        float perpendicularDist = (float)Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

        return (perpendicularDist <= rc.getType().bodyRadius);
    }

    /**
     * A slightly more complicated example function, this returns true if the given bullet is on a collision
     * course with the current robot. Doesn't take into account objects between the bullet and this robot.
     *
     * @param bullet The bullet in question
     * @return True if the line of the bullet's path intersects with this robot's current position.
     */
    protected static boolean willCollideAtLocation(BulletInfo bullet, MapLocation location) {
        // Get relevant bullet information
        Direction propagationDirection = bullet.dir;
        MapLocation bulletLocation = bullet.location;

        // Calculate bullet relations to this robot
        Direction directionToRobot = bulletLocation.directionTo(location);
        float distToRobot = bulletLocation.distanceTo(location);
        float theta = propagationDirection.radiansBetween(directionToRobot);

        // If theta > 90 degrees, then the bullet is traveling away from us and we can break early
        if (Math.abs(theta) > Math.PI/2) {
            return false;
        }

        // distToRobot is our hypotenuse, theta is our angle, and we want to know this length of the opposite leg.
        // This is the distance of a line that goes from location and intersects perpendicularly with propagationDirection.
        // This corresponds to the smallest radius circle centered at our location that would intersect with the
        // line that is the path of the bullet.
        float perpendicularDist = (float)Math.abs(distToRobot * Math.sin(theta)); // soh cah toa :)

        return (perpendicularDist <= rc.getType().bodyRadius);
    }

    protected static BulletInfo getClosestBullet(BulletInfo[] bullets) {
        BulletInfo closestBullet = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<bullets.length;i++){
            if(rc.getLocation().distanceTo(bullets[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(bullets[i].location);
                closestBullet = bullets[i];
            }
        }
        return closestBullet;
    }

    protected static boolean tryMoveDirectionOrRandom(Direction direction) throws GameActionException {
        if (rc.hasMoved()) {
            return true;
        }
        if (!tryMove(direction)) {
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    return true;
                }
            }
        }
        return false;
    }

    protected  static boolean willAnyCollideAtLocation(BulletInfo[] bullets, MapLocation location){
        for (int i = 0;i<bullets.length;i++) {
            if (willCollideAtLocation(bullets[i], location)) {
                return true;
            }
        }
        return false;
    }

    protected static void tryDodgeAllBullets(BulletInfo[] nearbyBullets, Direction targetDirection) throws GameActionException {
        //if the bullet will collide in the direction that im going i change direction
        MapLocation futurePosition = rc.getLocation().add(targetDirection,
                rc.getType().strideRadius);

        int count = 0;
        while (willAnyCollideAtLocation(nearbyBullets, futurePosition) || BulletinWay(nearbyBullets, futurePosition)) {
            if (count == 3) {
                break;
            }
            if (generator.nextBoolean()) {
                targetDirection = targetDirection.rotateRightDegrees(45);
            } else {
                targetDirection = targetDirection.rotateLeftDegrees(45);
            }
            futurePosition = rc.getLocation().add(targetDirection,
                    rc.getType().strideRadius);
            count++;
        }
        tryMoveDirectionOrRandom(targetDirection);
    }

    protected static boolean BulletinWay(BulletInfo[] nearbyBullets, MapLocation futurePosition) {
        //futurePosition.distanceTo(nearbyBullets[i].getLocation())
        //      <= rc.getType().bodyRadius + nearbyBullets[i].getRadius()
        for (int i= 0 ; i < nearbyBullets.length; i++) {
            if (futurePosition.distanceTo(nearbyBullets[i].getLocation())
                    <= rc.getType().bodyRadius + nearbyBullets[i].getRadius()) {
                return true;
            }
        }
        return false;
    }

}
