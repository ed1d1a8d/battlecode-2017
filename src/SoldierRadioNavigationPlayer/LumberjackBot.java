package SoldierRadioNavigationPlayer;

import battlecode.common.*;


public class LumberjackBot extends BaseBot {


    //Communication variables
    private static int lastRequest = 0;
    private static int requestTimeout = 20;

    private static MapLocation currentLocation;

    static void initialize() throws GameActionException{
        System.out.println("I'm a lumberjack!");


    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    public static void run() throws GameActionException {
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        boolean moved = false;
        try {

            // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
            RobotInfo[] enemiesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);
            RobotInfo[] friendliesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, rc.getTeam());

            if(enemiesWithinStrike.length >0 && enemiesWithinStrike.length >= friendliesWithinStrike.length && !rc.hasAttacked()) {
                // Use strike() to hit all nearby robots!
                rc.strike();
            } else {
                // No close robots, so search for robots within sight radius
                RobotInfo[] enemyWithinSight = rc.senseNearbyRobots(-1,enemy);

                // If there is a robot, move towards it
                if(enemyWithinSight.length > 0) {
                    MapLocation myLocation = rc.getLocation();
                    MapLocation enemyLocation = enemyWithinSight[0].getLocation();
                    Direction toEnemy = myLocation.directionTo(enemyLocation);
                    moved = tryMove(toEnemy);
                } else {
                    // There are no enemies


                    //check for help requests
                    if (lastRequest > requestTimeout) {
                        RequestData rd = Radio.RadioRequest.getRequest(RobotType.LUMBERJACK);
                        if (rd != null) {
                            debug_lumberjackreqeustresponse();
                            Navigation.initializeToLocation(rc, rd.location,RobotType.LUMBERJACK.bodyRadius + RobotType.LUMBERJACK.strideRadius);
                            lastRequest = 0;
                        }
                    }
                    ++lastRequest;


                    if(!moved && !rc.hasAttacked()) {
                        runLumberJackNavigation();
                    }

                    if(!tryClearTreesInRange() && !moved){
                        tryMove(randomDirection());
                    }

                }
            }

            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Lumberjack Exception");
            e.printStackTrace();
        }
    }


    /**
     * THis function currently keeps the soldier in perpetual navigation between archon locations
     * @throws GameActionException
     */
    private static void runLumberJackNavigation() throws GameActionException {


        currentLocation = rc.getLocation();

        if(Navigation.isInitialized() && !Navigation.hasReachedTarget()) {
            // What should the bot do if there is a loop
            if (Navigation.hasProbableLoop()) {
                //It could move to the boundary location closest to the target and clear trees

                //if it hasent reached its boundary
                if (!Navigation.hasReachedClosestBoundaryLocation()) {
                    Navigation.Navigate(true);
                } else {//reached closestboundarylocation
                    rc.setIndicatorDot(currentLocation, 200, 0, 0);

                    if (!TryClearTreesInDirection(currentLocation.directionTo(Navigation.endLocation))) {
                        //if no tree was found try move towards endlocation
                        Navigation.TryStepCloserToBoundary();
                    }
                }

            } else {
                Navigation.Navigate();
            }
        }

    }

    private static void debug_lumberjackreqeustresponse() {
        System.out.println("[debug] Lumberjack coming!");
    }

    private static boolean TryClearTreesInDirection(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (checkAndClearTreeInDirection(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (checkAndClearTreeInDirection(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (checkAndClearTreeInDirection(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
        return false;
    }

    private static boolean checkAndClearTreeInDirection(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    tryClearTree(treeinfo);
                }else{
                    //this is my own team tree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }

    private static boolean tryClearTreesInRange() throws GameActionException {

        TreeInfo[] treeswithinrange = rc.senseNearbyTrees(-1);

        for(int i=0; i<treeswithinrange.length;i++){
            if(treeswithinrange[i].getTeam() != rc.getTeam()){
                return tryClearTree(treeswithinrange[i]);
            }
        }
        return false;
    }

    private static boolean tryClearTree(TreeInfo treeInfo) throws GameActionException {
        if(rc.canInteractWithTree(treeInfo.ID)){
            if(!rc.hasAttacked() && rc.canChop(treeInfo.getID())){
                rc.chop(treeInfo.getID());
                return true;
            }
        }else{
            tryMoveDirectionOrRandom(rc.getLocation().directionTo(treeInfo.getLocation()));
        }
        return false;
    }
}
