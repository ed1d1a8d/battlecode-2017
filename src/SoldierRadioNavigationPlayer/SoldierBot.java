package SoldierRadioNavigationPlayer;

import battlecode.common.*;

public class SoldierBot extends BaseBot {

    // Scoutbot currently simply moves towards the target direction - not a smart move homie

    private static MapLocation[] archonLocations;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static MapLocation currentLocation;

    private static int previousShot = 0;
    private static int previousShotDelay = 15;

    //Incomming Communication variables
    private static int lastRequest = 0;
    private static int requestTimeout = 20;

    //Outgoing Communication variables
    private static int lastRequestedLumberjack = 0;
    private static int requestDelay = 30;

    static void initialize() throws GameActionException {

        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        MapLocation chosenarchon = archonLocations[(int)(Math.random()*archonLocations.length)];
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {


        // The code you want your robot to perform every round should be in this loop
        while (true) {
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                runRound();


                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {

        currentLocation = rc.getLocation();
        boolean hasmoved = false;
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            BulletInfo closestBullet = getClosestBullet(NearbyBullets);

            if (willCollideWithMe(closestBullet)) {
                hasmoved = tryMove(randomDirection());
                //return;
            }
        }



        // See if there are any nearby enemy enemyRobots
        RobotInfo[] enemyRobots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        RobotType chosenRobotType = null;
        RobotInfo chosenEnemy = null;
        if (enemyRobots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...


            chosenEnemy = getClosestHostileRobot(enemyRobots);

            if(chosenEnemy==null)
                chosenEnemy = enemyRobots[0];

            chosenRobotType = chosenEnemy.getType();
            if (!hasmoved) {
                if (chosenRobotType == RobotType.LUMBERJACK || chosenRobotType == RobotType.TANK || chosenRobotType == RobotType.SOLDIER) {
                    hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()).opposite());
                } else {
                    hasmoved = tryMove(currentLocation.directionTo(chosenEnemy.getLocation()));
                }
            }

            shootAtRobot(chosenEnemy);
            //return;
        }

        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
        }

        //check for help requests
        if (lastRequest > requestTimeout) {
            RequestData rd = Radio.RadioRequest.getRequest(RobotType.SOLDIER);
            if (rd != null) {
                debug_soldierreqeustresponse();
                Navigation.initializeToLocation(rc, rd.location,RobotType.SOLDIER.bodyRadius + RobotType.SOLDIER.strideRadius);
                lastRequest = 0;
            }
        }
        ++lastRequest;
        ++previousShot;


        if(!hasmoved && !rc.hasAttacked()) {
            runSoldierNavigation();
        }
        ++lastRequestedLumberjack;
    }

    private static void debug_soldierreqeustresponse() {
        System.out.println("[debug] Soldier coming!");
    }

    private static RobotInfo getClosestHostileRobot(RobotInfo[] enemyRobots) {

        for (int i = 0;i<enemyRobots.length;i++){
            if(enemyRobots[i].getType() == RobotType.TANK ||
                    enemyRobots[i].getType() == RobotType.SOLDIER ||
                    enemyRobots[i].getType() == RobotType.LUMBERJACK ||
                    enemyRobots[i].getType() == RobotType.SCOUT){
                return enemyRobots[i];
            }
        }
        return null;
    }

    /**
     * THis function currently keeps the soldier in perpetual navigation between archon locations
     * @throws GameActionException
     */
    private static void runSoldierNavigation() throws GameActionException {
        //if it reached archon and archon not found... move to next
        if (!Navigation.isInitialized() || Navigation.hasReachedTarget()){
            MapLocation chosenarchon = archonLocations[generator.nextInt(archonLocations.length)];
            //Initialize naviation to move to chosenarchon
            Navigation.initializeToLocation(rc,chosenarchon,RobotType.SOLDIER.bodyRadius + RobotType.SOLDIER.strideRadius);
        }


        if(!Navigation.hasReachedTarget()) {
            // What should the bot do if there is a loop
            if (Navigation.hasProbableLoop()) {
                //It could move to the boundary location closest to the target and clear trees

                if(Navigation.closestBoundaryToTargetLocation !=null){
                    requestHelp(Navigation.closestBoundaryToTargetLocation);
                    Navigation.terminate();
                }
                //if it hasent reached its boundary
//                if (!Navigation.hasReachedClosestBoundaryLocation()) {
//                    Navigation.Navigate(true);
//                } else {//reached closestboundarylocation
//                    rc.setIndicatorDot(currentLocation, 200, 0, 0);
//
//                    if (!TryClearTrees(currentLocation.directionTo(Navigation.endLocation))) {
//                        //if no tree was found try move towards endlocation
//                        Navigation.TryStepCloserToBoundary();
//                    }
//                }

            } else {
                Navigation.Navigate();
            }
        }

    }

    private static void requestHelp(MapLocation target) throws GameActionException {
//        if (rc.senseNearbyRobots(-1, enemy).length > 0 && lastRequestedSoldier > requestDelay) {
//            lastRequestedSoldier = 0;
//            Radio.RadioRequest.request(RobotType.SOLDIER, rc.getLocation());
//            System.out.println("[debug] Requested soldier:" + Radio.RadioRequest.countRequests(RobotType.SOLDIER));
//        }
//        ++lastRequestedSoldier;

        if (lastRequestedLumberjack > requestDelay) {
            Radio.RadioRequest.request(RobotType.LUMBERJACK, target);
            lastRequestedLumberjack = 0;
        }

    }

    //returns false if no tree was found immediately in front
    //else fires at tree

    private static boolean TryClearTrees(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (CheckAndFireAtTree(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
            return false;
    }

    private static boolean CheckAndFireAtTree(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    if (rc.canFirePentadShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.firePentadShot(directiontocheck);
                    }
                    else if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(directiontocheck);
                    }
                }else{
                    //this is my own team tree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }


    private static boolean trishotbit = false;
    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (!trishotbit && rc.canFireTriadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireTriadShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            trishotbit = true;
        }else
        if (trishotbit && rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireTriadShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            trishotbit = false;
        }
        else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else{
            previousShotAtEnemyID = 0;
        }
    }
}

