import TestRadio.Radio;
import TestRadio.RequestData;
import battlecode.common.*;
import battlecode.instrumenter.inject.RobotMonitor;
import org.junit.After;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.Assert.*;

public class RadioTest extends Radio {
    @After
    public void resetRadio() throws NoSuchFieldException{
        resetBroadCastArray();
        Field field = Radio.class.getDeclaredField("globalCenter");
        field.setAccessible(true);
        try {
            field.set(null, null);
        } catch (Exception e) {
            //a necessary exception since we call field.set with null instance
            //since we are using singleton pattern for Radio...
        }
    }

    @Test
    public void testInitialization() {
        try {
            LocalInitialize(rc);
            fail("Should not be able to LocalInitialize() before GlobalInitialize() is called");
        } catch(GameActionException e) {
        }

        try {
            GlobalInitialize(rc, new MapLocation(0, 0));
        } catch(GameActionException e) {
            fail("Unexpected exception: " + e.getMessage());
        }

        try {
            GlobalInitialize(rc, new MapLocation(0, 0));
        } catch(GameActionException e) {
            fail("GlobalInitialize should not have thrown exception when called more than once");
        }

        try {
            LocalInitialize(rc);
        } catch(GameActionException e) {
            fail("Unexpected exception: " + e.getMessage());
        }
    }

    @Test
    public void testDataAllocation() {
        assertEquals("Overflowed broadcast array",
                GameConstants.BROADCAST_MAX_CHANNELS,
                RadioSettings.SIZE + RadioCustomChannels.SIZE + RadioRequest.SIZE + RadioStack.SIZE);
    }

    @Test
    public void testRadioStack() throws GameActionException, NoSuchFieldException {
        //Override Clock
        RobotMonitor.setBytecodeLimit((int) 1e9);
        RobotMonitor.reactivate();

        //Init
        try {
            GlobalInitialize(rc, new MapLocation(0, 0));
            LocalInitialize(rc);
        } catch (Exception e) {
            fail("Unexpected exception: " + e.getMessage());
        }
        Random generator = new Random(0);


        //Stack
        Stack<Integer> stk = new Stack<Integer>();
        for (int i = 0; i < 1000000; i++) {
            int val = generator.nextInt();
            RadioStack.push(val);
            stk.push(val);
        }
        for (int i = 0; i < RadioStack.SIZE; i++) {
            assertEquals(RadioStack.pop(), (int) stk.pop());
        }
        for (int i = 0; i < 2000; i++) {
            assertEquals(RadioStack.pop(), EMPTY);
        }
        stk.clear();
        for (int i = 0; i < 1000000; i++) {
            int val = generator.nextInt();
            RadioStack.push(val);
            stk.push(val);
        }


        //Stack
        for (int i = 0; i < RadioStack.SIZE; i++) {
            assertEquals(RadioStack.pop(), (int) stk.pop());
        }
        for (int i = 0; i < 2000; i++) {
            assertEquals(RadioStack.pop(), EMPTY);
        }


        stk.clear();
        for (int i = 0; i < 1000000; i++) {
            int val = generator.nextInt();
            RadioStack.push(val);
            stk.push(val);
        }
    }

    @Test
    public void testRadioRequest() throws GameActionException {
        RobotMonitor.setBytecodeLimit((int) 1e9);
        RobotMonitor.reactivate();
        resetBroadCastArray();
        try {
            GlobalInitialize(rc, new MapLocation(0, 0));
            LocalInitialize(rc);
        } catch (Exception e) {
            fail("Unexpected exception: " + e.getMessage());
        }
        Random generator = new Random(0);

        System.out.println("SIZE:" + RadioRequest.SIZE);
        System.out.println("START:" + RadioRequest.START);

        ArrayList<Stack<MapLocation>> stks = new ArrayList<Stack<MapLocation>>();
        for (int i = 0; i < RobotType.values().length; i++) {
            stks.add(new Stack<MapLocation>());
        }
        for (int i = 0; i < 10; i++) {
            int robot = generator.nextInt(RobotType.values().length);
            float x = 500 + generator.nextFloat() * GameConstants.MAP_MAX_HEIGHT;
            float y = generator.nextFloat() * GameConstants.MAP_MAX_HEIGHT;
            MapLocation location = new MapLocation(x, y);
            RadioRequest.request(RobotType.values()[robot], location);
            stks.get(robot).push(location);
        }

        float mxDistance = (float) Math.sqrt(2);
        float mxEncountered = 0;
        int[] cnt = {0, 0, 0, 0, 0, 0};
        for (int i = 0; i < 100000; i++) {
            int robot = generator.nextInt(RobotType.values().length);
            RequestData data = RadioRequest.getRequest(RobotType.values()[robot]);
            if (data == null) {
                //assertEquals(cnt[robot], RadioRequest.ROBOT_STACK_SIZE[robot]);
            } else {
                MapLocation roundedLocation = data.location;
                MapLocation location = stks.get(robot).pop();
                float distance = roundedLocation.distanceTo(location);
                if (distance >= (float) (mxDistance + 0.001)) {
                    System.out.println(i);
                    System.out.println(RobotType.values()[robot].toString());
                    System.out.println(location.x + ", " + location.y);
                    System.out.println(roundedLocation.x + ", " + roundedLocation.y);
                }
                assertTrue("Check location integerization", distance < (float) (mxDistance + 0.001));
                mxEncountered = Math.max(distance, mxEncountered);
                ++cnt[robot];
            }
        }
        System.out.println("Max encountered: " + mxEncountered);
        System.out.println("True max: " + mxDistance);
    }

    public int broadcastArray[] = new int[GameConstants.BROADCAST_MAX_CHANNELS];

    public void resetBroadCastArray() {
        Arrays.fill(broadcastArray, 0);
    }

    RobotController rc = new RobotController() {
        @Override
        public void broadcast(int idx, int val) throws GameActionException {
            if (idx < 0 || idx >= GameConstants.BROADCAST_MAX_CHANNELS) {
                throw new GameActionException(GameActionExceptionType.INTERNAL_ERROR,
                        "Broadcast index " + Integer.toString(idx) + " out of bounds");
            }
            broadcastArray[idx] = val;
        }

        @Override
        public int readBroadcast(int idx) throws GameActionException {
            if (idx < 0 || idx >= GameConstants.BROADCAST_MAX_CHANNELS) {
                throw new GameActionException(GameActionExceptionType.INTERNAL_ERROR,
                        "Broadcast index " + Integer.toString(idx) + " out of bounds");
            }
            return broadcastArray[idx];
        }

        @Override
        public int getRoundLimit() {
            return 0;
        }

        @Override
        public int getRoundNum() {
            return 0;
        }

        @Override
        public float getTeamBullets() {
            return 0;
        }

        @Override
        public int getTeamVictoryPoints() {
            return 0;
        }

        @Override
        public int getOpponentVictoryPoints() {
            return 0;
        }

        @Override
        public int getRobotCount() {
            return 0;
        }

        @Override
        public int getTreeCount() {
            return 0;
        }

        @Override
        public MapLocation[] getInitialArchonLocations(Team team) {
            return new MapLocation[0];
        }

        @Override
        public int getID() {
            return 0;
        }

        @Override
        public Team getTeam() {
            return null;
        }

        @Override
        public RobotType getType() {
            return null;
        }

        @Override
        public MapLocation getLocation() {
            return null;
        }

        @Override
        public float getHealth() {
            return 0;
        }

        @Override
        public int getAttackCount() {
            return 0;
        }

        @Override
        public int getMoveCount() {
            return 0;
        }

        @Override
        public boolean onTheMap(MapLocation mapLocation) throws GameActionException {
            return false;
        }

        @Override
        public boolean onTheMap(MapLocation mapLocation, float v) throws GameActionException {
            return false;
        }

        @Override
        public boolean canSenseBulletLocation(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canSenseLocation(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canSenseRadius(float v) {
            return false;
        }

        @Override
        public boolean canSensePartOfCircle(MapLocation mapLocation, float v) {
            return false;
        }

        @Override
        public boolean canSenseAllOfCircle(MapLocation mapLocation, float v) {
            return false;
        }

        @Override
        public boolean isLocationOccupied(MapLocation mapLocation) throws GameActionException {
            return false;
        }

        @Override
        public boolean isLocationOccupiedByTree(MapLocation mapLocation) throws GameActionException {
            return false;
        }

        @Override
        public boolean isLocationOccupiedByRobot(MapLocation mapLocation) throws GameActionException {
            return false;
        }

        @Override
        public boolean isCircleOccupied(MapLocation mapLocation, float v) throws GameActionException {
            return false;
        }

        @Override
        public boolean isCircleOccupiedExceptByThisRobot(MapLocation mapLocation, float v) throws GameActionException {
            return false;
        }

        @Override
        public TreeInfo senseTreeAtLocation(MapLocation mapLocation) throws GameActionException {
            return null;
        }

        @Override
        public RobotInfo senseRobotAtLocation(MapLocation mapLocation) throws GameActionException {
            return null;
        }

        @Override
        public boolean canSenseTree(int i) {
            return false;
        }

        @Override
        public boolean canSenseRobot(int i) {
            return false;
        }

        @Override
        public boolean canSenseBullet(int i) {
            return false;
        }

        @Override
        public TreeInfo senseTree(int i) throws GameActionException {
            return null;
        }

        @Override
        public RobotInfo senseRobot(int i) throws GameActionException {
            return null;
        }

        @Override
        public BulletInfo senseBullet(int i) throws GameActionException {
            return null;
        }

        @Override
        public RobotInfo[] senseNearbyRobots() {
            return new RobotInfo[0];
        }

        @Override
        public RobotInfo[] senseNearbyRobots(float v) {
            return new RobotInfo[0];
        }

        @Override
        public RobotInfo[] senseNearbyRobots(float v, Team team) {
            return new RobotInfo[0];
        }

        @Override
        public RobotInfo[] senseNearbyRobots(MapLocation mapLocation, float v, Team team) {
            return new RobotInfo[0];
        }

        @Override
        public TreeInfo[] senseNearbyTrees() {
            return new TreeInfo[0];
        }

        @Override
        public TreeInfo[] senseNearbyTrees(float v) {
            return new TreeInfo[0];
        }

        @Override
        public TreeInfo[] senseNearbyTrees(float v, Team team) {
            return new TreeInfo[0];
        }

        @Override
        public TreeInfo[] senseNearbyTrees(MapLocation mapLocation, float v, Team team) {
            return new TreeInfo[0];
        }

        @Override
        public BulletInfo[] senseNearbyBullets() {
            return new BulletInfo[0];
        }

        @Override
        public BulletInfo[] senseNearbyBullets(float v) {
            return new BulletInfo[0];
        }

        @Override
        public BulletInfo[] senseNearbyBullets(MapLocation mapLocation, float v) {
            return new BulletInfo[0];
        }

        @Override
        public MapLocation[] senseBroadcastingRobotLocations() {
            return new MapLocation[0];
        }

        @Override
        public boolean hasMoved() {
            return false;
        }

        @Override
        public boolean hasAttacked() {
            return false;
        }

        @Override
        public boolean isBuildReady() {
            return false;
        }

        @Override
        public int getBuildCooldownTurns() {
            return 0;
        }

        @Override
        public boolean canMove(Direction direction) {
            return false;
        }

        @Override
        public boolean canMove(Direction direction, float v) {
            return false;
        }

        @Override
        public boolean canMove(MapLocation mapLocation) {
            return false;
        }

        @Override
        public void move(Direction direction) throws GameActionException {

        }

        @Override
        public void move(Direction direction, float v) throws GameActionException {

        }

        @Override
        public void move(MapLocation mapLocation) throws GameActionException {

        }

        @Override
        public boolean canStrike() {
            return false;
        }

        @Override
        public void strike() throws GameActionException {

        }

        @Override
        public boolean canFireSingleShot() {
            return false;
        }

        @Override
        public boolean canFireTriadShot() {
            return false;
        }

        @Override
        public boolean canFirePentadShot() {
            return false;
        }

        @Override
        public void fireSingleShot(Direction direction) throws GameActionException {

        }

        @Override
        public void fireTriadShot(Direction direction) throws GameActionException {

        }

        @Override
        public void firePentadShot(Direction direction) throws GameActionException {

        }

        @Override
        public boolean canChop(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canChop(int i) {
            return false;
        }

        @Override
        public void chop(MapLocation mapLocation) throws GameActionException {

        }

        @Override
        public void chop(int i) throws GameActionException {

        }

        @Override
        public boolean canShake(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canShake(int i) {
            return false;
        }

        @Override
        public void shake(MapLocation mapLocation) throws GameActionException {

        }

        @Override
        public void shake(int i) throws GameActionException {

        }

        @Override
        public boolean canWater(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canWater(int i) {
            return false;
        }

        @Override
        public void water(MapLocation mapLocation) throws GameActionException {

        }

        @Override
        public void water(int i) throws GameActionException {

        }

        @Override
        public boolean canWater() {
            return false;
        }

        @Override
        public boolean canShake() {
            return false;
        }

        @Override
        public boolean canInteractWithTree(MapLocation mapLocation) {
            return false;
        }

        @Override
        public boolean canInteractWithTree(int i) {
            return false;
        }

        @Override
        public void broadcastBoolean(int i, boolean b) throws GameActionException {

        }

        @Override
        public boolean readBroadcastBoolean(int i) throws GameActionException {
            return false;
        }

        @Override
        public void broadcastInt(int i, int i1) throws GameActionException {

        }

        @Override
        public int readBroadcastInt(int i) throws GameActionException {
            return 0;
        }

        @Override
        public void broadcastFloat(int i, float v) throws GameActionException {

        }

        @Override
        public float readBroadcastFloat(int i) throws GameActionException {
            return 0;
        }

        @Override
        public boolean hasRobotBuildRequirements(RobotType robotType) {
            return false;
        }

        @Override
        public boolean hasTreeBuildRequirements() {
            return false;
        }

        @Override
        public boolean canBuildRobot(RobotType robotType, Direction direction) {
            return false;
        }

        @Override
        public void buildRobot(RobotType robotType, Direction direction) throws GameActionException {

        }

        @Override
        public boolean canPlantTree(Direction direction) {
            return false;
        }

        @Override
        public void plantTree(Direction direction) throws GameActionException {

        }

        @Override
        public boolean canHireGardener(Direction direction) {
            return false;
        }

        @Override
        public void hireGardener(Direction direction) throws GameActionException {

        }

        @Override
        public float getVictoryPointCost() {
            return 0;
        }

        @Override
        public void donate(float v) throws GameActionException {

        }

        @Override
        public void disintegrate() {

        }

        @Override
        public void resign() {

        }

        @Override
        public void setIndicatorDot(MapLocation mapLocation, int i, int i1, int i2) {

        }

        @Override
        public void setIndicatorLine(MapLocation mapLocation, MapLocation mapLocation1, int i, int i1, int i2) {

        }

        @Override
        public void setTeamMemory(int i, long l) {

        }

        @Override
        public void setTeamMemory(int i, long l, long l1) {

        }

        @Override
        public long[] getTeamMemory() {
            return new long[0];
        }

        @Override
        public long getControlBits() {
            return 0;
        }
    };
}
