package RandomGardener;

import battlecode.common.*;

public strictfp class Radio {
    private static RobotController rc;
    private static MapLocation globalCenter = null; //protected so testing class can access

    //Will be null if not initialized
    public static MapLocation getGlobalCenter() {
        return globalCenter;
    }

    //Does nothing if called more than once globally
    public static void GlobalInitialize(RobotController myrc, MapLocation center) throws GameActionException {
        rc = myrc;
        if (RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_SET) == 0) {
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_X, encodeCoordinate(center.x));
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_Y, encodeCoordinate(center.y));
            RadioSettings.write(RadioSettings.Setting.GLOBAL_CENTER_SET, 1);
        }
    }

    //Throws exception if called before GlobalInitialize()
    public static void LocalInitialize(RobotController myrc) throws GameActionException {
        rc = myrc;
        if (RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_SET) == 1) {
            globalCenter = new MapLocation(decodeCoordinate(RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_X)),
                    decodeCoordinate(RadioSettings.read(RadioSettings.Setting.GLOBAL_CENTER_Y)));
        } else {
            throw new GameActionException(GameActionExceptionType.CANT_DO_THAT,
                    "Called Radio.LocalInitialize() before Radio.GlobalInitialize()");
        }
    }

    // Some flags. Sometimes returned by appropriate functions
    public static final int ERROR = -1;
    public static final int OUT_OF_BOUNDS = -2; //This is the return value for error
    public static final int EMPTY = -3;
    public static final int NOT_ENOUGH_BYTECODES = -4;

    /**
     *  Data is ordered [RadioSettings, RadioCustomChannels, RadioMap, RadioStack]
     *  The size of everything but RadioStack is defined inside the corresponding class.
     *  RadioSetting and RadioCustomChannels sizes are defined by the size of their enums
     *  RadioHeatMap has a fixed size.
     *  PLEASE TRY TO USE METHODS PROVIDED BY THIS CLASS
     */

    /**
     * The RadioSetting is for use by this class only, and stores some internal values.
     * Avoid reading and writing to this block from outside this class -- the data there is meant for
     * internal use.
     */
    public strictfp static class RadioSettings {
        //Internal Setting enum
        //This is used like C-style enum, all values are treated as integers
        public enum Setting {
            GLOBAL_CENTER_SET,
            GLOBAL_CENTER_X,
            GLOBAL_CENTER_Y,
            STACK_BOTTOM,
            STACK_TOP
        }
        public static final int SIZE = Setting.values().length;
        public static final int START = 0;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        public static int read(Setting setting) throws GameActionException {
            return read(setting.ordinal());
        }

        public static void write(Setting setting, int val) throws GameActionException {
            write(setting.ordinal(), val);
        }
    }

    /**
     * A collection of random global variables that you want to have global access to
     * Add these variables to the enum channels.
     * Access the variable by calling read(Channel channel) and write(Channel channel, int val);
     */
    public strictfp static class RadioCustomChannels {
        public enum Channel {
            TOTAL_GARDENER_HEALTH
        }
        public static final int SIZE = Channel.values().length;
        public static final int START = RadioSettings.SIZE;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        public static int read(Channel channel) throws GameActionException {
            return read(channel.ordinal());
        }

        public static void write(Channel channel, int val) throws GameActionException {
            write(channel.ordinal(), val);
        }

        public static void add(Channel channel, int d) throws GameActionException {
            int cur = read(channel);
            write(channel, cur + d);
        }
    }

    /**
     * A structure that allows bots to request for backup.
     * Bots can request for a specific robot to back them up, by calling:
     *      request(MapLocation location, RobotType type)
     * This will request a robot of type type to the specified location
     * If a robot wishes to accept a request for help, they can call MapLocation getRequest(RobotType),
     * which will provide them with the location from which help was requested.
     */
    public strictfp static class RadioRequest {
        public static final int COORDINATE_BITS = 4;
        public static final int PREFIX = 8; //8 channels used to store bottom and top, take up first 8 of SIZE
        public static final int FOOTPRINT = 300; //total memory taken up
        public static final int SIZE = FOOTPRINT - PREFIX; //size of usable stack
        public static final int START = RadioSettings.SIZE + RadioCustomChannels.SIZE;

        private static int getBottom(RobotType robot) throws GameActionException {
            int idx = robot.ordinal() - 2;
            return rc.readBroadcast(START + 2 * idx);
        }

        private static void setBottom(RobotType robot, int val) throws GameActionException {
            int idx = robot.ordinal() - 2;
            rc.broadcast(START + 2 * idx, val);
        }

        private static int getTop(RobotType robot) throws GameActionException  {
            int idx = robot.ordinal() - 2;
            return rc.readBroadcast(START + 2 * idx + 1);
        }

        private static void setTop(RobotType robot, int val) throws GameActionException  {
            int idx = robot.ordinal() - 2;
            rc.broadcast(START + 2 * idx + 1, val);
        }

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + PREFIX + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + PREFIX + idx, val);
        }

        private static void push(RobotType robot, int val) throws GameActionException {
            int bottom = getBottom(robot);
            int top = getTop(robot);

            int cur = read(top % SIZE);
            int idx = robot.ordinal() - 2;
            cur ^= ((cur >> (idx * 2 * COORDINATE_BITS)) & ((1 << (2 * COORDINATE_BITS)) - 1))
                    << (idx * 2 * COORDINATE_BITS);
            val &= (1 << (2 * COORDINATE_BITS)) - 1;
            cur ^= val << (idx * 2 * COORDINATE_BITS);
            write(top % SIZE, cur);

            if (bottom + SIZE == top) {
                setBottom(robot, bottom + 1);
            }
            setTop(robot, top + 1);
        }

        private static int pop(RobotType robot) throws GameActionException {
            int bottom = getBottom(robot);
            int top = getTop(robot);
            if (bottom == top) {
                return EMPTY;
            }
            setTop(robot, top - 1);

            int ret = read((top - 1) % SIZE);
            int idx = robot.ordinal() - 2;
            ret >>= (idx * 2 * COORDINATE_BITS);
            ret &= (1 << (2 * COORDINATE_BITS)) - 1;
            return ret;
        }

        //Helper method for compressing coordinates
        public static int compressCoordinates(MapLocation location) {
            int xPos = (int) (Math.floor((location.x - globalCenter.x) / GameConstants.MAP_MAX_HEIGHT * (1 << (COORDINATE_BITS - 1))));
            xPos += (1 << COORDINATE_BITS) / 2; //make positive
            if (xPos < 0) {
                xPos = 0;
            } else if (xPos >= (1 << COORDINATE_BITS)) {
                xPos = (1 << COORDINATE_BITS) - 1;
            }
            int yPos = (int) (Math.floor((location.y - globalCenter.y) / GameConstants.MAP_MAX_HEIGHT * (1 << (COORDINATE_BITS - 1))));
            yPos += (1 << COORDINATE_BITS) / 2; //make positive
            if (yPos < 0) {
                yPos = 0;
            } else if (yPos >= (1 << COORDINATE_BITS)) {
                yPos = (1 << COORDINATE_BITS) - 1;
            }

            return (yPos << COORDINATE_BITS) + xPos;
        }

        //Helper method for decomporessing coordinates
        private static MapLocation decompressCoordinates(int val) {
            int ix = val % (1 << COORDINATE_BITS);
            int iy = val >> COORDINATE_BITS;

            double x = globalCenter.x + ((ix + 0.5) / (1 << (COORDINATE_BITS - 1)) - 1.0) * GameConstants.MAP_MAX_HEIGHT;
            double y = globalCenter.y + ((iy + 0.5) / (1 << (COORDINATE_BITS - 1)) - 1.0) * GameConstants.MAP_MAX_HEIGHT;
            return new MapLocation((float) x, (float) y);
        }

        //Checks if RobotType is one of: SCOUT, SOLDIER, LUMBERJACK, TANK
        private static void checkRobotType(RobotType robot) throws GameActionException {
            if (robot == RobotType.ARCHON || robot == RobotType.GARDENER) { //invalid type
                throw new GameActionException(GameActionExceptionType.CANT_DO_THAT, "Invalid robot type");
            }
        }

        public static int countRequests(RobotType robot) throws GameActionException {
            checkRobotType(robot);
            return getTop(robot) - getBottom(robot);
        }

        //Does nothing if not enough bytecodes
        public static void request(MapLocation location, RobotType robot) throws GameActionException {
            checkRobotType(robot);
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 200) { //TODO: Recheck bytecodes
                System.out.println("[debug] Not enough bytcodes to RadioRequest.request()");
                return;
            }

            push(robot, compressCoordinates(location));
        }

        //Atomic, returns NOT_ENOUGH_BYTECODES if not enough bytecodes to complete operation
        //return null if empty
        public static MapLocation getRequest(RobotType robot) throws GameActionException {
            checkRobotType(robot);
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 200) { //TODO: Recheck bytecodes
                System.out.println("[debug] Not enough bytcodes to RadioRequest.request()");
                return rc.getLocation(); //returns self location instead
            }

            int ret = pop(robot);
            if (ret == EMPTY) {
                return null; //returns self location if empty
            }
            return decompressCoordinates(ret);
        }
    }

    /**
     * The RadioStack is a stack. It has size RadioStack.SIZE.
     * You can push data on and pop data off.
     * New data is guaranteed to be pushed on.
     * If size is exceeded, the oldest data (bottom of stack) is thrown away.
     * It has the following methods:
     *     void push(int val);
     *     int pop();
     *
     * Note: There is no isEmpty() function is implemented due to ByteCode costs.
     *       Instead call pop until EMPTY is returned.
     */
    public strictfp static class RadioStack {
        public static final int START = RadioSettings.SIZE + RadioCustomChannels.SIZE + RadioRequest.FOOTPRINT;
        public static final int SIZE = GameConstants.BROADCAST_MAX_CHANNELS - START;

        private static int read(int idx) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return OUT_OF_BOUNDS;
            }
            return rc.readBroadcast(START + idx);
        }

        //Does nothing if out of bounds
        private static void write(int idx, int val) throws GameActionException {
            if (idx < 0 || idx > SIZE) {
                return;
            }
            rc.broadcast(START + idx, val);
        }

        //Atomic, does nothing if not enough bytecodes
        public static void push(int val) throws GameActionException {
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 78) { //TODO: Recheck bytecodes
                return;
            }
            //Takes 74 bytecodes
            int bottom = RadioSettings.read(RadioSettings.Setting.STACK_BOTTOM);
            int top = RadioSettings.read(RadioSettings.Setting.STACK_TOP);
            write(top % SIZE, val);
            if (bottom + SIZE == top) {
                RadioSettings.write(RadioSettings.Setting.STACK_BOTTOM, bottom + 1);
            }
            RadioSettings.write(RadioSettings.Setting.STACK_TOP, top + 1);
        }

        //Atomic, returns NOT_ENOUGH_BYTECODES if not enough bytecodes to complete operation
        //return EMPTY if empty
        public static int pop() throws GameActionException {
            //This takes two bytecodes, but we use 4 just in case
            if (Clock.getBytecodesLeft() < 65) { //TODO: Recheck bytecodes
                return NOT_ENOUGH_BYTECODES;
            }
            //Takes 61 bytecodes from here
            int bottom = RadioSettings.read(RadioSettings.Setting.STACK_BOTTOM);
            int top = RadioSettings.read(RadioSettings.Setting.STACK_TOP);
            if (bottom == top) {
                return EMPTY;
            }
            RadioSettings.write(RadioSettings.Setting.STACK_TOP, top - 1);
            return read((top - 1) % SIZE);
        }
    }

    //DEPRECATED!
    /**
     This class encodes the information collected by a unit on its surrounding enemies.
     It provides some utility functions for encoding to a 31-BIT-INTEGER and decoding.
     We want to cap it at 31 because we want to reserve some space to send other types of messages.

     What the bits mean is that whatever the actual value will get capped at the maximum number of bits
     So if you sense 100 enemyTrees, the actual encoded value will be 2^4 - 1 = 31 trees.
     The xPos and yPos are a bit different though, your coordinate will be we instead placed into one box
     in a 16 by 16 grid.
     Pass in null for robot info if you don't want to pass in anything
     **/
    @Deprecated
    public strictfp static class EnemyUnitReport {
        private static int totalBits;

        public enum Field {
            X(5),
            Y(5),
            ARCHONS(1),
            GARDENERS(3),
            ENEMYTREES(3),
            SCOUTS(4),
            SOLDIERS(4),
            LUMBERJACKS(3),
            TANKS(3);

            public final int bits;
            public int prefix;

            Field(int bits) {
                this.bits = bits;
                this.prefix = totalBits;
                totalBits += bits; //this works because enums are singletons -- initialized at most once in the JVM
            }

            //Please call EnemyUnitReport.encode(location, enemies, enemyTrees) instead
            //This is for internal use
            public int encode(int val) {
                if (val < 0) {
                    val = 0;
                } else if (val >= (1 << bits)) {
                    val = (1 << bits) - 1;
                }
                return val << prefix;
            }
        }

        public static int getTotalBits() {
            return totalBits;
        }

        //If data.length != Field.values().length, returns ERROR
        public static int encode(int[] data) {
            if (data.length != Field.values().length) {
                return ERROR;
            }

            int ret = 0;
            for (Field f : Field.values()) {
                ret |= f.encode(data[f.ordinal()]);
            }
            return ret;
        }

        public static int decode(int encodedData, Field field) {
            return (encodedData >> field.prefix) & ((1 << field.bits) & 1);
        }

        public static int encode(MapLocation location, RobotInfo[] enemies, int enemyTrees) {
            if (globalCenter == null) {
                return ERROR;
            }

            int xPos = (int) Math.floor((location.x - globalCenter.x) / 100 * (1 << Field.X.bits));
            xPos += (1 << Field.X.bits) / 2; //make positive
            if (xPos < 0) {
                xPos = 0;
            } else if (xPos >= (1 << Field.X.bits)) {
                xPos = (1 << Field.X.bits) - 1;
            }
            int yPos = (int) Math.floor((location.y - globalCenter.y) / 100 * (1 << Field.Y.bits));
            yPos += (1 << Field.Y.bits) / 2; //make positive
            if (yPos < 0) {
                yPos = 0;
            } else if (yPos >= (1 << Field.Y.bits)) {
                yPos = (1 << Field.Y.bits) - 1;
            }

            int numArchons = 0;
            int numGardeners = 0;
            int numSoldiers = 0;
            int numScouts = 0;
            int numLumberjacks = 0;
            int numTanks = 0;
            if (enemies != null) {
                for (RobotInfo robot : enemies) {
                    switch (robot.getType()) {
                        case ARCHON:
                            ++numArchons;
                            break;
                        case GARDENER:
                            ++numGardeners;
                            break;
                        case SOLDIER:
                            ++numSoldiers;
                            break;
                        case SCOUT:
                            ++numScouts;
                            break;
                        case LUMBERJACK:
                            ++numLumberjacks;
                            break;
                        case TANK:
                            ++numTanks;
                            break;
                    }
                }
            }
            int numEnemyTrees = enemyTrees;

            int[] data = new int[9];
            data[Field.X.ordinal()] = xPos;
            data[Field.X.ordinal()] = xPos;
            data[Field.Y.ordinal()] = yPos;
            data[Field.ARCHONS.ordinal()] = numArchons;
            data[Field.GARDENERS.ordinal()] = numGardeners;
            data[Field.ENEMYTREES.ordinal()] = numEnemyTrees;
            data[Field.SCOUTS.ordinal()] = numScouts;
            data[Field.SOLDIERS.ordinal()] = numSoldiers;
            data[Field.LUMBERJACKS.ordinal()] = numLumberjacks;
            data[Field.TANKS.ordinal()] = numTanks;
            return encode(data);
        }
    }

    /**
     If you do not want to use the enemyUnitReport data, you can opt to pass in your own data.
     It will be a 32 bit value with a single 1 bit at the most significant position
     with your 31-bit data appended.
     */
    public static int encodeCustomValue(int val) { //WARNING: - if val = 2^31 - 1, then this will encode ERROR
        int ret = ~Integer.MAX_VALUE; //1000...000 in binary
        ret |= val;
        return ret;
    }

    public static int decodeCustomValue(int val) {
        return val & Integer.MAX_VALUE;
    }

    //From docs, the maximum coordinate value is on the order of 600.
    //600 * 1e6 < 1e9, fits into int
    public static int encodeCoordinate(float val) {
        return (int) (val * 1e6);
    }

    public static float decodeCoordinate(int val) {
        return (float) (val / 1e6);
    }
}
