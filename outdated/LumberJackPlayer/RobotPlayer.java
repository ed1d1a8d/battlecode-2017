package LumberJackPlayer;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public strictfp class RobotPlayer {
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    @SuppressWarnings("unused")
    public static void run(RobotController rc) throws GameActionException {
        BaseBot.InitializeRobot(rc);
        switch (rc.getType()) {
            case ARCHON:
                ArchonBot.initialize();
                break;
            case GARDENER:
                GardenerBot.initialize();
                break;
            case SOLDIER:
                SoldierBot.initialize();
                break;
            case SCOUT:
                ScoutBot.initialize();
                break;
            case LUMBERJACK:
                LumberjackBot.initialize();
                break;
        }
        while (true){
            switch (rc.getType()) {
                case ARCHON:
                    ArchonBot.run();
                    break;
                case GARDENER:
                    GardenerBot.run();
                    break;
                case SOLDIER:
                    SoldierBot.run();
                    break;
                case SCOUT:
                    ScoutBot.run();
                    break;
                case LUMBERJACK:
                    LumberjackBot.run();
                    break;
            }
            BaseBot.shakeTreeIfPossible();
        }
    }
}

