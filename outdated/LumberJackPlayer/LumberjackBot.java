package LumberJackPlayer;

import battlecode.common.*;


public class LumberjackBot extends BaseBot {
    static void initialize() throws GameActionException{
        System.out.println("I'm a lumberjack!");


    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    public static void run() throws GameActionException {
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {

            // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
            RobotInfo[] enemiesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);
            RobotInfo[] friendliesWithinStrike = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius + GameConstants.LUMBERJACK_STRIKE_RADIUS, rc.getTeam());

            if(enemiesWithinStrike.length >0 && enemiesWithinStrike.length >= friendliesWithinStrike.length && !rc.hasAttacked()) {
                // Use strike() to hit all nearby robots!
                rc.strike();
            } else {
                // No close robots, so search for robots within sight radius
                RobotInfo[] enemyWithinSight = rc.senseNearbyRobots(-1,enemy);

                // If there is a robot, move towards it
                if(enemyWithinSight.length > 0) {
                    MapLocation myLocation = rc.getLocation();
                    MapLocation enemyLocation = enemyWithinSight[0].getLocation();
                    Direction toEnemy = myLocation.directionTo(enemyLocation);
                    tryMove(toEnemy);
                } else {
                    // There are no enemies
                    tryClearTreesInRange();
                }
            }

            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Lumberjack Exception");
            e.printStackTrace();
        }
    }

    private static boolean tryClearTreesInRange() throws GameActionException {

        TreeInfo[] treeswithinrange = rc.senseNearbyTrees(-1);

        for(int i=0; i<treeswithinrange.length;i++){
            if(treeswithinrange[i].getTeam() != rc.getTeam()){
                return tryClearTree(treeswithinrange[i]);
            }
        }
        return false;
    }

    private static boolean tryClearTree(TreeInfo treeInfo) throws GameActionException {
        if(rc.canInteractWithTree(treeInfo.ID)){
            if(!rc.hasAttacked() && rc.canChop(treeInfo.getID())){
                rc.chop(treeInfo.getID());
                return true;
            }
        }else{
            tryMoveDirectionOrRandom(rc.getLocation().directionTo(treeInfo.getLocation()));
        }
        return false;
    }
}
