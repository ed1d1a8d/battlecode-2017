package LumberJackPlayer;

import battlecode.common.*;


public strictfp class ScoutBot extends BaseBot {

    //private static int targetGardeners = 10;
    //private static int numGardeners = 0;
    //private static final double archonWanderDistance = 5;

    // Scoutbot currently simply moves towards the target direction - not a smart move homie
    private static Direction targetDirection;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;

    static void initialize(){
        MapLocation[] archonLocations;
        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }
        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        center = rc.getLocation();
        int numArchons = rc.getInitialArchonLocations(rc.getTeam()).length;
    }

    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        try {
            runRound();

            Clock.yield();

        } catch (Exception e) {
            System.out.println("Archon Exception");
            e.printStackTrace();
        }
    }

    private static void runRound() throws GameActionException {

        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if(NearbyBullets != null && NearbyBullets.length>0) {
            for(int i=0;i<NearbyBullets.length;i++){
                //if the bullet will collide in the direction that im going i change direction
                if(willCollideAtLocation(NearbyBullets[i],rc.getLocation().add(targetDirection,
                        rc.getType().bodyRadius + rc.getType().strideRadius))){
                    if(Math.random()>.5) {
                        targetDirection = targetDirection.rotateRightDegrees(90);
                    }else{
                        targetDirection = targetDirection.rotateLeftDegrees(90);
                    }
                    tryMoveDirectionOrRandom(targetDirection);
                    return;
                }
            }
        }

        //Check and shoot at nearby enemies
        Team enemy = rc.getTeam().opponent();
        RobotInfo[] enemyrobots = rc.senseNearbyRobots(-1, enemy);
        if (enemyrobots.length > 0) {
            for (int i = 0; i < enemyrobots.length; i++) {
                Direction directionToOpponent = rc.getLocation().directionTo(enemyrobots[i].getLocation());
                if (enemyrobots[i].getType() == RobotType.ARCHON && rc.getRobotCount() < 7) {
                    if (rc.canMove(directionToOpponent)) {
                        tryMoveDirectionOrRandom(directionToOpponent);
                    }
                    shootAtRobot(enemyrobots[i]);
                } else if (enemyrobots[i].getType() == RobotType.SOLDIER || enemyrobots[i].getType() == RobotType.LUMBERJACK){
                    if (rc.canMove(directionToOpponent.opposite())){
                        tryMoveDirectionOrRandom(directionToOpponent.opposite());
                    }

                } else if (enemyrobots[i].getType() == RobotType.GARDENER) { // if the enemy is a gardener we give special attention
                    RobotInfo gardenertoattack = getClosestRobot(enemyrobots, RobotType.GARDENER);
                    if (rc.getLocation().distanceTo(gardenertoattack.location) > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius+0.02f) {
                        Direction scoutToGardenerDir = rc.getLocation().directionTo(gardenertoattack.location);
                        MapLocation nextToGardener = gardenertoattack.getLocation().subtract(scoutToGardenerDir, 2.01f);
                        if (rc.canMove(nextToGardener)){
                            rc.move(nextToGardener);
                        } else{
                            tryMoveDirectionOrRandom(rc.getLocation().directionTo(gardenertoattack.location));
                        }
                    } else if (rc.getLocation().distanceTo(gardenertoattack.location) > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius){
                        // If we're right next to the gardener, rotate randomly either clockwise or counterclockwise
                        rc.setIndicatorDot(rc.getLocation(), 100, 0,0);
                        int isClockwise = (Math.random() > .5 ? 1 : -1);
                        Direction scoutToGardenerDir = rc.getLocation().directionTo(gardenertoattack.location);
                        Direction rotatedDirection = scoutToGardenerDir.rotateRightRads((float)(2*Math.PI/8.0*isClockwise));
                        MapLocation rotatedLocation = gardenertoattack.getLocation().add(
                                rotatedDirection.opposite(), RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius + .01f);
                        rc.setIndicatorDot(rotatedLocation, 0, 0, 100);
                        // If you rotate into a tree, the way it's currently implemented, you'll hit
                        if (rc.canMove(rotatedLocation) && !rc.isLocationOccupiedByTree(rotatedLocation)){
                            rc.move(rotatedLocation);
                        }
                    }
                    shootAtRobot(gardenertoattack);
                    break;
                }
                shootAtRobot(enemyrobots[i]);
                break;
            }
            if (rc.hasAttacked() || rc.hasMoved()){
                return;
            }
        }else{
            //previousShotAtEnemyID = 0;
        }
        if (Math.random() < .1){
            MapLocation[] archonLocations;
            if (rc.getTeam() == Team.A){
                archonLocations = rc.getInitialArchonLocations(Team.B);
            } else{
                archonLocations = rc.getInitialArchonLocations(Team.A);
            }
            targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        }


        //Check if targetDirection on map else change to a random direction

        if(!rc.onTheMap(rc.getLocation().add(targetDirection,5))){
            targetDirection = randomDirection();
        }

        if(previousShotAtEnemyID!=0){
            targetDirection = rc.getLocation().directionTo(previousShotAtEnemyLocaton);
        }
        tryMoveDirectionOrRandom(targetDirection);
    }

    private static int getRobotCount(RobotInfo[] robots, RobotType robottype) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType() == robottype)
                count++;
        }
        return count;
    }

    private static int getHostileCount(RobotInfo[] robots) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType().canAttack())
                count++;
        }
        return count;
    }

    private static RobotInfo getClosestRobot(RobotInfo[] robots, RobotType robottype) {
        RobotInfo closestRobot = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<robots.length;i++){
            if(robots[i].getType() == robottype && rc.getLocation().distanceTo(robots[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(robots[i].location);
                closestRobot = robots[i];
            }
        }
        return closestRobot;
    }

    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFireSingleShot()) {
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }else{
            previousShotAtEnemyID = 0;
            previousShotAtEnemyLocaton = null;
        }
    }



}
