package CameBackFromRadioShackPlayer;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public strictfp class RobotPlayer {
    @SuppressWarnings("unused")
    public static void run(RobotController rc) throws GameActionException {
        BaseBot.InitializeRobot(rc);
        Dodge.Initialize(rc);
        switch (rc.getType()) {
            case ARCHON:
                ArchonBot.initialize();
                break;
            case GARDENER:
                GardenerBot.initialize();
                break;
            case SOLDIER:
                SoldierBot.initialize();
                break;
            case SCOUT:
                ScoutBot.initialize();
                break;
            case LUMBERJACK:
                LumberjackBot.initialize();
                break;
        }
        while (true) {
            if (rc.getTeamBullets() >= 10000) {
                rc.donate(rc.getTeamBullets());
            }
            switch (rc.getType()) {
                case ARCHON:
                    ArchonBot.run();
                    break;
                case GARDENER:
                    GardenerBot.run();
                    break;
                case SOLDIER:
                    SoldierBot.run();
                    break;
                case SCOUT:
                    ScoutBot.run();
                    break;
                case LUMBERJACK:
                    LumberjackBot.run();
                    break;
            }
            BaseBot.shakeTreeIfPossible();
        }
    }
}

