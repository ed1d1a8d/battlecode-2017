package CameBackFromRadioShackPlayer;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotType;


public strictfp class ArchonBot extends BaseBot {

    private static int targetGardeners = 10;
    private static int numGardeners = 0;
    private static final double archonWanderDistance = 5;
    private static int lastRequested = 0;
    private static int governmentRank = 20;

    static void initialize() throws GameActionException {
        center = rc.getLocation();
        int numArchons = rc.getInitialArchonLocations(rc.getTeam()).length;
        targetGardeners = targetGardeners / numArchons + ((targetGardeners % numArchons == 0)?0:1);
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        try {
            if (rc.getLocation().distanceTo(center) > archonWanderDistance) {
                if (!tryMove(rc.getLocation().directionTo(center))) {
                    for (int i = 0; i < 20; i++) {
                        if (tryMove(randomDirection())) {
                            break;
                        }
                    }
                }
            } else {
              tryMove(randomDirection());
            }
                // This limits us to how many gardeners we can make.
                // The math.random() is so we don't build too many gardeners at once; there is probably a better solution

            double averageGardeners = Radio.RadioCustomChannels.read(Radio.RadioCustomChannels.Channel.TOTAL_GARDENER_HEALTH)
                    * 1.0 / RobotType.GARDENER.maxHealth;
            System.out.println(averageGardeners);
            if (rc.getTeamBullets() >= RobotType.GARDENER.bulletCost && 4 * averageGardeners <= (rc.getTreeCount())) { //6.2*numGardeners <= (rc.getTreeCount())) {//rc.getTreeCount()*3 >= rc.getRobotCount()-rc.getInitialArchonLocations(rc.getTeam()).length){
                System.out.println("Attempting to build gardener");
                for (int j = 0; j < 20; j++) {
                        Direction dir = randomDirection();
                        if (rc.canHireGardener(dir)) {
                            rc.hireGardener(dir);
                            ++numGardeners;
                            break;
                        }
                    }
                }

            if (rc.senseNearbyRobots(-1, enemy).length > 0 && lastRequested > governmentRank) {
                lastRequested = 0;
                Radio.RadioRequest.request(rc.getLocation(), RobotType.SOLDIER);
            }
            ++lastRequested;

            //System.out.println((rc.getTreeCount())*1.0/numGardeners);

            Clock.yield();

        } catch (Exception e) {
            System.out.println("Archon Exception");
            e.printStackTrace();
        }
    }
}
