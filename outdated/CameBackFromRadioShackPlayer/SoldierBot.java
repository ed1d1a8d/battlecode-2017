package CameBackFromRadioShackPlayer;

import battlecode.common.*;

public class SoldierBot extends BaseBot {
    private static int targettedArchon;
    private static MapLocation targetLocation;

    private static int previousShotAtEnemyID;
    private static int previousShot = 0;
    private static int previousShotDelay = 15;
    private static float previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocation;

    private static int lastRequest = 0;
    private static int requestTimeout = 20;

    public static void initialize() throws GameActionException {
        targettedArchon = generator.nextInt(enemyArchonLocations.length);
        targetLocation = enemyArchonLocations[targettedArchon];
    }

    public static void run() throws GameActionException {
        while (true) {
            try {
                runRound();
                Clock.yield();
            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);
        if(NearbyBullets != null && NearbyBullets.length>0) {
            tryDodgeAllBullets(NearbyBullets, rc.getLocation().directionTo(targetLocation));
        }
        MapLocation myLocation = rc.getLocation();

        // See if there are any nearby enemy robots
        RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        if (robots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...
            tryMove(myLocation.directionTo(robots[0].getLocation()));
            shootAtRobot(robots[0]);
            return;
        }

        // Move to target
        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
        }
        if (previousShotAtEnemyID != 0) {
            targetLocation = previousShotAtEnemyLocation;
        } else if (lastRequest > requestTimeout) {
            MapLocation requestLocation = Radio.RadioRequest.getRequest(RobotType.SOLDIER).location;
            if (requestLocation != null) {
                System.out.println("[debug] Soldier coming!");
                targetLocation = requestLocation;
                lastRequest = 0;
            } else {
                targetLocation = enemyArchonLocations[targettedArchon];
            }
        }
        ++lastRequest;
        ++previousShot;
        tryMoveDirectionOrRandom(rc.getLocation().directionTo(targetLocation));
    }

    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocation = robot.getLocation();
            previousShot = 0;
        } else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocation = robot.getLocation();
            previousShot = 0;
        } else {
            previousShotAtEnemyID = 0;
        }
    }
}
