package CameBackFromRadioShackPlayer;

import battlecode.common.*;

public strictfp class ScoutBot extends BaseBot {
    private static int targettedArchon;
    private static MapLocation targetLocation; // Where our current target is
    private static MapLocation turnMoveDirection; // What direction we would like to move our current turn

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocation;
    private static int previousShot = 0;
    private static int previousShotDelay = 15;

    private static int lastRequest = 20;
    private static final int requestTimeout = 20;

    public static void initialize() {
        targettedArchon = generator.nextInt(enemyArchonLocations.length);
        targetLocation = enemyArchonLocations[targettedArchon];
    }

    public static void run() throws GameActionException {
        while (true) {
            try {
                runRound();
                Clock.yield();
            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {
        ++lastRequest;
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);
        int myBullets = 0;
        if (NearbyBullets != null && NearbyBullets.length > 3) {
            tryDodgeAllBullets(NearbyBullets, rc.getLocation().directionTo(targetLocation));
            //tryDodgeNearestBullet(NearbyBullets);
        }

        //Check and shoot at nearby enemies
        Team enemy = rc.getTeam().opponent();
        RobotInfo[] enemyrobots = rc.senseNearbyRobots(-1, enemy);
        // If there are some enemies...
        if (enemyrobots.length > 0) {
            RobotInfo gardenertoattack = getClosestRobot(enemyrobots, RobotType.GARDENER);
            if (gardenertoattack != null) { // if the enemy is a gardener we give special attention
                if (lastRequest > requestTimeout) {
                    Radio.RadioRequest.request(RobotType.SCOUT, rc.getLocation());
                    lastRequest = 0;
                }
                Direction scoutToGardenerDir = rc.getLocation().directionTo(gardenertoattack.location);
                if (rc.getLocation().distanceTo(gardenertoattack.location) > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius + 0.02f) {
                    MapLocation nextToGardener = gardenertoattack.getLocation().subtract(scoutToGardenerDir, 2.01f);
                    if (!rc.hasMoved() && rc.canMove(nextToGardener)) {
                        rc.move(nextToGardener);
                    } else {
                        tryMoveDirectionOrRandom(rc.getLocation().directionTo(gardenertoattack.location));
                    }
                    System.out.println("[debug] Moving to gardener");
                } else if (rc.getLocation().distanceTo(gardenertoattack.location)
                                    > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius) {
                    // If right next to gardener already, constantly rotate to become hard to hit.
                    rc.setIndicatorDot(rc.getLocation(), 100, 0, 0);
                    int isClockwise = (Math.random() > .5 ? 1 : -1);
                    Direction rotatedDirection = scoutToGardenerDir.rotateRightRads((float) (2 * Math.PI / 8.0 * isClockwise));
                    MapLocation rotatedLocation = gardenertoattack.getLocation().add(
                            rotatedDirection.opposite(), RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius + .01f);
                    rc.setIndicatorDot(rotatedLocation, 0, 0, 100);
                    if (!rc.hasMoved() && rc.canMove(rotatedLocation) && !rc.isLocationOccupiedByTree(rotatedLocation)) {
                        rc.move(rotatedLocation);
                    }
                }
                shootAtRobot(gardenertoattack);
                return;
            }

            if (rc.hasAttacked()) {
                return;
            }
        } else { // if there are no enemies
            //previousShotAtEnemyID = 0;
        }

        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
        }
        if (previousShotAtEnemyID != 0) {
            targetLocation = previousShotAtEnemyLocation;
        } else if (lastRequest > requestTimeout) {
            MapLocation requestLocation = Radio.RadioRequest.getRequest(RobotType.SCOUT).location;
            if (requestLocation != null) {
                System.out.println("[debug] Scout coming!");
                targetLocation = requestLocation;
                lastRequest = 0;
            } else {
                targetLocation = enemyArchonLocations[generator.nextInt(enemyArchonLocations.length)];
            }
        }
        ++previousShot;
        tryMoveDirectionOrRandom(rc.getLocation().directionTo(targetLocation));

        for (RobotInfo robot : enemyrobots) {
            if (robot.getType() != RobotType.ARCHON) {
                if (robot.getID() == previousShotAtEnemyID && robot.getHealth() == previousShotAtEnemyHealth) {
                    //if the enemy is not getting weaker try another angle
                    tryMoveDirectionOrRandom(rc.getLocation().directionTo(robot.location).rotateRightDegrees(90));
                } else if (rc.canMove(rc.getLocation().directionTo(robot.getLocation()))) {
                    tryMoveDirectionOrRandom(rc.getLocation().directionTo(robot.getLocation()));
                }
                shootAtRobot(robot);
                return;
            }
        }
        for (RobotInfo robot : enemyrobots) {
            if (robot.getType() == RobotType.ARCHON) {
                if (robot.getID() == previousShotAtEnemyID && robot.getHealth() == previousShotAtEnemyHealth) {
                    //if the enemy is not getting weaker try another angle
                    tryMoveDirectionOrRandom(rc.getLocation().directionTo(robot.location).rotateRightDegrees(90));
                } else if (rc.canMove(rc.getLocation().directionTo(robot.getLocation()))) {
                    tryMoveDirectionOrRandom(rc.getLocation().directionTo(robot.getLocation()));
                }
                shootAtRobot(robot);
                return;
            }
        }
    }

    private static int getRobotCount(RobotInfo[] robots, RobotType robottype) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType() == robottype)
                count++;
        }
        return count;
    }

    private static int getHostileCount(RobotInfo[] robots) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType().canAttack())
                count++;
        }
        return count;
    }

    private static RobotInfo getClosestRobot(RobotInfo[] robots, RobotType robottype) {
        RobotInfo closestRobot = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<robots.length;i++){
            if(robots[i].getType() == robottype && rc.getLocation().distanceTo(robots[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(robots[i].location);
                closestRobot = robots[i];
            }
        }
        return closestRobot;
    }

    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFireSingleShot()) {
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocation = robot.getLocation();
            previousShot = 0;
        }else{
            previousShotAtEnemyID = 0;
        }
    }

}

