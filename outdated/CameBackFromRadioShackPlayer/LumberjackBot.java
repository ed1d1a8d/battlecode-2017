package CameBackFromRadioShackPlayer;

import battlecode.common.*;

import java.util.Random;


public class LumberjackBot extends BaseBot {
    private static MapLocation[] archonLocations;
    private static Direction targetDirection;
    private static Random rand;

    private static int lastRequest = 0;
    private static int requestTimeout = 30;

    static void initialize() throws GameActionException{
        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        rand = new Random(rc.getID());
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    private static Direction lastDirection = null;
    public static void run() throws GameActionException {

        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {
            TreeInfo[] neutralTrees  = rc.senseNearbyTrees(-1, Team.NEUTRAL);
            boolean chopped = false;
            for (TreeInfo t : neutralTrees) {
                int id = t.getID();
                if (rc.canChop(id)) {
                    rc.chop(id);
                    chopped = true;
                    break;
                }
            }
            if (!chopped && neutralTrees.length > 0) {
                Direction dir = rc.getLocation().directionTo(neutralTrees[0].location);
                if (tryMove(dir))
                    lastDirection = dir;
            }


            MapLocation requestLocation = null;
            if (lastRequest > requestTimeout) {
                requestLocation = Radio.RadioRequest.getRequest(RobotType.LUMBERJACK);
                lastRequest = 0;
            }
            if (requestLocation != null) {
                targetDirection = rc.getLocation().directionTo(requestLocation);
            } else {
                targetDirection = rc.getLocation().directionTo(archonLocations[0]);
            }
            tryMoveDirectionOrRandom(targetDirection);
            ++lastRequest;
/*
            // See if there are any enemy robots within striking range (distance 1 from lumberjack's radius)
            RobotInfo[] robots = rc.senseNearbyRobots(RobotType.LUMBERJACK.bodyRadius+ GameConstants.LUMBERJACK_STRIKE_RADIUS, enemy);

            if(robots.length > 0 && !rc.hasAttacked()) {
                // Use strike() to hit all nearby robots!
                rc.strike();
            } else {
                // No close robots, so search for robots within sight radius
                robots = rc.senseNearbyRobots(-1,enemy);

                // If there is a robot, move towards it
                if(robots.length > 0) {
                    MapLocation myLocation = rc.getLocation();
                    MapLocation enemyLocation = robots[0].getLocation();
                    Direction toEnemy = myLocation.directionTo(enemyLocation);
                    tryMove(toEnemy);
                } else {
                    // Move Randomly
                    tryMove(randomDirection());
                }
            }
*/
            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Lumberjack Exception");
            e.printStackTrace();
        }
    }
}
