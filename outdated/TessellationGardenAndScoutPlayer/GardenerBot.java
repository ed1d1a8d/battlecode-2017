package TessellationGardenAndScoutPlayer;

import battlecode.common.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;


public strictfp class GardenerBot extends BaseBot {

    private static int gardenRatio = 6;
    private static int numTrees = 0;
    private static boolean builtLumberjack = true;
    private static int age = 0;
    private static Direction moveDirection;
    private static ArrayList<Integer> shakenTrees = new ArrayList<>();
    private static Team myTeam = rc.getTeam();
    private static final float triangleWidth = 4.4f;
    private static final float SQRT3 = (float) Math.sqrt(3.0);
    private static MapLocation myLoc = null;

    private enum MoveType {
        FLEE_FOREST, JOIN_FOREST, RANDOM, FLEE_GARDENER, WATERING
    }
    private static Direction lastDirection = null;
    private static MoveType lastMoveType = null;
    private static final float eps = 1e-4f;
    private static int lumberjacks = 0;
    static void initialize() throws GameActionException {
        for (RobotInfo i : rc.senseNearbyRobots()) {
            if (i.getType() == RobotType.ARCHON) {
                moveDirection = rc.getLocation().directionTo(i.getLocation()).opposite();
                break;
            }
        }
    }

    private static boolean canPlantTree(MapLocation loc) throws GameActionException {
        if (myLoc.distanceTo(loc) > 3) //stride radius + body radius + tree body radius
            return false;
        if (rc.isCircleOccupied(loc, 1)) return false;
        return true;

    }

    private static boolean plantTree(MapLocation loc) throws GameActionException {
        float d = myLoc.distanceTo(loc);
        Direction dir = myLoc.directionTo(loc);
        System.out.println(rc.hasMoved());
        if (d > 2+eps) { if (rc.canMove(dir, d-2)) rc.move(dir, d - 2); else return false; }
        else if (d < 2-eps) { if (rc.canMove(dir.opposite(), 2-d)) rc.move(dir.opposite(), 2 - d); else return false; }
        System.out.println(rc.hasMoved());
        System.out.printf("%f (%f, %f)\n", d, dir.getDeltaX(1.0f), dir.getDeltaY(1.0f));
        if (rc.canPlantTree(dir)) {
            rc.plantTree(dir);
            return true;
        }
        return false;
    }

    private static boolean tryBuildTreeHereOrNeighbors(MapLocation loc) throws GameActionException {
        if (rc.getTeamBullets() < 50) return false;
        rc.setIndicatorDot(loc,0,255,0);


        ArrayList<MapLocation> posBuild = new ArrayList<MapLocation>();
        posBuild.add(loc);
        for (int i = 0; i < 6; i++) {
            posBuild.add(loc.add(new Direction((float) (Math.PI/3 * i)), triangleWidth));
        }
        //Collections.shuffle(posBuild);

        float mindist = Float.MAX_VALUE; MapLocation tloc = null;
        for(MapLocation nloc : posBuild) {
            rc.setIndicatorDot(nloc, 0, 255, 0);
            if (canPlantTree(nloc)) {
                float d = myLoc.distanceTo(nloc);
                if (d < mindist) {
                    mindist = d;
                    tloc = nloc;
                }
            }
        }
        if (tloc == null) return false;
        return plantTree(tloc);
    }

    private static boolean tryMoveRandom(TreeInfo[] myTrees) throws GameActionException {
        if (rc.hasMoved()) { rc.setIndicatorDot(rc.getLocation(), 255, 255, 255);  return false; }
        if (lastDirection != null) {
            double prob = 0.0;
            switch (lastMoveType) {
                case FLEE_FOREST: prob = 0.3; break;
                case FLEE_GARDENER: prob = 0.6; break;
                case JOIN_FOREST: prob = 0.6; break;
                case RANDOM: prob = 0.3; break;
                case WATERING: prob = 0.6; break;
            }

            if (Math.random() < prob) {
                if (tryMove(lastDirection))
                    return true;
            }
        }
        RobotInfo[] friends = rc.senseNearbyRobots(-1, myTeam);
        Arrays.sort(friends, new Comparator<RobotInfo>() {
            @Override
            public int compare(RobotInfo o1, RobotInfo o2) {
                float f = myLoc.distanceTo(o1.location)-myLoc.distanceTo(o2.location);
                if (f < 0) return -1;
                else if (f > 0) return 1;
                return 0;
            }
        });
        for (RobotInfo r : friends) {
            if (r.getType() == RobotType.GARDENER) {
                Direction dir = myLoc.directionTo(r.location).opposite();
                if (tryMove(myLoc.directionTo(r.location).opposite())) {
                    lastDirection = dir;
                    lastMoveType = MoveType.FLEE_GARDENER;
                    rc.setIndicatorLine(myLoc, r.location, 255, 100, 0);
                    return true;
                }
            }
        }

        float dx = 0, dy = 0;
        int c = 0;
        for (TreeInfo t : myTrees) {
            MapLocation loc = t.getLocation();
            ++c;
            dx +=  loc.x - myLoc.x;
            dy +=  loc.y - myLoc.y;
        }
        Direction prefdir = new Direction(dx, dy);
        final float[] fleeForestProbs = {-1, .05f, .05f, .05f, .2f, .3f, .5f, .6f, .7f};
        float fleeForestProb = c < fleeForestProbs.length ? fleeForestProbs[c] : fleeForestProbs[fleeForestProbs.length-1];
        if (Math.random() < fleeForestProb) {
            prefdir = prefdir.opposite();
            rc.setIndicatorLine(rc.getLocation(), rc.getLocation().add(prefdir, 2), 0, 255, 255);
            //rc.setIndicatorDot(rc.getLocation(),255,255,0);
            if (tryMove(prefdir)) {
                lastDirection = prefdir;
                lastMoveType = MoveType.FLEE_FOREST;
                return true;
            }
            prefdir = prefdir.opposite();
        }
        final float[] joinForestProbs = {-1, .8f, .7f, .6f, .5f, .5f, .5f};
        float joinForestProb = c < joinForestProbs.length ? joinForestProbs[c] : joinForestProbs[joinForestProbs.length-1];
        if (Math.random() < joinForestProb) { // move towards nascent forest
            if (rc.canMove(prefdir)) {
                //return tryMove(prefdir, 15, 2);
                if (tryMove(prefdir)) {
                    rc.setIndicatorDot(rc.getLocation(), 255,0,255);
                    lastDirection = prefdir;
                    lastMoveType = MoveType.JOIN_FOREST;
                    return true;
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            Direction dir = randomDirection();
            if (rc.canMove(dir)) {
                rc.setIndicatorLine(rc.getLocation(), rc.getLocation().add(dir,3), 100,200,0);
                lastDirection = dir;
                lastMoveType = MoveType.RANDOM;
                rc.move(dir);
                return true;
            }
        }
        return false;
    }

    private static boolean tryTessellate(TreeInfo[] myTrees) throws GameActionException {
        int col = Math.round((myLoc.x) / (triangleWidth / 2)), row = 0;
        float x = col * (triangleWidth / 2), y = 0;

        if (col % 2 == 0) {
            row = Math.round(myLoc.y / (triangleWidth * SQRT3));
            y = row * (triangleWidth * SQRT3);
        } else {
            row = Math.round((myLoc.y - triangleWidth * SQRT3 / 2) / (triangleWidth * SQRT3));
            y = (row + 0.5f) * (triangleWidth * SQRT3);
        }
        MapLocation toBuildLoc = new MapLocation(x, y);
        rc.setIndicatorDot(toBuildLoc, 0, 255, 0);

        if (!tryBuildTreeHereOrNeighbors(toBuildLoc)) {
            rc.setIndicatorDot(rc.getLocation(), 255, 0, 0);
            tryMoveRandom(myTrees);
            return false;
        }
        return true;
    }
    private static boolean tryMoveRoughlyToward(MapLocation loc) throws GameActionException {
        return tryMove(myLoc.directionTo(loc)); //might change in future

    }
    private static boolean tryWaterInOrder(TreeInfo[] myTrees, int start) throws  GameActionException {
        for (int i = start; i < myTrees.length; i++) {
            //rc.setIndicatorLine(myLoc, myTrees[i].getLocation(), 0, 0, 255);
            int idi = myTrees[i].getID();
            if (!rc.hasMoved()) {
                if (tryMoveRoughlyToward(myTrees[i].getLocation())) {
                    lastDirection = myLoc.directionTo(myTrees[i].getLocation());
                    lastMoveType = MoveType.WATERING;
                }
            }
            if (rc.canWater(idi)) {
                rc.water(idi);
                return true;
            }
        }
        return false;
    }
    private static boolean tryWater(TreeInfo[] myTrees) throws  GameActionException {

        if (myTrees[0].getHealth() < 30) {
            int id0 =  myTrees[0].getID();
            if (rc.canWater(id0)) { rc.water(id0); return true; }
            else {
                boolean watered = tryWaterInOrder(myTrees, 1);
                if (!rc.hasMoved() && tryMoveRoughlyToward(myTrees[0].getLocation())) {
                    lastDirection = myLoc.directionTo(myTrees[0].getLocation());
                    lastMoveType = MoveType.WATERING;
                    if (!watered)
                        return tryWaterInOrder(myTrees, 0);
                }
                return watered;
            }
        } else {
            return tryWaterInOrder(myTrees, 0);
        }
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        try {
            if (rc.getTeamBullets() >= 10000) {
                rc.donate(rc.getTeamBullets());
            }
            myLoc = rc.getLocation();

            if (Math.random() < .6 && rc.getTeamBullets() >= RobotType.SOLDIER.bulletCost){
                for (int i = 0; i < 10; i++) {
                    Direction dir = randomDirection();
                    if (rc.canBuildRobot(RobotType.SOLDIER, dir)){
                        rc.buildRobot(RobotType.SOLDIER, dir);
                    }
                }
            }
            if (Math.random() < .6 && rc.getTeamBullets() >= RobotType.SCOUT.bulletCost){
                for (int i = 0; i < 10; i++) {
                    Direction dir = randomDirection();
                    if (rc.canBuildRobot(RobotType.SCOUT, dir)){
                        rc.buildRobot(RobotType.SCOUT, dir);
                        break;
                    }
                }
            }




            /*
            if (lumberjacks < 1 && Math.random() < .05 && rc.getTeamBullets() >= RobotType.LUMBERJACK.bulletCost){
                for (int i = 0; i < 10; i++) {
                    Direction dir = randomDirection();
                    if (rc.canBuildRobot(RobotType.LUMBERJACK, dir)){
                        rc.buildRobot(RobotType.LUMBERJACK, dir);
                        ++lumberjacks;
                        break;
                    }
                }
            }*/


            TreeInfo[] myTrees = rc.senseNearbyTrees(-1, myTeam);
            Arrays.sort(myTrees, new Comparator<TreeInfo>() {
                @Override
                public int compare(final TreeInfo o1, final TreeInfo o2) {
                    float f = o1.getHealth() - o2.getHealth();
                    if (f < 0) return -1;
                    else if (f > 0) return 1;
                    return 0;
                }
            });

            boolean dontWater = Math.random() < .3;
            if (myTrees.length == 0 || myTrees[0].getHealth() >= 40)
                dontWater |= Math.random() < .3;

            if (myTrees.length > 0 && !dontWater) {
                tryWater(myTrees);
            }
            if (!rc.hasMoved()) {
                tryTessellate(myTrees);
            }
            if (dontWater) tryWaterInOrder(myTrees, 0);


            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            ++age;
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Gardener Exception");
            e.printStackTrace();
        }
    }
}
