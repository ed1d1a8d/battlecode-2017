package TessellationGardenAndScoutPlayer;


import battlecode.common.*;

import java.util.Random;


public class SoldierBot extends BaseBot {

    // Scoutbot currently simply moves towards the target direction - not a smart move homie
    private static Direction targetDirection;
    private static MapLocation[] archonLocations;

    private static int previousShotAtEnemyID;
    private static int previousShot = 0;
    private static int previousShotDelay = 15;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static Random rand;

    private static int lastRequest = 0;
    private static int requestTimeout = 20;
    private static Direction lastDirection = null;

    static void initialize() throws GameActionException {

        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        rand = new Random(rc.getID());
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {


        // The code you want your robot to perform every round should be in this loop
        while (true) {
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                runRound();


                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);
        if(NearbyBullets != null && NearbyBullets.length > 0) {
            tryDodgeAllBullets(NearbyBullets);
        }

        MapLocation myLocation = rc.getLocation();

        // See if there are any nearby enemy robots
        RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        if (robots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...
            Direction dir = myLocation.directionTo(robots[0].getLocation());

            if (tryMove(dir)) lastDirection = dir;

            shootAtRobot(robots[0]);
            return;
        }

        // Move to target
        if (previousShot > previousShotDelay) {
            previousShotAtEnemyID = 0;
        }
        if (previousShotAtEnemyID != 0) {
            targetDirection = rc.getLocation().directionTo(previousShotAtEnemyLocaton);
        } else if (lastRequest > requestTimeout) {
            MapLocation requestLocation = Radio.RadioRequest.getRequest(RobotType.SOLDIER);
            if (requestLocation != null) {
                System.out.println("[debug] Soldier coming!");
                targetDirection = rc.getLocation().directionTo(requestLocation);
                lastRequest = 0;
            } else {
                targetDirection = rc.getLocation().directionTo(archonLocations[0]);
            }
        }
        ++lastRequest;
        ++previousShot;
        if (lastDirection != null && !rc.hasMoved() && Math.random() < 0.6) {
            if (tryMove(lastDirection))
                return;
        }
        if (tryMove(targetDirection)) {
            lastDirection = targetDirection;
        } else {
            for (int i = 0; i < 10; i++) {
                Direction dir = randomDirection();
                if (tryMove(dir)) {
                    lastDirection = dir;
                    break;
                }
            }
        }
        //tryMoveDirectionOrRandom(targetDirection);
    }


    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            previousShot = 0;
        }
        else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
            previousShot = 0;
        }
        else{
            previousShotAtEnemyID = 0;
        }
    }

    private static void tryDodgeAllBullets(BulletInfo[] nearbyBullets) throws GameActionException {

        //if the bullet will collide in the direction that im going i change direction
        double randomchoice = Math.random();
        MapLocation futurePosition = rc.getLocation().add(targetDirection,
                rc.getType().strideRadius);

        int count = 0;
        while (willAnyCollideAtLocation(nearbyBullets, futurePosition) || BulletinWay(nearbyBullets, futurePosition)) {
            if(count ==3)
                break;
            // if (randomchoice > .5) {
            targetDirection = targetDirection.rotateRightDegrees(45);
            // } else {
            //     targetDirection = targetDirection.rotateLeftDegrees(45);
            // }
            futurePosition = rc.getLocation().add(targetDirection,
                    rc.getType().strideRadius);
            count++;
            //return;
        }
        tryMoveDirectionOrRandom(targetDirection);
    }

    private static boolean BulletinWay(BulletInfo[] nearbyBullets, MapLocation futurePosition) {
        //futurePosition.distanceTo(nearbyBullets[i].getLocation()) <= rc.getType().bodyRadius + nearbyBullets[i].getRadius()
        for(int i=0;i<nearbyBullets.length;i++){
            if(futurePosition.distanceTo(nearbyBullets[i].getLocation()) <= rc.getType().bodyRadius + nearbyBullets[i].getRadius()){
                return true;
            }
        }
        return false;
    }


    protected static BulletInfo getClosestBullet(BulletInfo[] bullets) {
        BulletInfo closestBullet = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<bullets.length;i++){
            if(rc.getLocation().distanceTo(bullets[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(bullets[i].location);
                closestBullet = bullets[i];
            }
        }
        return closestBullet;
    }
}
/*
import battlecode.common.*;
import java.util.Random;

import java.util.Arrays;
import java.util.Comparator;


public class SoldierBot extends BaseBot {

    // Scoutbot currently simply moves towards the target direction - not a smart move homie
    private static Direction targetDirection;
    private static MapLocation[] archonLocations;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static Random rand;

    static void initialize() throws GameActionException {

        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        rand = new Random(rc.getID());
    }
    private static final Team myTeam = rc.getTeam();
    private static Direction lastDirection = null;
    private static int time = 0;
    private static int lastTimeAttacked = -100000;

     /** run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     *
    static void run() throws GameActionException {

        // The code you want your robot to perform every round should be in this loop
        while (true) {

            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                ++time;
                MapLocation myLocation = rc.getLocation();

                // See if there are any nearby enemy robots
                RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);
                Arrays.sort(robots, new Comparator<RobotInfo>() {
                    @Override
                    public int compare(RobotInfo o1, RobotInfo o2) {
                        float f = myLocation.distanceTo(o1.location) - myLocation.distanceTo(o2.location);
                        if (f < 0) return -1;
                        else if (f > 0) return 1;
                        return 0;
                    }
                });
                boolean firedPentad = false;
                // If there are some...
                if (robots.length > 0) {
                    // And we have enough bullets, and haven't attacked yet this turn...
                    if (rc.canFirePentadShot()) {
                        rc.firePentadShot(rc.getLocation().directionTo(robots[0].location));
                        lastTimeAttacked = time;
                        firedPentad = true;
                    }
                    else if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(rc.getLocation().directionTo(robots[0].location));
                        lastTimeAttacked = time;
                    }
                }

                if (firedPentad && Math.random() < 0.8) continue;
                //if (time - lastTimeAttacked < 5 && Math.random() < 0.8) continue;

                if (Math.random() < 0.7 && dodgeBullets()) continue;



                if (robots.length > 0 && Math.random() < 0.9) {
                    Direction dir = myLocation.directionTo(robots[0].location);
                    if (tryMove(dir))
                        lastDirection = dir;
                }
                RobotInfo[] friends = rc.senseNearbyRobots(-1, myTeam);
                if (!rc.hasMoved() && friends.length > 0 && Math.random() < 0.1) {
                    Arrays.sort(friends, new Comparator<RobotInfo>() {
                        @Override
                        public int compare(RobotInfo o1, RobotInfo o2) {
                            if (o1.type == RobotType.GARDENER && o2.type != RobotType.GARDENER) return -1;
                            if (o2.type == RobotType.GARDENER && o1.type != RobotType.GARDENER) return 1;
                            float f = myLocation.distanceTo(o1.location) - myLocation.distanceTo(o2.location);
                            if (f < 0) return -1;
                            else if (f > 0) return 1;
                            return 0;
                        }
                    });
                    for (RobotInfo r : friends) {
                        Direction dir = myLocation.directionTo(r.location);
                        if (Math.random() < 0.2) dir = dir.opposite();
                        if (tryMove(dir)) {
                            lastDirection = dir;
                            break;
                        }
                    }
                }
                if (!rc.hasMoved()) {
                    if (lastDirection != null && Math.random() < 0.9) {
                        if (!tryMove(lastDirection)) {
                            Direction dir = randomDirection();
                            if (tryMove(dir))
                                lastDirection = dir;
                        }
                    } else {
                        Direction dir = randomDirection();
                        if (tryMove(dir))
                            lastDirection = dir;
                    }
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static boolean dodgeBullets() throws GameActionException {
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            BulletInfo closestBullet = getClosestBullet(NearbyBullets);
            if (willCollideWithMe(closestBullet)) {
                Direction dir = rc.getLocation().directionTo(closestBullet.location);
                if  (tryMove(dir.rotateLeftDegrees(90))) {
                    lastDirection = dir.rotateLeftDegrees(90);
                    return true;
                }
                if (tryMove(dir.rotateRightDegrees(90))) {
                    lastDirection = dir.rotateRightDegrees(90);
                    return true;
                }
                Direction rand = randomDirection();
                if (tryMove(rand)) {
                    lastDirection = rand;
                    return true;
                }
            }
        }
        return false;
/*
        MapLocation myLocation = rc.getLocation();

        // See if there are any nearby enemy robots
        RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        if (robots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...
            tryMove(myLocation.directionTo(robots[0].getLocation()));

            shootAtRobot(robots[0]);
            return;
        }

        // Move to target
        if(previousShotAtEnemyID!=0) {
            targetDirection = rc.getLocation().directionTo(previousShotAtEnemyLocaton);
        }
        else
        {
            targetDirection = rc.getLocation().directionTo(archonLocations[0]);
            //targetDirection = rc.getLocation().directionTo(archonLocations[rand.nextInt(archonLocations.length)]);
        }
        if(rc.getRoundNum()>100)
            tryMoveDirectionOrRandom(targetDirection);
    }


    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else{
            previousShotAtEnemyID = 0;
        }
    }

    protected static BulletInfo getClosestBullet(BulletInfo[] bullets) {
        BulletInfo closestBullet = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<bullets.length;i++){
            if(rc.getLocation().distanceTo(bullets[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(bullets[i].location);
                closestBullet = bullets[i];
            }
        }
        return closestBullet;
    }
}
*/