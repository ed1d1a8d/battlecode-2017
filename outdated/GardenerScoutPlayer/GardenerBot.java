package GardenerScoutPlayer;

import battlecode.common.*;


public strictfp class GardenerBot extends BaseBot{

    private static final int targetTrees = 5; //number of trees we want to maintain per gardener

    private static final double gardenerWanderDistance = 3; //Radius of Gardeners garden
    private static final int gardenerTenureAge = 50;

    private static int numTrees = 0;
    private static int age = 0;

    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        // The code you want your robot to perform every round should be in this loop
        while (true) {
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {

                // Generate a random direction
                Direction dir1 = randomDirection();

                // Randomly attempt to build a soldier or lumberjack in this direction
                if (rc.canBuildRobot(RobotType.SCOUT, dir1) && Math.random() < .3) {
                    rc.buildRobot(RobotType.SCOUT, dir1);
                }

                if (rc.getTeamBullets() >= 10000) {
                    rc.donate(rc.getTeamBullets());
                }
                if (age < gardenerTenureAge) {
                    tryMove(randomDirection());
                } else {
                    if (age == gardenerTenureAge) {
                        center = rc.getLocation();
                    }
                    for (int i = numTrees; i < targetTrees; i++) {
                        for (int j = 0; i < 20; i++) {
                            Direction dir = randomDirection();
                            if (rc.canPlantTree(dir)) {
                                rc.plantTree(dir);
                                ++numTrees;
                                break;
                            }
                        }
                    }

                    Team friend = rc.getTeam();
                    TreeInfo[] trees =
                            rc.senseNearbyTrees(rc.getType().bodyRadius + rc.getType().strideRadius, friend);
                    for (int i = 0; i < trees.length; i++) {
                        if (rc.canWater(trees[i].location)) {
                            if (trees[i].health < 45) {
                                rc.water(trees[i].location);
                                break;
                            }
                        }
                    }
                    if (rc.getLocation().distanceTo(center) > gardenerWanderDistance) {
                        tryMove(rc.getLocation().directionTo(center));
                    } else {
                        tryMove(randomDirection());
                    }
                }

                if (age == 33) {
                    center = rc.getLocation();
                }

                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();
                ++age;

            } catch (Exception e) {
                System.out.println("Gardener Exception");
                e.printStackTrace();
            }
        }
    }


}
