package GardenAndScoutDodgePlayer;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;


public strictfp class ArchonBot extends BaseBot {

    private static int targetGardeners = 10;
    private static int numGardeners = 0;
    private static final double archonWanderDistance = 5;

    static void initialize() throws GameActionException {
        System.out.println("wtf");
        center = rc.getLocation();
        int numArchons = rc.getInitialArchonLocations(rc.getTeam()).length;
        targetGardeners = targetGardeners / numArchons + ((targetGardeners % numArchons == 0)?0:1);
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        try {
            if (rc.getTeamBullets() >= 10000) {
                rc.donate(rc.getTeamBullets());
            }

            if (rc.getLocation().distanceTo(center) > archonWanderDistance) {
                if (!tryMove(rc.getLocation().directionTo(center))) {
                    for (int i = 0; i < 20; i++) {
                        if (tryMove(randomDirection())) {
                            break;
                        }
                    }
                }
            } else {
                // This limits us to how many gardeners we can make.
                // The math.random() is so we don't build too many gardeners at once; there is probably a better solution
                if (rc.getTreeCount()*3 >= rc.getRobotCount()-rc.getInitialArchonLocations(rc.getTeam()).length){
                    for (int j = 0; j < 20; j++) {
                        Direction dir = randomDirection();
                        if (rc.canHireGardener(dir)) {
                            rc.hireGardener(dir);
                            ++numGardeners;
                            break;
                        }
                    }
                }
                tryMove(randomDirection());
            }

            Clock.yield();

        } catch (Exception e) {
            System.out.println("Archon Exception");
            e.printStackTrace();
        }
    }
}
