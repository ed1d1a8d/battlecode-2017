package SoldierNavigationPlayer;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.TreeInfo;
import battlecode.common.GameConstants;

/*

 */
public class Navigation {


    private enum Mode {
    TO_END_LOCATION,
        BOUNDRY_BROWSE
    }

    private static Mode currentMode;
    public static MapLocation startLocation,endLocation,currentLocation;
    private static float neededProximityToTarget=0.0f;
    private static RobotController rc;

    private static final float globalbeamsize = 6.0f;
    private static final float beamangletowall1 = 45.0f;
    private static final float beamangletowall2 = 90.0f;


    // initialized to the last known direction that the navigation took or may also have a potential better direction
    // when ih Boundry_Browse mode
    private static Direction currentDirection;
    private static Direction wallDirection1;
    private static Direction wallDirection2;
    // Distance from previous location to endlocation
    private static float previousDistance;
    //Distance to end location when booundry_browse mode is activated
    private static float boundaryStartDistance;
    private static MapLocation boundaryStartLocation;

    //The location on the boundary that seems to be the most closest to the target
    private static float closestBoundaryToTargetDistance;
    private static MapLocation closestBoundaryToTargetLocation;
    private static int boundaryStepCount = 0;
    private static boolean isPossibleLoopInBoundary = false;
    private static boolean hasReachecdClosestBoundaryLocation = false;

    private static final int minBoundaryStepCountBeforeCheckingForLoop = 13;


    /**
     * Call this function to initialize navigation from current location to target
     * @param myRC
     * @param targetLocation
     */
    static void initializeToLocation(RobotController myRC, MapLocation targetLocation,float NeededProximityToTarget){
        rc = myRC;
        startLocation = rc.getLocation();
        endLocation = targetLocation;
        previousDistance = startLocation.distanceTo(endLocation);
        currentMode = Mode.TO_END_LOCATION;
        currentLocation = startLocation;
        currentDirection = startLocation.directionTo(endLocation);

        neededProximityToTarget = NeededProximityToTarget;
        resetBoundaryParameters();


    }

    private static void resetBoundaryParameters() {
        //boundary values
        wallDirection1 = null;
        wallDirection2 = null;
        boundaryStartDistance = 0.0f;
        isPossibleLoopInBoundary = false;
        hasReachecdClosestBoundaryLocation = false;
        boundaryStartLocation = null;
        boundaryStepCount = 0;
        closestBoundaryToTargetLocation = null;
        closestBoundaryToTargetDistance = 0;

    }

    static boolean isInitialized() {
        if (endLocation != null) {
            return true;
        }
        return false;

    }

    static boolean hasReachedTarget(){
        currentLocation = rc.getLocation();
        if(currentLocation.distanceTo(endLocation)<=neededProximityToTarget){
            return true;
        }
        return false;
    }

    static boolean hasProbableLoop(){
        if(isPossibleLoopInBoundary && currentMode == Mode.BOUNDRY_BROWSE){
            return true;
        }
        return false;
    }

    static boolean hasReachedClosestBoundaryLocation(){
        return hasReachecdClosestBoundaryLocation;
    }

    static void terminate(){
        debug_calledterminate();
        initializeToLocation(rc,null,1);
    }

    private static void debug_calledterminate() {
        System.out.println("Called Terminate");
    }

    /**
     *  Call this function in every round to perform navigation to end
     returns
     * @return true if it moved
     * @throws GameActionException
     */
    static boolean Navigate() throws GameActionException {
        return Navigate(false);
    }

    public static boolean TryStepCloserToBoundary() throws GameActionException {

        currentLocation = rc.getLocation();
        Direction dirtoend = currentLocation.directionTo(endLocation);
        debug_printranTryStepCloserToBoundary();
        if(!rc.hasMoved() && rc.canMove(dirtoend)){
            rc.move(dirtoend);
            //tryMove(dirtoend);
            if(!checkForNonFriendlyTreeOnBoundary(dirtoend,globalbeamsize)) {
                debug_printranTryStepCloserToBoundaryinsidefor();
                currentMode = Mode.TO_END_LOCATION;
                resetBoundaryParameters();
            }
            return true;
        }
        return false;

    }

    private static void debug_printranTryStepCloserToBoundary() {
        System.out.println("called :trystepclosertoboundary");

    }

    private static void debug_printranTryStepCloserToBoundaryinsidefor() {
        System.out.println("called :trystepclosertoboundary inside for");

    }

    /**
     *  Call this function in every round to perform navigation to boundaryLocation
     returns
     * @return true if it moved
     * @throws GameActionException
     */
    static boolean Navigate(boolean toClosestBoundaryLocation) throws GameActionException {
        currentLocation = rc.getLocation();
        if (endLocation == null || currentLocation.distanceTo(endLocation) <= 1.5f) {
            return false;
        }
        boolean moved = false;
        if(currentMode == Mode.TO_END_LOCATION){
            wallDirection1 = null;
            wallDirection2 = null;
            moved = tryMove(currentLocation.directionTo(endLocation));

        }else if(currentMode == Mode.BOUNDRY_BROWSE){// currentmode is Mode.BoundryBrowse


            if(toClosestBoundaryLocation == true && !hasReachecdClosestBoundaryLocation){
                moved = tryMove(currentLocation.directionTo(closestBoundaryToTargetLocation));
            }else{
                moved = tryMove(currentDirection);
            }


            // use a beam to check for wall and move
            float currectdistance = currentLocation.distanceTo(endLocation);
            //Heuristic used to check when to leave boundary
            if((!blockedByObjectOrMapBoundary(wallDirection1,globalbeamsize) && !blockedByObjectOrMapBoundary(wallDirection2,globalbeamsize))
                    || boundaryStartDistance - currectdistance >= 1 || boundaryStepCount >= GameConstants.MAP_MAX_WIDTH + GameConstants.MAP_MAX_HEIGHT){
                currentMode = Mode.TO_END_LOCATION;
                resetBoundaryParameters();
            }

            if(moved){
                boundaryStepCount++;
            }
        }


        rc.setIndicatorLine(currentLocation,currentLocation.add(currentDirection,2),0,0,0);
        debug_PrintDistandMode();
        return moved;
    }

    private static void debug_PrintDistandMode() {
        System.out.println(previousDistance + ":" + currentMode.toString());
    }

    /**
     * returns whether a beam from start to end is blocked at any location
     * @param startlocation
     * @param endLocation
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean blockedByObjectOrMapBoundary(MapLocation startlocation, MapLocation endLocation, float beamLength) throws GameActionException {
        Direction directiontoCheck = startlocation.directionTo(endLocation);
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);
        MapLocation currentLocation;

        for(float i = rc.getType().bodyRadius+0.5f;i< rc.getType().bodyRadius + beamLength; i = i+1.0f){
            currentLocation = startlocation.add(directiontoCheck,i);
            rc.setIndicatorLine(startlocation,currentLocation,255,255,(int)(153+i));
            if(!rc.onTheMap(currentLocation) || rc.isLocationOccupied(currentLocation)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether a beam in direction directiontocheck is blocked by any object
     * @param directiontoCheck
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean blockedByObjectOrMapBoundary(Direction directiontoCheck, float beamLength) throws GameActionException {

        //float totaldistancetocheck = rc.getType().bodyRadius + beamLength;
        float totaldistancetocheck = beamLength;

        rc.setIndicatorLine(currentLocation,currentLocation.add(directiontoCheck,totaldistancetocheck),255,255,153);
        MapLocation newcurrentLocation;

        for(float i = rc.getType().bodyRadius+0.5f;i< totaldistancetocheck; i = i+1.0f){
            newcurrentLocation = currentLocation.add(directiontoCheck,i);
            if(!rc.canSenseLocation(newcurrentLocation))
                return false;
            rc.setIndicatorLine(currentLocation,newcurrentLocation,255,255,(int)(153+i));
            if(!rc.onTheMap(newcurrentLocation) || rc.isLocationOccupiedByTree(newcurrentLocation)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether a beam in direction directiontocheck is blocked
     * @param directiontoCheck
     * @param beamLength
     * @return
     * @throws GameActionException
     */
    private static boolean checkForNonFriendlyTreeOnBoundary(Direction directiontoCheck, float beamLength) throws GameActionException {

        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);
        MapLocation newcurrentLocation;
        TreeInfo currentTree;
        for(float i = rc.getType().bodyRadius+0.5f;i< rc.getType().bodyRadius + beamLength; i = i+1.0f){
            newcurrentLocation = currentLocation.add(directiontoCheck,i);
            if(!rc.canSenseLocation(newcurrentLocation))
                return false;
            rc.setIndicatorLine(currentLocation,newcurrentLocation,255,255,(int)(153+i));
            if(rc.isLocationOccupiedByTree(newcurrentLocation)){
                //Check if the tree is our own tree and if so return false;
                currentTree = rc.senseTreeAtLocation(newcurrentLocation);
                if(currentTree.getTeam()==rc.getTeam()){
                    return false;
                }else{
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles directly in the path.
     *
     * @param dir The intended direction of movement
     * @return true if a move was performed
     * @throws GameActionException
     */
    private static boolean tryMove(Direction dir) throws GameActionException {

        return tryMove(dir,10,18);
    }

    /**
     * Attempts to move in a given direction, while avoiding small obstacles direction in the path.
     *
     * @param dir The intended direction of movement
     * @param degreeOffset Spacing between checked directions (degrees)
     * @param checksPerSide Number of extra directions checked on each side, if intended direction was unavailable
     * @return true if a move was performed
     * @throws GameActionException
     */
    private static boolean tryMove(Direction dir, float degreeOffset, int checksPerSide) throws GameActionException {

        // First, try intended direction
        if (rc.hasMoved() && rc.canMove(dir)) {
            rc.move(dir);
            recalculateParameters(dir);
            return true;
        }

        // Now try a bunch of similar angles
        boolean moved = false;
        int currentCheck = 1;

        //find a good direction to start
        Direction directiontoend = currentLocation.directionTo(endLocation);
        float degsbetween = dir.degreesBetween(directiontoend);
        boolean startLeft = (degsbetween>0);
        while(currentCheck<=checksPerSide) {
            if(startLeft) {
                // Try the offset of the left side
                if (tryMoveLeft(dir, degreeOffset, currentCheck)) return true;
                // Try the offset on the right side
                if (tryMoveRight(dir, degreeOffset, currentCheck)) return true;
            }else{
                // Try the offset on the right side
                if (tryMoveRight(dir, degreeOffset, currentCheck)) return true;
                // Try the offset of the left side
                if (tryMoveLeft(dir, degreeOffset, currentCheck)) return true;
            }
            // No move performed, try slightly further
            currentCheck++;
        }

        // A move never happened, so return false.
        return false;
    }

    private static boolean tryMoveRight(Direction dir, float degreeOffset, int currentCheck) throws GameActionException {
        if(rc.hasMoved())
            return false;
        Direction rightRotatedDirection = dir.rotateRightDegrees(degreeOffset * currentCheck);
        if(rc.canMove(rightRotatedDirection)) {
            rc.move(rightRotatedDirection);
            recalculateParameters(rightRotatedDirection);

            return true;
        }
        return false;
    }

    private static boolean tryMoveLeft(Direction dir, float degreeOffset, int currentCheck) throws GameActionException {
        if(rc.hasMoved())
            return false;
        Direction leftRotatedDirection = dir.rotateLeftDegrees(degreeOffset * currentCheck);
        if(rc.canMove(leftRotatedDirection)) {
            rc.move(leftRotatedDirection);
            recalculateParameters(leftRotatedDirection);

            return true;
        }
        return false;
    }

    /**
     * THis function calculates certain parameters that are bookeeping to follow boundary
     * in boundary browse mode
     * @param dir
     * @throws GameActionException
     */
    private static void recalculateParameters(Direction dir) throws GameActionException {
        float newdistance = currentLocation.distanceTo(endLocation);
        if(newdistance > previousDistance && currentMode == Mode.TO_END_LOCATION){
            currentMode = Mode.BOUNDRY_BROWSE;
            boundaryStartDistance = newdistance;
            boundaryStartLocation = currentLocation;
            boundaryStepCount = 0;
            isPossibleLoopInBoundary = false;
            hasReachecdClosestBoundaryLocation = false;
            closestBoundaryToTargetDistance = newdistance;
            closestBoundaryToTargetLocation = currentLocation;

            // pwall direction will be null the first time mode changes to boundary_browse and
            // there is a free movement in the direction the robot is going

                Direction directiontoend = currentLocation.directionTo(endLocation);
                float degsbetween = dir.degreesBetween(directiontoend);
                if(degsbetween < 0){
                    wallDirection1 = dir.rotateRightDegrees(beamangletowall1);
                    wallDirection2 = dir.rotateRightDegrees(beamangletowall2);
                }else{
                    wallDirection1 = dir.rotateLeftDegrees(beamangletowall1);
                    wallDirection2 = dir.rotateLeftDegrees(beamangletowall2);
                }


                //just in case no wall is found
                if(!blockedByObjectOrMapBoundary(wallDirection1,globalbeamsize)
                        && !blockedByObjectOrMapBoundary(wallDirection2,globalbeamsize)){
                    wallDirection1 = wallDirection1.opposite();
                    wallDirection2 = wallDirection2.opposite();
                }
                rc.setIndicatorLine(currentLocation,currentLocation.add(wallDirection1,3),102,204,255);
                rc.setIndicatorLine(currentLocation,currentLocation.add(wallDirection2,3),102,204,255);
            //}
        }

        if(wallDirection1 !=null && currentMode == Mode.BOUNDRY_BROWSE){
            //it is going in intended direction so recalculate direction and walldirection
            //probe towards wall
            Direction newDirection = probeTowardsWall(dir, wallDirection1);
            if(newDirection!=null)
                currentDirection = newDirection;
            float anglesbetween = dir.degreesBetween(wallDirection1);
            if(anglesbetween < 0){
                wallDirection1 = currentDirection.rotateRightDegrees(beamangletowall1);
                wallDirection2 = currentDirection.rotateRightDegrees(beamangletowall2);
            }else{
                wallDirection1 = currentDirection.rotateLeftDegrees(beamangletowall1);
                wallDirection2 = currentDirection.rotateLeftDegrees(beamangletowall2);
            }

            //if its a potential loop then set the flag
            if(boundaryStepCount >= minBoundaryStepCountBeforeCheckingForLoop
                    && currentLocation.distanceTo(boundaryStartLocation)<=1){
                isPossibleLoopInBoundary = true;
            }

            // if it is looking for the closest boundary and its reached
            if(closestBoundaryToTargetLocation!=null
                    && currentLocation.distanceTo(closestBoundaryToTargetLocation)<=1.0f){
                hasReachecdClosestBoundaryLocation = true;
            }

        }else{ // not it boundary_browse mode
            currentDirection = dir;
        }


        previousDistance = newdistance;
    }

    /**
     * Tries to probe in a specific direction to see if a move is possible
     * @param currentmovedirection
     * @param wallDirection
     * @return
     * returns the direction that is moveable that is closest to the wall
     */
    private static Direction probeTowardsWall(Direction currentmovedirection, Direction wallDirection) {
        Direction currentvaliddirection = currentmovedirection;
        float anglesbetween = currentmovedirection.degreesBetween(wallDirection);

        int currentCheck = 1;

        Direction currentCheckDirection = null;
        while(currentCheck<=9) {
            if(anglesbetween < 0){
                currentCheckDirection = currentmovedirection.rotateRightDegrees(currentCheck * 10);
            }else{
                currentCheckDirection = currentmovedirection.rotateLeftDegrees(currentCheck * 10);
            }
            if(rc.canMove(currentCheckDirection)) {
                currentvaliddirection = currentCheckDirection;
            }
            else{
                return currentvaliddirection;
            }
            currentCheck++;
        }
        return currentvaliddirection;

    }
}
