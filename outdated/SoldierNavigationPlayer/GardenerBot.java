package SoldierNavigationPlayer;

import battlecode.common.*;

import java.util.ArrayList;


public strictfp class GardenerBot extends BaseBot {

    private static final int targetTrees = 5; //number of trees we want to maintain per gardener

    private static final double gardenerWanderDistance = 3; //Radius of Gardeners garden
    private static final int gardenerTenureAge = 50;

    private static int gardenRatio = 10;
    private static float gardenRadius = 1.61f; // Derived from some fancy pants mathematics
    private static int numTrees = 0;
    private static boolean builtLumberjack = false;
    private static int soldierCount = 0;
    private static MapLocation center;
    private static int age = 0;
    private static Direction moveDirection;
    private static ArrayList<Integer> shakenTrees = new ArrayList<>();

    static int getNumberValidLocations(){
        int numValidLocations = 0;
        for (float i = 0; i < 2*Math.PI; i+= 2*Math.PI/gardenRatio) {
            if (rc.canPlantTree(new Direction(i))){
                numValidLocations ++;
            }
        }
        return numValidLocations;
    }
    static void initialize() throws GameActionException{

        for (RobotInfo i : rc.senseNearbyRobots()){
            if (i.getType() == RobotType.ARCHON ){
                moveDirection = rc.getLocation().directionTo(i.getLocation()).opposite();
                break;
            }
        }
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        // The code you want your robot to perform every round should be in this loop
//        Random gen = new Random(rc.getID());
//        moveDirection = new Direction((float)gen.nextDouble() * 2 * (float)Math.PI);
        // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
        try {
            if (rc.getTeamBullets() >= 10000) {
                rc.donate(rc.getTeamBullets());
            }
            /*if (Math.random() < .3 && rc.getTeamBullets() >= RobotType.SCOUT.bulletCost){
                for (float i = 0; i < 2*Math.PI; i+= 2*Math.PI/gardenRatio) {
                    if (rc.canBuildRobot(RobotType.SCOUT, new Direction(i))){
                        rc.buildRobot(RobotType.SCOUT, new Direction(i));
                        numTrees++;
                    }
                }
            }*/
            if (soldierCount<=10){
                //for (float i = 0; i < 2*Math.PI; i+= 2*Math.PI/gardenRatio) {
                Direction buildDIrection = randomDirection();


                    rc.setIndicatorLine(rc.getLocation(), rc.getLocation().add(buildDIrection), 0, 0, 0);
                    if (rc.canBuildRobot(RobotType.SOLDIER, buildDIrection)) {
                        rc.buildRobot(RobotType.SOLDIER, buildDIrection);
                        numTrees++;
                        soldierCount++;
                         //break;
                    }

                //}
            }
            TreeInfo[] neutralTrees = rc.senseNearbyTrees(rc.getType().bodyRadius + rc.getType().strideRadius, Team.NEUTRAL);
            if (neutralTrees.length > 0){
                for (int i=0; i< neutralTrees.length; i++){
                    if (numTrees == 0 && !builtLumberjack){
                        for (int j = 0; j < 20; j++) {
                            Direction dir = randomDirection();
                            if (!rc.hasAttacked() && rc.canBuildRobot(RobotType.LUMBERJACK, dir)) {
                                rc.buildRobot(RobotType.LUMBERJACK, dir);
                                builtLumberjack = true;
                                break;
                            }
                        }
                    }
                    if (!shakenTrees.contains(neutralTrees[i].ID) && rc.canShake()){
                        double b = rc.getTeamBullets();
                        rc.shake(neutralTrees[i].ID);
                        shakenTrees.add(neutralTrees[i].ID);
                        System.out.println(rc.getTeamBullets() - b);
                    }
                }
            }
            int numValidLocations = getNumberValidLocations();
            if (center != null && rc.getTeamBullets() > 50 && numValidLocations > 1){
                rc.setIndicatorDot(center, 50, 0,0);
                for (float i = 0; i < 2*Math.PI; i+= 2*Math.PI/gardenRatio) {
//                        MapLocation treeDirection = new MapLocation((float)(Math.cos(i)*gardenRadius), (float)(Math.sin(i)*gardenRadius));
//                        MapLocation treeLocation = new MapLocation(treeDirection.x+center.x, treeDirection.y+center.y);
//                        if (!rc.isCircleOccupied(treeLocation, 1)){
//                            rc.move(new MapLocation(treeLocation.x-treeDirection.x*1.5f, treeLocation.y-treeDirection.y*1.5f));
//                            rc.plantTree(new Direction(treeDirection.x, treeDirection.y));
//                            break;
//                        }
                    if (rc.canPlantTree(new Direction(i))){
                        rc.plantTree(new Direction((i)));
                        numTrees++;
                    }
                }
            }

            Team friend = rc.getTeam();
            TreeInfo[] trees =
                    rc.senseNearbyTrees(rc.getType().bodyRadius + rc.getType().strideRadius, friend);
            for (int i = 0; i < trees.length; i++) {
                if (rc.canWater(trees[i].location)) {
                    if (trees[i].health < 45) {
                        rc.water(trees[i].location);
                        break;
                    }
                }
            }
            if (center == null) {
                tryMove(moveDirection);
                if (age == 3) {
                    center = rc.getLocation();

                }
            }

            // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
            ++age;
            Clock.yield();

        } catch (Exception e) {
            System.out.println("Gardener Exception");
            e.printStackTrace();
        }
    }
}
