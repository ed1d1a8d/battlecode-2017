package SoldierNavigationPlayer;

import battlecode.common.*;

import java.util.Random;


public class SoldierBot extends BaseBot {

    // Scoutbot currently simply moves towards the target direction - not a smart move homie

    private static MapLocation[] archonLocations;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static MapLocation currentLocation;
    private static Random rand;

    static void initialize() throws GameActionException {

        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        MapLocation chosenarchon = archonLocations[(int)(Math.random()*archonLocations.length)];


        rand = new Random(rc.getID());
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {


        // The code you want your robot to perform every round should be in this loop
        while (true) {
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                runRound();


                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {

        currentLocation = rc.getLocation();
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            BulletInfo closestBullet = getClosestBullet(NearbyBullets);

            if (willCollideWithMe(closestBullet)) {
                tryMove(randomDirection());
                return;
            }
        }



        // See if there are any nearby enemy robots
        RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        if (robots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...
            tryMove(currentLocation.directionTo(robots[0].getLocation()));

            shootAtRobot(robots[0]);
            return;
        }


        runSoldierNavigation();


    }

    private static void runSoldierNavigation() throws GameActionException {
        //if it reached archon and archon not found... move to next
        if(!Navigation.isInitialized() || Navigation.hasReachedTarget()){
            MapLocation chosenarchon = archonLocations[(int)(Math.random()*archonLocations.length)];
            //Initialize naviation to move to chosenarchon
            Navigation.initializeToLocation(rc,chosenarchon,1.0f);
        }

        // What should the bot do if there is a loop
        if(Navigation.hasProbableLoop()){
            //It could move to the boundary location closest to the target and clear trees

            //if it hasent reached its boundary
            if(!Navigation.hasReachedClosestBoundaryLocation()){
                Navigation.Navigate(true);
            }else{//reached closestboundarylocation
                rc.setIndicatorDot(currentLocation,200,0,0);

                if(!TryClearTrees(currentLocation.directionTo(Navigation.endLocation)))
                {
                    //if no tree was found try move towards endlocation
                    Navigation.TryStepCloserToBoundary();
                }
            }

        } else {
            Navigation.Navigate();
        }

    }

    //returns false if no tree was found immediately in front
    //else fires at tree

    private static boolean TryClearTrees(Direction directionToClear) throws GameActionException {
        //rc.setIndicatorLine(startlocation,endLocation,255,255,153);


        int currentoffsettocheck = 1;
        Direction directiontocheck = directionToClear;

        if (CheckAndFireAtTree(directiontocheck)) return true;
        while(currentoffsettocheck<=3){

            //rc.setIndicatorLine(currentLocation,locationToCheck,255,153,255);

            Direction rightRotatedDirection = directiontocheck.rotateRightDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(rightRotatedDirection)) return true;
            Direction leftRotatedDirection = directiontocheck.rotateLeftDegrees(15 * currentoffsettocheck);
            if (CheckAndFireAtTree(leftRotatedDirection)) return true;

            currentoffsettocheck++;
        }
            return false;
    }

    private static boolean CheckAndFireAtTree(Direction directiontocheck) throws GameActionException {
        float distancetocheck = rc.getType().bodyRadius+0.5f;
        MapLocation locationToCheck,locationToCheck2;
        locationToCheck = currentLocation.add(directiontocheck,distancetocheck);
        locationToCheck2 = currentLocation.add(directiontocheck,distancetocheck+0.5f);
        boolean istreeatlocation = rc.isLocationOccupiedByTree(locationToCheck);
        boolean istreeatlocation2 = rc.isLocationOccupiedByTree(locationToCheck2);
        if( istreeatlocation || istreeatlocation2){
            if(!rc.hasAttacked()){
                TreeInfo treeinfo = null;
                if(istreeatlocation)
                    treeinfo = rc.senseTreeAtLocation(locationToCheck);
                else
                    treeinfo = rc.senseTreeAtLocation(locationToCheck2);
                if(treeinfo.getTeam() != rc.getTeam()) {
                    if (rc.canFirePentadShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.firePentadShot(directiontocheck);
                    }
                    else if (rc.canFireSingleShot()) {
                        // ...Then fire a bullet in the direction of the enemy.
                        rc.fireSingleShot(directiontocheck);
                    }
                }else{
                    //this is my own team tree blocking
                    Navigation.terminate();
                }
            }

            return true;
        }
        return false;
    }


    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(currentLocation.directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else{
            previousShotAtEnemyID = 0;
        }
    }

    private static BulletInfo getClosestBullet(BulletInfo[] bullets) {
        BulletInfo closestBullet = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<bullets.length;i++){
            if(currentLocation.distanceTo(bullets[i].location) < currentclosestdistance){
                currentclosestdistance = currentLocation.distanceTo(bullets[i].location);
                closestBullet = bullets[i];
            }
        }
        return closestBullet;
    }
}

