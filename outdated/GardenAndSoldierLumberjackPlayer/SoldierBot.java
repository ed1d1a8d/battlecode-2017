package GardenAndSoldierLumberjackPlayer;

import battlecode.common.*;
import java.util.Random;


public class SoldierBot extends BaseBot {

    // Scoutbot currently simply moves towards the target direction - not a smart move homie
    private static Direction targetDirection;
    private static MapLocation[] archonLocations;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation previousShotAtEnemyLocaton;
    private static Random rand;

    static void initialize() throws GameActionException {

        if (rc.getTeam() == Team.A){
            archonLocations = rc.getInitialArchonLocations(Team.B);
        } else{
            archonLocations = rc.getInitialArchonLocations(Team.A);
        }

        //archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());
        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        rand = new Random(rc.getID());
    }
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {


        // The code you want your robot to perform every round should be in this loop
        while (true) {
            // Try/catch blocks stop unhandled exceptions, which cause your robot to explode
            try {
                runRound();


                // Clock.yield() makes the robot wait until the next turn, then it will perform this loop again
                Clock.yield();

            } catch (Exception e) {
                System.out.println("Soldier Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {
        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if (NearbyBullets != null && NearbyBullets.length > 0) {
            BulletInfo closestBullet = getClosestBullet(NearbyBullets);

            if (willCollideWithMe(closestBullet)) {
                tryMove(randomDirection());
                return;
            }
        }

        MapLocation myLocation = rc.getLocation();

        // See if there are any nearby enemy robots
        RobotInfo[] robots = rc.senseNearbyRobots(-1, enemy);

        // If there are some...
        if (robots.length > 0) {
            // And we have enough bullets, and haven't attacked yet this turn...
            tryMove(myLocation.directionTo(robots[0].getLocation()));

            shootAtRobot(robots[0]);
            return;
        }

        // Move to target
        if(previousShotAtEnemyID!=0) {
            targetDirection = rc.getLocation().directionTo(previousShotAtEnemyLocaton);
        }
        else
        {
            targetDirection = rc.getLocation().directionTo(archonLocations[0]);
            //targetDirection = rc.getLocation().directionTo(archonLocations[rand.nextInt(archonLocations.length)]);
        }
        if(rc.getRoundNum()>100)
            tryMoveDirectionOrRandom(targetDirection);
    }


    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFirePentadShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.firePentadShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else if (rc.canFireSingleShot()) {
            // ...Then fire a bullet in the direction of the enemy.
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
            previousShotAtEnemyLocaton = robot.getLocation();
        }
        else{
            previousShotAtEnemyID = 0;
        }
    }

    private static BulletInfo getClosestBullet(BulletInfo[] bullets) {
        BulletInfo closestBullet = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<bullets.length;i++){
            if(rc.getLocation().distanceTo(bullets[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(bullets[i].location);
                closestBullet = bullets[i];
            }
        }
        return closestBullet;
    }
}
