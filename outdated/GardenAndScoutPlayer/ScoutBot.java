package GardenAndScoutPlayer;

import battlecode.common.*;
import java.lang.Math;
import java.util.Random;


public strictfp class ScoutBot extends BaseBot{

    //private static int targetGardeners = 10;
    //private static int numGardeners = 0;
    //private static final double archonWanderDistance = 5;

    // Scoutbot currently simply moves towards the target direction - not a smart move homie
    private static Direction targetDirection;

    private static int previousShotAtEnemyID;
    private static double previousShotAtEnemyHealth;
    private static MapLocation[] archonLocations;
    private static void initializeScout(){

        //as initialarchonlocation only give you initial locations
        archonLocations = rc.getInitialArchonLocations(rc.getTeam().opponent());

        targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
    }

    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    static void run() throws GameActionException {
        initializeScout();
        center = rc.getLocation();
        int numArchons = rc.getInitialArchonLocations(rc.getTeam()).length;
        while (true) {
            try {
                runRound();

                Clock.yield();

            } catch (Exception e) {
                System.out.println("Archon Exception");
                e.printStackTrace();
            }
        }
    }

    private static void runRound() throws GameActionException {

        //Check and dodge nearby bullets
        BulletInfo[] NearbyBullets = rc.senseNearbyBullets(10);

        if(NearbyBullets != null && NearbyBullets.length>0) {
            for(int i=0;i<NearbyBullets.length;i++){
                //if the bullet will collide in the direction that im going i change direction
                if(willCollideAtLocation(NearbyBullets[i],rc.getLocation().add(targetDirection,
                        rc.getType().bodyRadius + rc.getType().strideRadius))){
                    if(Math.random()>.5) {
                        targetDirection = targetDirection.rotateRightDegrees(90);
                    }else{
                        targetDirection = targetDirection.rotateLeftDegrees(90);
                    }
                    tryMoveDirectionOrRandom(targetDirection);
                    return;
                }
            }
        }

        //Check and shoot at nearby enemies
        Team enemy = rc.getTeam().opponent();
        RobotInfo[] enemyrobots = rc.senseNearbyRobots(-1, enemy);
        //int hostilecount = getHostileCount(enemyrobots);
        int notGardenerCount = enemyrobots.length - getRobotCount(enemyrobots,RobotType.GARDENER);
        // If there are some enemies...
        if (enemyrobots.length > 0) {

            RobotInfo gardenertoattack = getClosestRobot(enemyrobots, RobotType.GARDENER);
            if (gardenertoattack != null) { // if the enemy is a gardener we give special attention

                Direction scoutToGardenerDir = rc.getLocation().directionTo(gardenertoattack.location);
                if (rc.getLocation().distanceTo(gardenertoattack.location) > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius + 0.02f) {
                    MapLocation nextToGardener = gardenertoattack.getLocation().subtract(scoutToGardenerDir, 2.01f);
                    if (rc.canMove(nextToGardener)) {
                        rc.move(nextToGardener);
                    } else {
                        tryMoveDirectionOrRandom(rc.getLocation().directionTo(gardenertoattack.location));
                    }
                    System.out.println("[debug]Moving to gardener");
                } else if (rc.getLocation().distanceTo(gardenertoattack.location) > RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius) {
                    // If right next to gardener already, constantly rotate to become hard to hit.
                    rc.setIndicatorDot(rc.getLocation(), 100, 0, 0);
                    int isClockwise = (Math.random() > .5 ? 1 : -1);
                    Direction rotatedDirection = scoutToGardenerDir.rotateRightRads((float) (2 * Math.PI / 8.0 * isClockwise));
                    MapLocation rotatedLocation = gardenertoattack.getLocation().add(
                            rotatedDirection.opposite(), RobotType.GARDENER.bodyRadius + RobotType.SCOUT.bodyRadius + .01f);
                    rc.setIndicatorDot(rotatedLocation, 0, 0, 100);
                    if (rc.canMove(rotatedLocation) && !rc.isLocationOccupiedByTree(rotatedLocation)) {
                        rc.move(rotatedLocation);
                    }
                }
                shootAtRobot(gardenertoattack);
                return;
            }

            //This is to shoot at any enemy rohot
            int i = 0;
            if (enemyrobots.length > 0) {
                while (i < enemyrobots.length && enemyrobots[i].getType() != RobotType.ARCHON)
                    i++;
                if (i < enemyrobots.length) {
                    if (enemyrobots[i].getID() == previousShotAtEnemyID && enemyrobots[i].getHealth() == previousShotAtEnemyHealth) {
                        //if the enemy is not getting weaker try another angle
                        tryMoveDirectionOrRandom(rc.getLocation().directionTo(enemyrobots[i].location).rotateRightDegrees(90));
                    } else if (rc.canMove(rc.getLocation().directionTo(enemyrobots[i].getLocation()))) {
                        tryMoveDirectionOrRandom(rc.getLocation().directionTo(enemyrobots[i].getLocation()));
                    }
                    shootAtRobot(enemyrobots[i]);
                }
            }


            if (rc.hasAttacked() || rc.hasMoved()) {

                return;
            }
        }
        else{// if there are no enemies
            //previousShotAtEnemyID = 0;
        }
        if (Math.random() < .1){
            targetDirection = rc.getLocation().directionTo(archonLocations[(int)(Math.random()*archonLocations.length)]);
        }


        //Check if targetDirection on map else change to a random direction

        if(!rc.onTheMap(rc.getLocation().add(targetDirection,5))){
            targetDirection = randomDirection();
        }
        tryMoveDirectionOrRandom(targetDirection);
    }

    private static int getRobotCount(RobotInfo[] robots, RobotType robottype) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType() == robottype)
                count++;
        }
        return count;
    }

    private static int getHostileCount(RobotInfo[] robots) {
        int count = 0;
        for(int i = 0;i<robots.length;i++){
            if(robots[i].getType().canAttack())
                count++;
        }
        return count;
    }

    private static RobotInfo getClosestRobot(RobotInfo[] robots, RobotType robottype) {
        RobotInfo closestRobot = null;
        float currentclosestdistance = 1000000000;
        for(int i =0;i<robots.length;i++){
            if(robots[i].getType() == robottype && rc.getLocation().distanceTo(robots[i].location)<currentclosestdistance){
                currentclosestdistance = rc.getLocation().distanceTo(robots[i].location);
                closestRobot = robots[i];
            }
        }
        return closestRobot;
    }

    private static void shootAtRobot(RobotInfo robot) throws GameActionException {
        if (rc.canFireSingleShot()) {
            rc.fireSingleShot(rc.getLocation().directionTo(robot.location));
            previousShotAtEnemyID = robot.getID();
            previousShotAtEnemyHealth = robot.getHealth();
        }else{
            previousShotAtEnemyID = 0;
        }
    }

    private static boolean tryMoveDirectionOrRandom(Direction direction) throws GameActionException {
        if (!tryMove(direction)) {
            for (int i = 0; i < 20; i++) {
                if (tryMove(randomDirection())) {
                    return true;
                }
            }
        }
        return false;
    }

}
